package makos.com.carrent.upadte_payment_method;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.TextView;

import java.util.ArrayList;

import makos.com.carrent.R;
import makos.com.carrent.shooting_damage.Order_damage_list_Adapter;

/**
 * Created by pc2 on 24-12-2018.
 */

public class Card_list_adapter extends RecyclerView.Adapter<Card_list_adapter.card_holder>
{
    Activity activity;
    ArrayList<card_list_model> model=new ArrayList<card_list_model>();

    public Card_list_adapter(Activity activity,ArrayList<card_list_model> model)
    {
        this.activity = activity;
        this.model = model;
    }

    @NonNull
    @Override
    public card_holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i)
    {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.custom_update_payment,viewGroup,false);
       Card_list_adapter.card_holder holder = new  Card_list_adapter.card_holder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull card_holder card_holder, final int i) {

        TextView cardnum= card_holder.cardnum;
        cardnum.setText(model.get(i).getAccount_number());

        final RadioButton card_type=card_holder.cardtype;
        card_type.setText(model.get(i).getCard_type());

        TextView default_set=card_holder.set_default;

        Log.e("statuseselected", String.valueOf(model.get(i).isSelected()) +""+model.get(i).getStatus());

        card_type.setChecked(model.get(i).isSelected());
        if(model.get(i).getStatus().equals("Default Card"))
        {
            default_set.setBackgroundResource(R.drawable.defaultcard);
            default_set.setText("כרטיס ברירת מחדל");
           // card_type.setChecked(true);
        }else
        {
            default_set.setBackgroundResource(R.drawable.roundersqr2);
            default_set.setText("הגדר כברירת מחדל");
        }

    }

    @Override
    public int getItemCount() {
        return model.size();
    }

    public static class card_holder extends RecyclerView.ViewHolder {

        RadioButton cardtype;
        TextView cardnum;
        TextView set_default;
        public card_holder(@NonNull View itemView) {
            super(itemView);
            cardtype=itemView.findViewById(R.id.cardtype);
            cardnum=itemView.findViewById(R.id.number);
            set_default=itemView.findViewById(R.id.setdefault);
        }
    }

}
