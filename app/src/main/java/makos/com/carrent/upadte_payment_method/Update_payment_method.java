package makos.com.carrent.upadte_payment_method;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.icu.util.GregorianCalendar;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.twinkle94.monthyearpicker.picker.YearMonthPickerDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import makos.com.carrent.Mysingleton.Mysingleton;
import makos.com.carrent.NetworkInfo.Connectivity;
import makos.com.carrent.R;

public class Update_payment_method extends AppCompatActivity {

    String userid = "";
    SharedPreferences preferences;
    SharedPreferences.Editor editor;

    EditText date, acct_no, card_holder_name, ticket_number, security_num;
    Button upadtebtn;
    Button add_new_card;
    int status = 0;

    RecyclerView card_list_recycler;
    ArrayList<card_list_model> model = new ArrayList<card_list_model>();
    RecyclerView.Adapter adapter;
    RecyclerView.LayoutManager layoutManager;

    TextView remove_card,edit_card,set_default_card;
    String card_id;

    TextView nothing_to_show;
    RadioButton card_type;
    int pos=0;
    int remove_stat=0;

    String showdate = "";
     String monthstr="";

     int year_chosen = 0;
     int month_chosen = 0;

     int default_stat = 0;

    final String[] months=new String[]{"ינואר","פברואר","מרץ","אפריל","מאי","יוני","יולי","אוגוסט","ספטמבר","אוקטובר","נובמבר","דצמבר"};

    @SuppressLint("NewApi")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_payment_method);

        date = findViewById(R.id.validity);
        acct_no = findViewById(R.id.id);
        card_holder_name = findViewById(R.id.card_holder_name);
        ticket_number = findViewById(R.id.ticket_num);
        security_num = findViewById(R.id.security_code);
        add_new_card = findViewById(R.id.new_card);
        upadtebtn = findViewById(R.id.upadtebtn);
        card_list_recycler = findViewById(R.id.card_recycler);
        nothing_to_show=findViewById(R.id.textview);

        date.setEnabled(false);
        acct_no.setEnabled(false);
        card_holder_name.setEnabled(false);
        ticket_number.setEnabled(false);
        security_num.setEnabled(false);

        adapter = new Card_list_adapter(Update_payment_method.this, model);
        layoutManager = new LinearLayoutManager(getApplicationContext());
        card_list_recycler.setLayoutManager(layoutManager);
        card_list_recycler.setAdapter(adapter);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        preferences = getSharedPreferences("makos.com.carrent", MODE_PRIVATE);
        editor = preferences.edit();

        userid = preferences.getString("USER_ID", "0");

        date.setFocusable(false);
       // @SuppressLint({"NewApi", "LocalSuppress"}) HebrewCalendar calendar = (HebrewCalendar) HebrewCalendar.getInstance(ULocale.forLanguageTag("@calendar=hebrew"));
       // GregorianCalendar calendar = new GregorianCalendar(Locale.forLanguageTag("@calendar=hebrew"));
        Calendar calendar = Calendar.getInstance();
        calendar.set(2000, 01, 01);
        date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                setdialog();
            }
        });

        card_list_api(1,2);

        upadtebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (status == 0) {
                    validation();

                    Log.e("update", String.valueOf(status) + "" + upadtebtn.getText());

                } else if (status == 1) {
                    //status=2;
                    Log.e("add", String.valueOf(status) + "" + upadtebtn.getText());
                    validation();
                }
            }
        });


        add_new_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                upadtebtn.setEnabled(true);
                Toast.makeText(getApplicationContext(), "add", Toast.LENGTH_SHORT).show();
                status = 1;
                upadtebtn.setText("הוסף כרטיס");//add card
                Log.e("add", String.valueOf(status) + "" + upadtebtn.getText());

                date.setEnabled(true);
                acct_no.setEnabled(true);
                card_holder_name.setEnabled(true);
                ticket_number.setEnabled(true);
                security_num.setEnabled(true);

                date.setText("");
                acct_no.setText("");
                card_holder_name.setText("");
                ticket_number.setText("");
                security_num.setText("");


            }
        });



 card_list_recycler.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), card_list_recycler,
                new RecyclerTouchListener.ClickListener() {
        @SuppressLint("ResourceAsColor")
        @Override
        public void onClick (View view,final int position)
        {

            edit_card=view.findViewById(R.id.edit);
            remove_card=view.findViewById(R.id.remove);
            set_default_card=view.findViewById(R.id.setdefault);
            card_type=view.findViewById(R.id.cardtype);

            card_id=model.get(position).getCard_id();

            pos=position;




            card_type.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v)
                {
                    for (int i = 0; i < model.size(); i++) {
                    if (model.get(i).isSelected()) {
                        model.get(i).setSelected(false);
                        Log.e("checkedstat", String.valueOf(model.get(position).isSelected()));
                    }
                }
                    model.get(position).setSelected(true);
                    get_card_details(model.get(position).getCard_id());

                    Log.e("checkedstat", String.valueOf(model.get(position).isSelected()));

                    adapter.notifyDataSetChanged();
                }});


            Log.e("position", String.valueOf(position));

            edit_card.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    upadtebtn.setEnabled(true);
                    if (model.get(position).getStatus().equals("Default Card"))
                    {
                        default_stat = 1;
                    }else
                    {
                        default_stat = 0;
                    }
                    upadtebtn.setText("עדכון כרטיס");
                    status=0;
                    date.setEnabled(true);
                    acct_no.setEnabled(true);
                    card_holder_name.setEnabled(true);
                    ticket_number.setEnabled(true);
                    security_num.setEnabled(true);
                    Log.e("card_id",card_id);
                    get_card_details(card_id);


                }
            });

            remove_card.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.e("cardid",card_id+""+userid);
                    String status=model.get(position).getStatus();
                    remove_card_api(userid,card_id,status);

                }
            });

            set_default_card.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.e("cardid",card_id+""+userid);
                    set_default_api(card_id,userid,position);
                }
            });

        }


        @Override
        public void onLongClick (View view,int position)
        {

        }

    }));
  }

    private static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private Update_payment_method.RecyclerTouchListener.ClickListener clickListener;

        public RecyclerTouchListener(Context applicationContext, final RecyclerView recyclerView, final Update_payment_method.RecyclerTouchListener.ClickListener clickListener)
        {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(applicationContext, new GestureDetector.SimpleOnGestureListener()
            {
                @Override
                public boolean onSingleTapUp(MotionEvent e)
                {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e)
                {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null)
                    {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }
        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e)
        {
            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e))
            {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }
        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e)
        {
        }
        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept)
        {
        }
        public interface ClickListener
        {
            void onClick(View view, int position);
            void onLongClick(View view, int position);
        }
    }

    private void card_list_api(final int pos, final int stat)
    {

        if(Connectivity.isConnected(getApplicationContext())){
            model.clear();
            String card_list_url="http://rentapp-env.av3qgyvtfy.ap-south-1.elasticbeanstalk.com/rentapp/view_card_details"+"?user_id="+userid;
            Log.e("card_list_url",card_list_url);

            StringRequest stringRequest=new StringRequest(Request.Method.GET, card_list_url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                        Log.e("response",response);

                    try {
                        JSONArray jsonArray=new JSONArray(response);

                        JSONObject jsonObject=jsonArray.getJSONObject(0);

                        if(jsonObject.getString("Status").equals("True"))
                        {

                            card_list_recycler.setVisibility(View.VISIBLE);
                            nothing_to_show.setVisibility(View.INVISIBLE);

                            Toast.makeText(getApplicationContext(),"פרטי כרטיס",Toast.LENGTH_SHORT).show();

                            JSONArray jsonArray1 = jsonObject.getJSONArray("Card Data");

                            for (int i=0;i<jsonArray1.length();i++)
                            {
                                JSONObject jsonObject1 = jsonArray1.getJSONObject(i);
                                 card_list_model models = new card_list_model();

                                 models.setCard_type(jsonObject1.getString("card_type"));
                                 models.setTicket_number(jsonObject1.getString("ticket_number"));
                                 models.setCard_id(jsonObject1.getString("card_id"));
                                 models.setStatus(jsonObject1.getString("status"));
                                 models.setAccount_number(jsonObject1.getString("account_no"));


                                 if (jsonObject1.getString("status").equals("Default Card") && (stat==2 || stat==3 || stat==4 || stat==5)  )
                                {
                                    models.setSelected(true);
                                    get_card_details(jsonObject1.getString("card_id"));
                                    remove_stat=1;
                                }else if(stat==0 && i==pos){
                                     models.setSelected(true);
                                 }else
                                {
                                    models.setSelected(false);
                                }
                                model.add(models);
                            }
                            adapter.notifyDataSetChanged();

                            Log.e("card_count_here", String.valueOf(model.size()));
                            editor.remove("CARD_COUNT");
                            editor.apply();
                            editor.putInt("CARD_COUNT",model.size());
                            editor.apply();
                        }else
                        {
                            Log.e("card_count_here", String.valueOf(model.size()));
                            editor.remove("CARD_COUNT");
                            editor.apply();
                            editor.putInt("CARD_COUNT",0);
                            editor.apply();

                            Toast.makeText(getApplicationContext(),"קבלת פרטי הכרטיס נכשלה",Toast.LENGTH_SHORT).show();
                            card_list_recycler.setVisibility(View.INVISIBLE);
                            nothing_to_show.setVisibility(View.VISIBLE);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });
            Mysingleton.getInstance(Update_payment_method.this).addToRequestQueue(stringRequest);
        }else {
            Toast.makeText(getApplicationContext(),"בדוק את חיבור האינטרנט",Toast.LENGTH_SHORT).show();
        }

    }

    private void validation() {

        Log.e("status", String.valueOf(status));
        if(acct_no.getText().toString().equals("")){
            Toast.makeText(getApplicationContext(),"הזן מזהה כרטיס",Toast.LENGTH_SHORT).show();
        }else if(card_holder_name.getText().toString().equals("")){
            Toast.makeText(getApplicationContext(),"הזן שם מחזיק הכרטיס",Toast.LENGTH_SHORT).show();
        }else if(ticket_number.getText().toString().equals("")){
            Toast.makeText(getApplicationContext(),"הזן מספר כרטיס",Toast.LENGTH_SHORT).show();
        }else if(date.getText().toString().equals("")){
            Toast.makeText(getApplicationContext(),"הזן מספר אימות",Toast.LENGTH_SHORT).show();
        }else if(security_num.getText().toString().equals("")){
            Toast.makeText(getApplicationContext(),"הזן מספר אבטחה",Toast.LENGTH_SHORT).show();
        }else if(status==1){
          add_new_card_api();
        }else if(status==0){
            upadte_card_api();
        }

    }

    private void add_new_card_api()
    {
        upadtebtn.setEnabled(false);
        String status;
        if(model.size()==0){
            status="1";
            remove_stat=1;
        }else {
            status="0";
        }

        if(Connectivity.isConnected(getApplicationContext())){
            String addurl= null;
            try {
                addurl = "http://rentapp-env.av3qgyvtfy.ap-south-1.elasticbeanstalk.com/rentapp/insert_card_details"
                        +"?user_id="+userid
                        +"&account_number="+ acct_no.getText().toString()
                        +"&card_holder_name="+  URLEncoder.encode(Html.toHtml(card_holder_name.getText()),"UTF-8").toString()
                        +"&ticket_number="+ URLEncoder.encode(ticket_number.getText().toString(),"UTF-8").toString()
                        +"&validity="+ URLEncoder.encode((showdate),"UTF-8").toString()
                        +"&security_code="+ URLEncoder.encode(security_num.getText().toString(),"UTF-8").toString()
                        +"&card_type="+URLEncoder.encode("autocheck","UTF-8").toString()
                        +"&status="+URLEncoder.encode(status,"UTF-8").toString();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            Log.e("addurl",addurl);

            StringRequest stringRequest=new StringRequest(Request.Method.GET, addurl,new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.e("response",response);

                    try {
                        JSONArray jsonArray=new JSONArray(response);

                        JSONObject jsonObject=jsonArray.getJSONObject(0);

                        if(jsonObject.getString("Status").equals("True")){
                            //Toast.makeText(getApplicationContext(),jsonObject.getString("Message").toString(),Toast.LENGTH_SHORT).show();
                            Toast.makeText(getApplicationContext(),"הכרטיס נוסף בהצלחה",Toast.LENGTH_SHORT).show();

                            card_list_api(1,5);

                            date.setText("");
                            acct_no.setText("");
                            card_holder_name.setText("");
                            ticket_number.setText("");
                            security_num.setText("");

                            date.setEnabled(false);
                            acct_no.setEnabled(false);
                            card_holder_name.setEnabled(false);
                            ticket_number.setEnabled(false);
                            security_num.setEnabled(false);

                            upadtebtn.setText("עדכון");
                            //get_card_details(card_id);


                        }else {
                            //Toast.makeText(getApplicationContext(),jsonObject.getString("Message").toString(),Toast.LENGTH_SHORT).show();
                            Toast.makeText(getApplicationContext(),"הוספת הכרטיס נכשלה",Toast.LENGTH_SHORT).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });
            Mysingleton.getInstance(Update_payment_method.this).addToRequestQueue(stringRequest);
        }else
        {
            Toast.makeText(getApplicationContext(),"בדוק את חיבור האינטרנט",Toast.LENGTH_SHORT).show();
        }
    }

    private void upadte_card_api()
    {
        upadtebtn.setEnabled(false);
        Log.e("card_id",card_id);
        if(Connectivity.isConnected(getApplicationContext())){

            String status;
            if(default_stat !=0){
                status="1";
            }else {
                status="0";
            }
            String update= null;
            try {
                update = "http://rentapp-env.av3qgyvtfy.ap-south-1.elasticbeanstalk.com/rentapp/update_card_details"
                        +"?card_id="+card_id
                        +"&user_id="+userid
                        +"&account_number="+ URLEncoder.encode(acct_no.getText().toString(),"UTF-8").toString()
                        +"&card_holder_name="+  URLEncoder.encode(Html.toHtml(card_holder_name.getText()),"UTF-8").toString()
                        +"&ticket_number="+ URLEncoder.encode(ticket_number.getText().toString(),"UTF-8").toString()
                        +"&validity="+ URLEncoder.encode(showdate,"UTF-8")
                        +"&security_code="+URLEncoder.encode(security_num.getText().toString(),"UTF-8").toString()
                        +"&card_type="+URLEncoder.encode("autocheck","UTF-8").toString()
                        +"&status="+URLEncoder.encode(status,"UTF-8").toString();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            Log.e("update",update);

            StringRequest stringRequest=new StringRequest(Request.Method.GET, update, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.e("response",response);

                    try {
                        JSONArray jsonArray=new JSONArray(response);

                        JSONObject jsonObject=jsonArray.getJSONObject(0);

                        if(jsonObject.getString("Status").equals("True")){
                            //Toast.makeText(getApplicationContext(),jsonObject.getString("Message").toString(),Toast.LENGTH_SHORT).show();
                            Toast.makeText(getApplicationContext(),"הכרטיס עודכן בהצלחה",Toast.LENGTH_SHORT).show();

                            card_list_api(pos,0);

                            date.setEnabled(false);
                            acct_no.setEnabled(false);
                            card_holder_name.setEnabled(false);
                            ticket_number.setEnabled(false);
                            security_num.setEnabled(false);
                            get_card_details(card_id);


                        }else {
                            //Toast.makeText(getApplicationContext(),jsonObject.getString("Message").toString(),Toast.LENGTH_SHORT).show();
                            Toast.makeText(getApplicationContext(),"העדכון נכשל",Toast.LENGTH_SHORT).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });
            Mysingleton.getInstance(Update_payment_method.this).addToRequestQueue(stringRequest);
        }else {
            Toast.makeText(getApplicationContext(),"בדוק את חיבור האינטרנט",Toast.LENGTH_SHORT).show();
        }

    }

    private void get_card_details(String card_id)
    {

        if(Connectivity.isConnected(getApplicationContext())){

            Log.e("cid",card_id);
            String get_card_details_url="http://rentapp-env.av3qgyvtfy.ap-south-1.elasticbeanstalk.com/rentapp/view_card_details1"+"?card_id="+card_id;
            Log.e("card_deatils",get_card_details_url);

            StringRequest stringRequest=new StringRequest(Request.Method.GET, get_card_details_url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response)
                {
                    Log.e("getresponse",response);
                    try
                    {
                        JSONArray jsonArray=new JSONArray(response);
                        JSONObject jsonObject=jsonArray.getJSONObject(0);
                        if(jsonObject.getString("Status").equals("True")){

                            JSONArray jsonArray1=jsonObject.getJSONArray("Card Data");

                            JSONObject jsonObject1=jsonArray1.getJSONObject(0);

                            acct_no.setText(Html.fromHtml(jsonObject1.getString("account_no")).toString());
                            card_holder_name.setText(Html.fromHtml(jsonObject1.getString("card_holder_name")).toString());
                            ticket_number.setText(Html.fromHtml(jsonObject1.getString("ticket_number")).toString());
                            security_num.setText(Html.fromHtml(jsonObject1.getString("security_code")).toString());

                            String convert = "";
                            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                            Date dd = new Date();
                            Calendar cal = Calendar.getInstance();
                            try
                            {
                                convert = jsonObject1.getString("validity").toString();
                                dd = format.parse(jsonObject1.getString("validity"));
                                cal.setTime(dd);
                                year_chosen = cal.get(Calendar.YEAR);

                                Log.e("substring",convert.substring(5,7));
                                if (convert.substring(5,7).equals("1-"))
                                {
                                    format.applyPattern("yyyy-MMM");
                                    date.setText((cal.get(Calendar.YEAR)) + "-" + months[0]);
                                    month_chosen = 0;
                                }else
                                {
                                    format.applyPattern("yyyy-MMM");
                                    date.setText((cal.get(Calendar.YEAR)) + "-" + months[(cal.get(Calendar.MONTH) + 1)]);
                                    month_chosen = cal.get(Calendar.MONTH) + 1;
                                }

                                Log.e("monthhere", String.valueOf(cal.get(Calendar.MONTH)));

                            } catch (ParseException e) {
                                e.printStackTrace();
                            }

                        }
                        else {
                            Toast.makeText(getApplicationContext(),"קבלת פרטי הכרטיס נכשלה",Toast.LENGTH_SHORT).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });
            Mysingleton.getInstance(Update_payment_method.this).addToRequestQueue(stringRequest);

        }else {
            Toast.makeText(getApplicationContext(),"בדוק את חיבור האינטרנט",Toast.LENGTH_SHORT).show();
        }


    }

    private void remove_card_api(String userid, String card_id, final String position) {
        if(Connectivity.isConnected(getApplicationContext())){

            final String remove_url="http://rentapp-env.av3qgyvtfy.ap-south-1.elasticbeanstalk.com/rentapp/delete_card"
                    +"?card_id="+card_id
                    +"&user_id="+userid;
            Log.e("remove_url",remove_url);

            StringRequest stringRequest=new StringRequest(Request.Method.GET, remove_url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    Log.e("removeres",response);

                    try {
                        JSONArray jsonArray=new JSONArray(response);

                        JSONObject jsonObject=jsonArray.getJSONObject(0);

                        if(jsonObject.getString("Status").equals("True")){
                           // Toast.makeText(getApplicationContext(),jsonObject.getString("Message"),Toast.LENGTH_SHORT).show();
                            Toast.makeText(getApplicationContext(),"הכרטיס הוסר בהצלחה",Toast.LENGTH_SHORT).show();
                            card_list_api(1,4);

                            date.setText("");
                            acct_no.setText("");
                            card_holder_name.setText("");
                            ticket_number.setText("");
                            security_num.setText("");

                            date.setEnabled(false);
                            acct_no.setEnabled(false);
                            card_holder_name.setEnabled(false);
                            ticket_number.setEnabled(false);
                            security_num.setEnabled(false);

                            if(position.equals("Default Card") && remove_stat==0){
                                final Dialog dialog = new Dialog(Update_payment_method.this);
                                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                dialog.setCancelable(true);
                                dialog.setContentView(R.layout.removedefault);
                                Button ok=dialog.findViewById(R.id.ok);

                                ok.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        dialog.dismiss();
                                    }
                                });
                                dialog.show();
                            }
                        }
                        else {
                            //Toast.makeText(getApplicationContext(),jsonObject.getString("Message"),Toast.LENGTH_SHORT).show();
                            Toast.makeText(getApplicationContext(),"הסרת הכרטיס נכשלה",Toast.LENGTH_SHORT).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });

            Mysingleton.getInstance(Update_payment_method.this).addToRequestQueue(stringRequest);

        }else {
            Toast.makeText(getApplicationContext(),"בדוק את חיבור האינטרנט",Toast.LENGTH_SHORT).show();
        }
    }


    private void set_default_api(String card_id, String userid, final int position) {

        if(Connectivity.isConnected(getApplicationContext()))
        {
            String ser_default_url="http://rentapp-env.av3qgyvtfy.ap-south-1.elasticbeanstalk.com/rentapp/set_card_default"
                    +"?card_id="+card_id
                    +"&user_id="+userid
                    +"&status=1";

            Log.e("set_default",ser_default_url);

            StringRequest stringRequest=new StringRequest(Request.Method.GET, ser_default_url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.e("setdefaultres",response);

                    try {
                        JSONArray jsonArray=new JSONArray(response);
                        JSONObject jsonObject=jsonArray.getJSONObject(0);

                        if(jsonObject.getString("Status").equals("True"))
                        {
                            //Toast.makeText(getApplicationContext(),jsonObject.getString("Message"),Toast.LENGTH_SHORT).show();
                            Toast.makeText(getApplicationContext(),"סטטוס הכרטיס השתנה בהצלחה",Toast.LENGTH_SHORT).show();
                            remove_stat=1;
                            card_list_api(1,3);
                        }
                        else
                        {
                            //Toast.makeText(getApplicationContext(),jsonObject.getString("Message"),Toast.LENGTH_SHORT).show();
                            Toast.makeText(getApplicationContext(),"שינוי הסטטוס נכשל",Toast.LENGTH_SHORT).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });
            Mysingleton.getInstance(Update_payment_method.this).addToRequestQueue(stringRequest);

        }else {
            Toast.makeText(getApplicationContext(),"בדוק את חיבור האינטרנט",Toast.LENGTH_SHORT).show();
        }
    }

    private void setdialog() {

        final Dialog dialog=new Dialog(Update_payment_method.this);

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.activity_yearmonthpicker);

        final TextView month_name,year_name;
        Button ok,cancel;
        final NumberPicker year_picker,month_picker;
        final LinearLayout year_layout,month_layout;

        month_name=dialog.findViewById(R.id.month_name);
        year_name=dialog.findViewById(R.id.year_name);
        ok=dialog.findViewById(R.id.okbtn);
        cancel=dialog.findViewById(R.id.cancelbtn);
        year_picker=dialog.findViewById(R.id.year_picker);
        month_picker=dialog.findViewById(R.id.month_picker);
        year_layout=dialog.findViewById(R.id.linearlayout_year);
        month_layout=dialog.findViewById(R.id.linearlayout_month);

        final  String[] monthseng=new String[]{"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
        //final String[] months=new String[]{"ינואר","פברואר","מרץ","אפריל","מאי","יוני","יולי","אוגוסט","ספטמבר","אוקטובר","נובמבר","דצמבר"};

         int num=0;
        Calendar calendar=Calendar.getInstance();
        Date dates=new Date();
        dates.setTime(calendar.getTimeInMillis());
        String year="",month="";
        year=String.valueOf(calendar.get(Calendar.YEAR));
        //month= String.valueOf(calendar.get(Calendar.MONTH));

        month_picker.setWrapSelectorWheel(true);

        SimpleDateFormat simpleDateFormat=new SimpleDateFormat();
        String datestr=simpleDateFormat.format(dates);
        try {
            dates=simpleDateFormat.parse(datestr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        simpleDateFormat.applyPattern("MMMM");
        month=simpleDateFormat.format(dates);

        year_name.setText(year);
        month_name.setText(month);

        year_picker.setMinValue(calendar.get(Calendar.YEAR));
        year_picker.setMaxValue((calendar.get(Calendar.YEAR)+50));

        month_picker.setMinValue(0);
        month_picker.setMaxValue(11);

        month_picker.setDisplayedValues(months);

        if (status == 0)
        {
            year_picker.setValue(year_chosen);

            month_picker.setValue(month_chosen);
        }

        month_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                month_picker.setVisibility(View.VISIBLE);
                month_layout.setVisibility(View.VISIBLE);
                year_picker.setVisibility(View.INVISIBLE);
                year_layout.setVisibility(View.GONE);
            }
        });

        year_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                month_picker.setVisibility(View.INVISIBLE);
                month_layout.setVisibility(View.GONE);
                year_layout.setVisibility(View.VISIBLE);
                year_picker.setVisibility(View.VISIBLE);

            }
        });

        year_picker.setOnScrollListener(new NumberPicker.OnScrollListener() {
            @Override
            public void onScrollStateChange(NumberPicker view, int scrollState) {
                year_name.setText(String.valueOf(year_picker.getValue()));
            }
        });

        month_picker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                month_name.setText(months[month_picker.getValue()].toString());
                monthstr=monthseng[month_picker.getValue()].toString();
                //num=month_picker.getValue();
            }
        });

        dialog.show();

        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.e("monththere", String.valueOf(month_picker.getValue()));
                showdate=year_name.getText().toString()+"-"+(month_picker.getValue()+1)+"-00";
                date.setText(year_name.getText().toString()+"-"+month_name.getText().toString());

                dialog.dismiss();
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case android.R.id.home:

                if(remove_stat==0)
                {
                    popup();
                }else {
                    finish();
                }
                return true;
            default:
                if(remove_stat==0)
                {
                    popup();
                }return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
       //
        Log.e("remove_stat", String.valueOf(remove_stat));
        if(remove_stat==0)
        {
            popup();
        }else {
            super.onBackPressed();
        }
    }

    private  void popup(){
        final Dialog dialog = new Dialog(Update_payment_method.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.removedefault);
        Button ok=dialog.findViewById(R.id.ok);

        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

}
