package makos.com.carrent;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.login.LoginManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import makos.com.carrent.Car_list.Previous_orders;
import makos.com.carrent.Change_password.Change_password;
import makos.com.carrent.Mysingleton.Mysingleton;
import makos.com.carrent.NetworkInfo.Connectivity;
import makos.com.carrent.Payment_Details.Payment_details;
import makos.com.carrent.QuestionAnswer.Question_Answer;
import makos.com.carrent.back_to_order.Back_order;
import makos.com.carrent.invoice.Invoice;
import makos.com.carrent.lobby.Lobby;
import makos.com.carrent.login.Login;
import makos.com.carrent.profile.Profile;
import makos.com.carrent.statusurl.Staticurl;
import makos.com.carrent.upadte_payment_method.Update_payment_method;
import makos.com.carrent.update_profile.Update_profile;

public class MainActivity extends AppCompatActivity  implements NavigationView.OnNavigationItemSelectedListener
{

    String userid = "";
    TextView pts;
    String tpts="0";

    public SharedPreferences preferences;
    SharedPreferences.Editor editor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
       // getSupportActionBar().setDisplayShowHomeEnabled(true);


        //Intent intent = getIntent();
        //userid = intent.getStringExtra("UserId");

        preferences = getSharedPreferences("makos.com.carrent",MODE_PRIVATE);
        editor = preferences.edit();

        userid = preferences.getString("USER_ID","0");

        Log.e("userid",userid);
        final DrawerLayout drawerLayout = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this,drawerLayout,toolbar, R.string.navigation_drawer_open,R.string.navigation_drawer_close);
        drawerLayout.setDrawerListener(toggle);

       // toggle.setDrawerIndicatorEnabled(true);
       // drawerLayout.addDrawerListener(toggle);

        toggle.setDrawerIndicatorEnabled(false);

        toggle.setHomeAsUpIndicator(R.drawable.navigationicon);

        toggle.setToolbarNavigationClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (drawerLayout.isDrawerVisible(GravityCompat.START)) {
                    drawerLayout.closeDrawer(GravityCompat.START);
                } else {
                    drawerLayout.openDrawer(GravityCompat.START);
                }
            }
        });
        toggle.syncState();

        /*ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        toggle.setDrawerIndicatorEnabled(false);
        drawerLayout.addDrawerListener(toggle);

        toggle.syncState();*/

        final NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View header = navigationView.getHeaderView(0);
        pts=header.findViewById(R.id.pts);

        userptsapi();

       /* CarHome userHome=new CarHome();
        FragmentManager fragmentManager = getSupportFragmentManager();
        Bundle arg=new Bundle();
        userHome.setArguments(arg);
       //fragmentTransactions.addToBackStack(null);
        fragmentManager.beginTransaction().replace(R.id.flContent, userHome).commit();*/

        Lobby lobby=new Lobby();
        Bundle bundle2 =new Bundle();
        bundle2.putString("userid",userid);
        FragmentManager fragmentManagers1= getSupportFragmentManager();
        FragmentTransaction fragmentTransactions1 = fragmentManagers1.beginTransaction();
        lobby.setArguments(bundle2);
        fragmentTransactions1.replace(R.id.flContent, lobby);
      //  fragmentTransactions1.addToBackStack(null);
        fragmentTransactions1.commit();

    }

    private void userptsapi() {

        if(Connectivity.isConnected(getApplicationContext())){

           // Log.d("userrname", String.valueOf(username)+ password);
            String userpts= Staticurl.userpoint+"?id="+userid;

            Log.e("userpts", userpts);

            StringRequest stringRequest=new StringRequest(Request.Method.GET,
                    userpts,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {

                            Log.e("res", response);

                            try {
                                JSONArray jsonArray=new JSONArray(response);
                                JSONObject jsonObject=jsonArray.getJSONObject(0);
                                if(jsonObject.getString("Status").equals("True"))
                                {
                                    tpts=jsonObject.getString("User Points");
                                    pts.setText(tpts);
                                }else {
                                    //Toast.makeText(getApplicationContext(),jsonObject.getString("Message"),Toast.LENGTH_SHORT).show();
                                    pts.setText("0");
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });
            Mysingleton.getInstance(MainActivity.this).addToRequestQueue(stringRequest);

        }

        else {
            Toast.makeText(getApplicationContext(), "Check Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }



    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        int id = item.getItemId();
        //Fragment fragment = null;
        // Class fragmentClass;
        //lobby  serchinvitaiton  updatepaymentmethod

        switch (id) {


            case R.id.home:
                /*CarHome Home=new CarHome();
                FragmentManager fragmentManager = getSupportFragmentManager();
                Bundle arg=new Bundle();
                Home.setArguments(arg);
                fragmentManager.beginTransaction().replace(R.id.flContent, Home).commit();*/

                Intent back = new Intent(MainActivity.this,Back_order.class);
                startActivity(back);

                break;

            case R.id.lobby:
                Lobby lobby=new Lobby();
                Bundle bundle =new Bundle();
                bundle.putString("userid",userid);
                FragmentManager fragmentManagers1= getSupportFragmentManager();
                FragmentTransaction fragmentTransactions1 = fragmentManagers1.beginTransaction();
                lobby.setArguments(bundle);
                fragmentTransactions1.replace(R.id.flContent, lobby);
                fragmentTransactions1.addToBackStack(null);
                fragmentTransactions1.commit();
                break;

            case R.id.loobyes:

                /*Profile profile=new Profile();
                FragmentManager fragmentManagers11= getSupportFragmentManager();
                FragmentTransaction fragmentTransactions11 = fragmentManagers11.beginTransaction();
                fragmentTransactions11.replace(R.id.flContent, profile);
                fragmentTransactions11.addToBackStack(null);
                fragmentTransactions11.commit();*/

                Intent profile = new Intent(getApplicationContext(),Profile.class);
                profile.putExtra("user_id",userid);
                startActivity(profile);
                break;

            case R.id.question_answer:

                /*Question_Answer question=new Question_Answer();
                Bundle bundle1 =new Bundle();
                bundle1.putString("userid",userid);
                FragmentManager fragmentManagers12= getSupportFragmentManager();
                FragmentTransaction fragmentTransactions12 = fragmentManagers12.beginTransaction();
                question.setArguments(bundle1);
                fragmentTransactions12.replace(R.id.flContent, question);
                fragmentTransactions12.addToBackStack(null);
                fragmentTransactions12.commit();*/

                Intent intent = new Intent(MainActivity.this,Question_Answer.class);
                startActivity(intent);
                break;

            case R.id.back_to_order:

                Intent intent1 = new Intent(MainActivity.this,Previous_orders.class);
                intent1.putExtra("user_id",userid);
                intent1.putExtra("status","2");
                startActivity(intent1);
                /*Back_order order=new Back_order();
                FragmentManager fragmentManagers2= getSupportFragmentManager();
                FragmentTransaction fragmentTransactions2 = fragmentManagers2.beginTransaction();
                fragmentTransactions2.replace(R.id.flContent, order);
                fragmentTransactions2.addToBackStack(null);
                fragmentTransactions2.commit();*/
                break;


            case R.id.chngpassword:
                Intent in=new Intent(getApplicationContext(), Change_password.class);
                in.putExtra("UserId",userid);
                startActivity(in);
                break;


            case R.id.updateprofile:
                Intent update_intent=new Intent(getApplicationContext(), Update_profile.class);
                update_intent.putExtra("userid",userid);
                startActivity(update_intent);
                break;

            case R.id.logout:
               logout_api();
                break;


            case R.id.invoice:
                Invoice invoice = new Invoice();
                Bundle bundle2=new Bundle();
                bundle2.putString("userid",userid);
                FragmentManager fragmentManagers3= getSupportFragmentManager();
                FragmentTransaction fragmentTransactions3 = fragmentManagers3.beginTransaction();
                invoice.setArguments(bundle2);
                fragmentTransactions3.replace(R.id.flContent, invoice);
                fragmentTransactions3.addToBackStack(null);
                fragmentTransactions3.commit();

               /*Intent intent3=new Intent(getApplicationContext(),Payment_details.class);
               startActivity(intent3);*/

                break;

      /*   case R.id.shot:
                Cardemo home=new Cardemo();
                FragmentManager fragmentManagers = getSupportFragmentManager();
                FragmentTransaction fragmentTransactions = fragmentManagers.beginTransaction();
                fragmentTransactions.replace(R.id.flContent, home);
                fragmentTransactions.addToBackStack(null);
                fragmentTransactions.commit();
                break;



            case R.id.invoice:
                Invoice invoice=new Invoice();
                FragmentManager fragmentManagers1 = getSupportFragmentManager();
                FragmentTransaction fragmentTransactions1 = fragmentManagers1.beginTransaction();
                fragmentTransactions1.replace(R.id.flContent, invoice);
                fragmentTransactions1.addToBackStack(null);
                fragmentTransactions1.commit();
                break;

            case R.id.searchinvitaions:

                Search_Invitation searchInvitation=new Search_Invitation();
                FragmentManager fragmentManagers2 = getSupportFragmentManager();
                FragmentTransaction fragmentTransactions2 = fragmentManagers2.beginTransaction();
                fragmentTransactions2.replace(R.id.flContent, searchInvitation);
                fragmentTransactions2.addToBackStack(null);
                fragmentTransactions2.commit();
                break;

            case R.id.updatepaymentmethod:

                Update_payment_method update=new Update_payment_method();
                FragmentManager fragmentManagers3 = getSupportFragmentManager();
                FragmentTransaction fragmentTransactions3 = fragmentManagers3.beginTransaction();
                fragmentTransactions3.replace(R.id.flContent, update);
                fragmentTransactions3.addToBackStack(null);
                fragmentTransactions3.commit();
                break;

            case R.id.myprofile:

                Profile profile=new Profile();
                FragmentManager fragmentManagers4 = getSupportFragmentManager();
                FragmentTransaction fragmentTransactions4 = fragmentManagers4.beginTransaction();
                fragmentTransactions4.replace(R.id.flContent, profile);
                fragmentTransactions4.addToBackStack(null);
                fragmentTransactions4.commit();
                break;

            case R.id.terms_of_insurance:

                Terms_of_Insurance insurance=new Terms_of_Insurance();
                FragmentManager fragmentManagers5= getSupportFragmentManager();
                FragmentTransaction fragmentTransactions5 = fragmentManagers5.beginTransaction();
                fragmentTransactions5.replace(R.id.flContent, insurance);
                fragmentTransactions5.addToBackStack(null);
                fragmentTransactions5.commit();
                break;

            case R.id.panel_managments:

                PanelManagement panel=new PanelManagement();
                FragmentManager fragmentManagers6= getSupportFragmentManager();
                FragmentTransaction fragmentTransactions6 = fragmentManagers6.beginTransaction();
                fragmentTransactions6.replace(R.id.flContent, panel);
                fragmentTransactions6.addToBackStack(null);
                fragmentTransactions6.commit();
                break;



            case R.id.points:

                Points point=new Points();
                FragmentManager fragmentManagers8= getSupportFragmentManager();
                FragmentTransaction fragmentTransactions8 = fragmentManagers8.beginTransaction();
                fragmentTransactions8.replace(R.id.flContent, point);
                fragmentTransactions8.addToBackStack(null);
                fragmentTransactions8.commit();
                break;

            case R.id.setting_up_companys:

                Setting_up_company setingcompny=new Setting_up_company();
                FragmentManager fragmentManagers9= getSupportFragmentManager();
                FragmentTransaction fragmentTransactions9 = fragmentManagers9.beginTransaction();
                fragmentTransactions9.replace(R.id.flContent, setingcompny);
                fragmentTransactions9.addToBackStack(null);
                fragmentTransactions9.commit();
                break;

            case R.id.compnylistprice:

                CompanyPriceList compnylist=new CompanyPriceList();
                FragmentManager fragmentManagers10= getSupportFragmentManager();
                FragmentTransaction fragmentTransactions10 = fragmentManagers10.beginTransaction();
                fragmentTransactions10.replace(R.id.flContent, compnylist);
                fragmentTransactions10.addToBackStack(null);
                fragmentTransactions10.commit();
                break;*/



        }
        DrawerLayout drawer= findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void logout_api() {

        String logouturl = Staticurl.logout+"?id="+userid;

        Log.e("logouturl",logouturl);

        if (Connectivity.isConnected(getApplicationContext()))
        {
            StringRequest  request = new StringRequest(Request.Method.GET, logouturl, new Response.Listener<String>() {
                @Override
                public void onResponse(String response)
                {
                    try
                    {
                        JSONArray jsonArray = new JSONArray(response);
                        JSONObject jsonObject = jsonArray.getJSONObject(0);

                        if (jsonObject.getString("Status").equals("True"))
                        {
                            Toast.makeText(getApplicationContext(),"התנתקות בהצלחה",Toast.LENGTH_SHORT).show();
                            //Toast.makeText(getApplicationContext(),"logout",Toast.LENGTH_SHORT).show();
                            logoutFromFacebook();
                            editor.clear();
                            editor.apply();
                            Intent in= new Intent(getApplicationContext(),Login.class);
                            startActivity(in);
                        }else
                        {
                            Toast.makeText(getApplicationContext(),"ההתנתקות נכשלה",Toast.LENGTH_SHORT).show();
                            //Toast.makeText(getApplicationContext(),"login",Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });
            Mysingleton.getInstance(getApplicationContext()).addToRequestQueue(request);
        }
        else
        {
            Toast.makeText(getApplicationContext(),"בדוק את החיבור לאינטרנט",Toast.LENGTH_SHORT).show();
        }

    }

    private void popup()
    {
        final Dialog dialog = new Dialog(MainActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.logout_popup);

        Button ok=dialog.findViewById(R.id.ok);


        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               editor.clear();
               editor.apply();
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    @Override
    public void onBackPressed()
    {
        new AlertDialog.Builder(this)
                .setMessage("האם אתה בטוח רוצה ל יציאה?")
                .setCancelable(false)
                .setNegativeButton("כן", new DialogInterface.OnClickListener() {
                    @SuppressLint("NewApi")
                    public void onClick(DialogInterface dialog, int id) {
                        finishAffinity();
                    }
                })
                .setPositiveButton("לא",null)
                .show();

        //super.onBackPressed();
    }


    public void logoutFromFacebook() {

        if (AccessToken.getCurrentAccessToken() == null) {
            return; // user already logged out
        }

        new GraphRequest(AccessToken.getCurrentAccessToken(), "/me/permissions/", null, HttpMethod.DELETE, new GraphRequest
                .Callback() {
            @Override
            public void onCompleted(GraphResponse graphResponse) {
                Toast.makeText(getApplicationContext(),"logout",Toast.LENGTH_SHORT).show();
                LoginManager.getInstance().logOut();

            }
        }).executeAsync();
    }
}
