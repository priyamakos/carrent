package makos.com.carrent.Forgot_password;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import makos.com.carrent.Mysingleton.Mysingleton;
import makos.com.carrent.NetworkInfo.Connectivity;
import makos.com.carrent.R;
import makos.com.carrent.login.Login;
import makos.com.carrent.statusurl.Staticurl;

public class Forget_Password extends AppCompatActivity
{
    EditText email;
    Button submit;
    TextView back;
    ProgressBar progressBar;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget__password);

        email = findViewById(R.id.emailid);
        submit = findViewById(R.id.forgotpass);
        back = findViewById(R.id.back);
        progressBar=findViewById(R.id.progressbar);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(Forget_Password.this, Login.class);
                startActivity(intent);
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (email.getText().toString().equals(""))
                {
                    Toast.makeText(getApplicationContext(),"הזן מזהה דוא\"ל",Toast.LENGTH_SHORT).show();
                }else
                {
                    forgetpassword();
                }
            }
        });
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    private void forgetpassword()
    {
        String forgeturl = null;
        try {
            forgeturl = Staticurl.forgot_passsword
                               + URLEncoder.encode(email.getText().toString(),"UTF-8").toString();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        Log.e("forgeturl",forgeturl);

        progressBar.setVisibility(View.VISIBLE);
        if (Connectivity.isConnected(getApplicationContext()))
        {
            StringRequest request = new StringRequest(Request.Method.GET, forgeturl, new Response.Listener<String>() {
                @Override
                public void onResponse(String response)
                {
                    Log.e("Forgotpassresponce",response);
                    try
                    {
                        JSONArray jsonArray = new JSONArray(response);
                        JSONObject jsonObject = jsonArray.getJSONObject(0);

                        if (jsonObject.getString("Status").equals("1"))
                        {
                            Toast.makeText(getApplicationContext(),"האימייל נשלח בהצלחה",Toast.LENGTH_SHORT).show();
                            progressBar.setVisibility(View.INVISIBLE);
                        }
                        else
                        {
                            Toast.makeText(getApplicationContext(),"שליחת האימייל נכשלה",Toast.LENGTH_SHORT).show();
                            progressBar.setVisibility(View.INVISIBLE);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressBar.setVisibility(View.INVISIBLE);
                }
            });
            request.setRetryPolicy(new DefaultRetryPolicy(
                    0,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            Mysingleton.getInstance(getApplicationContext()).addToRequestQueue(request);
        }else
        {
            Toast.makeText(getApplicationContext(),"בדוק את החיבור לאינטרנט",Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Take care of popping the fragment back stack or finishing the activity
     * as appropriate.
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
