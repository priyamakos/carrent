package makos.com.carrent.SplashScreen;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;


import makos.com.carrent.MainActivity;
import makos.com.carrent.R;
import makos.com.carrent.login.Login;

public class splashscreen extends AppCompatActivity {

    public SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    Handler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splashscreen);

        sharedPreferences = getSharedPreferences("makos.com.carrent", Context.MODE_PRIVATE);
        handler = new Handler();
        handler.postDelayed(new Runnable()
        {
            @Override
            public void run()
            {
                editor = sharedPreferences.edit();

                Log.e("statussplash", String.valueOf(sharedPreferences.getBoolean("STATUS", false)));

                if(sharedPreferences.getBoolean("STATUS", false))
                {
                    Intent splashLogin = new Intent(splashscreen.this, MainActivity.class);
                    splashLogin.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(splashLogin);
                    finish();
                }else
                {
                    Intent splashLogin = new Intent(splashscreen.this, Login.class);
                    splashLogin.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(splashLogin);
                    finish();

                }

            }
        },1500);
        printHashKey();

    }

    public void printHashKey() {
        try {
            PackageInfo info = getPackageManager().getPackageInfo(getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String hashKey = new String(Base64.encode(md.digest(), 0));
                Log.e("printHashKey", "printHashKey() Hash Key: " + hashKey);
            }
            Log.e("printHashKey", "printHashKey()");
        } catch (NoSuchAlgorithmException e) {
        } catch (Exception e) {
            Log.e("printHashKey", "printHashKey()", e);
        }
    }
}
