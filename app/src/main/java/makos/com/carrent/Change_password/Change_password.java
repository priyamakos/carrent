package makos.com.carrent.Change_password;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import makos.com.carrent.Mysingleton.Mysingleton;
import makos.com.carrent.NetworkInfo.Connectivity;
import makos.com.carrent.R;
import makos.com.carrent.statusurl.Staticurl;

/**
 * Created by PC1 on 01-12-2018.
 */

public class Change_password extends AppCompatActivity
{
    EditText oldpass,newpass;
    Button submit;
    String userid;
    ProgressBar progressBar;

    SharedPreferences preferences;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        oldpass = findViewById(R.id.oldpassword);
        newpass = findViewById(R.id.newpassword);
        submit = findViewById(R.id.submit);
        progressBar=findViewById(R.id.progressbar);

        preferences = getSharedPreferences("makos.com.carrent",MODE_PRIVATE);
        editor = preferences.edit();

        userid = preferences.getString("USER_ID","0");
        Intent intent = getIntent();
        //userid = intent.getStringExtra("UserId");
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (oldpass.getText().toString().equals(""))
                {
                    Toast.makeText(getApplicationContext(),"הזן סיסמה ישנה",Toast.LENGTH_SHORT).show();
                }else if (newpass.getText().toString().equals(""))
                {
                    Toast.makeText(getApplicationContext(),"הכנס סיסמה חדשה",Toast.LENGTH_SHORT).show();
                }else
                {
                    change_password();
                }
            }
        });

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    private  void  change_password()
    {
        String changeurl = null;
        try {
            changeurl = Staticurl.change_password
                               + "?id=" + userid
                               + "&old_password=" + URLEncoder.encode(Html.toHtml(oldpass.getText()),"UTF-8").toString()
                               + "&new_password=" +URLEncoder.encode(Html.toHtml(newpass.getText()),"UTF-8").toString();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        Log.e("changeurl", changeurl);

        progressBar.setVisibility(View.VISIBLE);
        if (Connectivity.isConnected(getApplicationContext()))
        {
            final StringRequest request = new StringRequest(Request.Method.GET, changeurl, new Response.Listener<String>() {
                @Override
                public void onResponse(String response)
                {
                    Log.e("Changepassresponce",response);
                    try
                    {
                        JSONArray jsonArray = new JSONArray(response);

                        JSONObject jsonObject = jsonArray.getJSONObject(0);

                        if (jsonObject.getString("Status").equals("True"))
                        {
                            Toast.makeText(getApplicationContext(),"סיסמה שונתה בהצלחה",Toast.LENGTH_SHORT).show();
                            oldpass.setText("");
                            newpass.setText("");
                            progressBar.setVisibility(View.INVISIBLE);
                        }
                        else
                        {
                            Toast.makeText(getApplicationContext(),"שינוי הסיסמה נכשל",Toast.LENGTH_SHORT).show();
                            progressBar.setVisibility(View.INVISIBLE);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error)
                {
                    progressBar.setVisibility(View.INVISIBLE);
                }
            });
            Mysingleton.getInstance(getApplicationContext()).addToRequestQueue(request);
        }
        else
        {
            Toast.makeText(getApplicationContext(),"בדוק את חיבור האינטרנט",Toast.LENGTH_SHORT).show();
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
