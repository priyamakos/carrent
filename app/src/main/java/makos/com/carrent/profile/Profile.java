package makos.com.carrent.profile;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;

import makos.com.carrent.Car_list.Previous_orders;
import makos.com.carrent.R;
import makos.com.carrent.Car_list.Total_bill;
import makos.com.carrent.upadte_payment_method.Update_payment_method;

public class Profile extends AppCompatActivity
{
    String  userid = "";

    LinearLayout order_folder;
    LinearLayout order_status;
    LinearLayout update_payment;
    LinearLayout invoice;

    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        order_folder = findViewById(R.id.order);
        order_status = findViewById(R.id.orderstatus);
        update_payment=findViewById(R.id.updatepayment);
        invoice = findViewById(R.id.invoice);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        order_status.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(Profile.this,Previous_orders.class);
                intent.putExtra("user_id",userid);
                intent.putExtra("status","2");
                intent.putExtra("stat_back","1");
                startActivity(intent);
            }
        });

        order_folder.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(Profile.this, Previous_orders.class);
                intent.putExtra("user_id",userid);
                intent.putExtra("status","1");
                intent.putExtra("stat_back","1");
                startActivity(intent);
            }
        });

        update_payment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Profile.this, Update_payment_method.class);
                intent.putExtra("user_id",userid);
                intent.putExtra("status","2");
                startActivity(intent);
            }
        });

        invoice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                //Intent intent = new Intent(Profile.this, Total_bill.class);
                //startActivity(intent);
            }
        });
        preferences = getSharedPreferences("makos.com.carrent",MODE_PRIVATE);
        editor = preferences.edit();

        userid = preferences.getString("USER_ID","0");

        Intent intent = getIntent();
        //userid = intent.getStringExtra("user_id");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
