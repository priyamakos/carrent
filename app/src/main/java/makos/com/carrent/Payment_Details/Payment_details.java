package makos.com.carrent.Payment_Details;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.inputmethodservice.KeyboardView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.Html;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.text.method.KeyListener;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.StringTokenizer;

import makos.com.carrent.Car_list.Insurance_Extra_list;
import makos.com.carrent.Car_list.Previous_orders;
import makos.com.carrent.Car_list.View_order;
import makos.com.carrent.MainActivity;
import makos.com.carrent.Mysingleton.Mysingleton;
import makos.com.carrent.NetworkInfo.Connectivity;
import makos.com.carrent.R;
import makos.com.carrent.invoice.Detailed_Activity;
import makos.com.carrent.statusurl.Staticurl;
import makos.com.carrent.upadte_payment_method.Update_payment_method;
import makos.com.carrent.upadte_payment_method.card_list_model;

public class Payment_details extends AppCompatActivity
{
    ArrayList<card_list_model> model = new ArrayList<card_list_model>();
    ArrayList<String> modelstr = new ArrayList<String>();

    String userid = "";
    String showdate = "";
    SharedPreferences preferences;
    SharedPreferences.Editor editor;

    Spinner cardlist;
    EditText name,account_no,validity,ticket_no, enterpoints,cvv;
    RadioButton pts;
    TextView points, points_amt, points_total;
    Button purchase, proceed;

    ProgressBar progressBar;

    int pos = 0;
    String s_status = "3";
    boolean stat = false;
    double amt_per_point = 0.0;
    double points_amount = 0.0;
    double commission_price = 0.0;
    double paying_amt = 0.0;
    int point = 0, point_got = 0;

    Calendar calendar;
    String returntime = "";

    String card_no = "", card_expiry = "", card_id = "";

    String selected_extra = "";
    // String userid = "";
    String orderid = "";
    String company_name = "";
    String order_date = "";
    String order_time = "";
    String return_date = "";
    String insurance_status = "0";
    double car_price = 0;
    String category_id = "";
    String selected_cars = "";
    double insuranceprice = 0;
    double extrasumprice = 0;
    double sum_bill = 0;
    double paid_amt = 0;
    double pending_amt = 0 ;
    double original_car_price_sum = 0.0;
    double comission_price = 0.0;
    double price = 0.0;

    boolean status = false;
    boolean clickstat = false;
    private DatePickerDialog datePickerDialog;

    final String[] months=new String[]{"ינואר","פברואר","מרץ","אפריל","מאי","יוני","יולי","אוגוסט","ספטמבר","אוקטובר","נובמבר","דצמבר"};

    private int year, month, day;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_details);

        progressBar = findViewById(R.id.progress_bar);

        preferences = getSharedPreferences("makos.com.carrent", MODE_PRIVATE);
        editor = preferences.edit();
        userid = preferences.getString("USER_ID", "0");

        cardlist = findViewById(R.id.card_list);
        name = findViewById(R.id.card_holder_name);
        account_no = findViewById(R.id.id);
        validity = findViewById(R.id.validity);
        ticket_no = findViewById(R.id.ticketno);

        pts = findViewById(R.id.point);
        points = findViewById(R.id.points);
        points_amt = findViewById(R.id.amt);

        purchase = findViewById(R.id.purchase);
        proceed = findViewById(R.id.proceed);

        enterpoints = findViewById(R.id.enterpoints);
        cvv = findViewById(R.id.cvv);
        points_total = findViewById(R.id.pts_amt2);

        //cvv.requestFocus();

        calendar = Calendar.getInstance();
//Locale.forLanguageTag("@calendar=hebrew")
        // @SuppressLint({"NewApi", "LocalSuppress"})GregorianCalendar calendar = new GregorianCalendar(Locale.forLanguageTag("@calendar=hebrew@calendar=hebrew"));
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);
        validity.setInputType(InputType.TYPE_NULL);
        validity.setFocusable(false);
        validity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                //setDate(999);
                setdialog();
            }
        });

        Intent intent = getIntent();
        s_status = intent.getStringExtra("status");
        // userid = intent.getStringExtra("user_id");

        Log.e("s_status",s_status);

        if (s_status.equals("3"))
        {
            company_name = intent.getStringExtra("company_name");//city name
            order_date = intent.getStringExtra("order_date");
            order_time = intent.getStringExtra("order_time");
            return_date = intent.getStringExtra("return_date");
            car_price = intent.getDoubleExtra("car_price",0);
            selected_cars = intent.getStringExtra("selected_car_id");
            insurance_status = intent.getStringExtra("insurance_status");
            insuranceprice = intent.getDoubleExtra("insurance_price",0);
            sum_bill = intent.getDoubleExtra("sum_price",0.0);
            commission_price = intent.getDoubleExtra("paid_amt",0.0);
            selected_extra = intent.getStringExtra("selected_extras");
            pending_amt = intent.getDoubleExtra("pending_amt",0.0);
            points_amt.setText(String.format("%.2f",commission_price));

        }else if (s_status.equals("2"))
        {
            sum_bill = intent.getDoubleExtra("bill_amt",0.0);
            commission_price = intent.getDoubleExtra("pending_amt",0.0);
            pending_amt = intent.getDoubleExtra("paid_amt",0.0);
            orderid = intent.getStringExtra("orderid");
            points_amt.setText(String.format("%.2f",commission_price));
        }

        Log.e("pending_amt_commission",pending_amt + " " + commission_price);




       // points_amt.setText(String.valueOf(commission_price));

       /* Intent intent1 = getIntent();
        commission_price = intent1.getDoubleExtra("paid_amt",0.0);
        Log.d("commission_price", String.valueOf(commission_price));*/

        card_list_api();

        TextWatcher textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                if (!enterpoints.getText().toString().equals(""))
                {
                    Log.e("enterpoints", String.valueOf(Integer.parseInt(enterpoints.getText().toString())));
                    if (Integer.parseInt(enterpoints.getText().toString()) <= point_got)
                    {
                        enterpoints.setEnabled(true);
                       //enterpoints.setKeyListener(null);
                        point = Integer.parseInt(enterpoints.getText().toString());
                        points_amount = amt_per_point * point;
                        points_total.setText(String.format("%.2f",points_amount));
                        Log.e("selected_position", amt_per_point + " " + point + " = " + points_amount);

                        //if (s_status.equals(""))
                        paying_amt = commission_price - points_amount;
                        Log.e("paying_amt", String.valueOf(paying_amt));
                        points_amt.setText(String.format("%.2f",paying_amt));
                        status = false;
                    }else
                    {
                        status = true;
                      //enterpoints.setMaxWidth(count);
                        enterpoints.setFilters(new InputFilter[] { new InputFilter.LengthFilter(String.valueOf(point_got).length()) });
                        Toast.makeText(getApplicationContext(),"אתה לא יכול להשתמש נקודות יותר ממה שיש לך",Toast.LENGTH_SHORT).show();
                    }
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        };

        enterpoints.addTextChangedListener(textWatcher);

            proceed.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v)
                {
                    //proceed.setOnClickListener(null);
                   /* if (clickstat == false) {
                        clickstat = true;*/
                   //proceed.setEnabled(false);
                        if (stat)
                        {
                            if (enterpoints.getText().toString().equals("") || Integer.parseInt(enterpoints.getText().toString()) <= point_got)
                            {
                                if (point_got == 0 && !enterpoints.getText().toString().equals(""))
                                {
                                    Toast.makeText(getApplicationContext(), "You do not have points", Toast.LENGTH_SHORT).show();
                                }
                                else
                                {
                                    used_points();

                                    if (account_no.getText().toString().equals(""))
                                    {
                                        Log.e("type","account_no");
                                        Toast.makeText(getApplicationContext(),"הזן מספר חשבון", Toast.LENGTH_SHORT).show();
                                    }else if (name.getText().toString().equals(""))
                                    {
                                        Log.e("type","name");
                                        Toast.makeText(getApplicationContext(),"הזן את שם בעל הכרטיס", Toast.LENGTH_SHORT).show();
                                    }else if (ticket_no.getText().toString().equals(""))
                                    {
                                        Log.e("type","ticket_no");
                                        Toast.makeText(getApplicationContext(),"הזן מספר כרטיס", Toast.LENGTH_SHORT).show();
                                    }else if (validity.getText().toString().equals(""))
                                    {
                                        Log.e("type","validity");
                                        Toast.makeText(getApplicationContext(),"הזן את תוקפו של הכרטיס", Toast.LENGTH_SHORT).show();
                                    }else if (cvv.getText().toString().equals(""))
                                    {
                                        Log.e("type","cvv");
                                        Toast.makeText(getApplicationContext(),"הזן ערך אימות כרטיס", Toast.LENGTH_SHORT).show();
                                    }else
                                    {
                                        make_payment();
                                    }
                                }
                            } else {
                                enterpoints.setFilters(new InputFilter[]{new InputFilter.LengthFilter(String.valueOf(point_got).length())});
                                Toast.makeText(getApplicationContext(), "אתה לא יכול להשתמש נקודות יותר ממה שיש לך", Toast.LENGTH_SHORT).show();
                            }
                    /*point = Integer.parseInt(enterpoints.getText().toString());
                    points_amount = amt_per_point * point;
                    Log.e("selected_position", amt_per_point + " " + point + " = " + points_amount);
                    paying_amt = commission_price - points_amount;
                    Log.e("paying_amt", String.valueOf(paying_amt));
                    points_amt.setText(String.valueOf(paying_amt));*/
                        } else {
                            paying_amt = commission_price;

                            if (account_no.getText().toString().equals(""))
                            {
                                Log.e("type","account_no");
                                Toast.makeText(getApplicationContext(),"הזן מספר חשבון", Toast.LENGTH_SHORT).show();
                            }else if (name.getText().toString().equals(""))
                            {
                                Log.e("type","name");
                                Toast.makeText(getApplicationContext(),"הזן את שם בעל הכרטיס", Toast.LENGTH_SHORT).show();
                            }else if (ticket_no.getText().toString().equals(""))
                            {
                                Log.e("type","ticket_no");
                                Toast.makeText(getApplicationContext(),"הזן מספר כרטיס", Toast.LENGTH_SHORT).show();
                            }else if (validity.getText().toString().equals(""))
                            {
                                Log.e("type","validity");
                                Toast.makeText(getApplicationContext(),"הזן את תוקפו של הכרטיס", Toast.LENGTH_SHORT).show();
                            }else if (cvv.getText().toString().equals("")) {
                                Toast.makeText(getApplicationContext(), "הזן ערך אימות כרטיס", Toast.LENGTH_SHORT).show();
                            }
                            else
                            {
                                proceed.setEnabled(false);
                                make_payment();
                            }
                        }
                   // }
                }
            });

        pts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (stat == false)
                {
                    stat = true;
               /*if (isChecked)

               {*/
                    userptsapi();
                    // }
                    pts.setChecked(true);
                    enterpoints.setEnabled(true);
                    points_amt.setText(String.format("%.2f",commission_price));
                }else
                {
                    stat = false;
                    pts.setChecked(false);
                    enterpoints.setEnabled(false);
                    points.setText("X");
                    points_total.setText("Y");
                    enterpoints.setText("");
                    //points_amt.setText("");
                }
                Log.e("selection", String.valueOf(stat));
            }
        });

        enterpoints.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event)
            {
                if (keyCode == KeyEvent.KEYCODE_DEL && enterpoints.getText().toString().equals(""))
                {
                    points_amt.setText(String.format("%.2f",commission_price));
                    points_total.setText(String.format("%.2f",0.0));
                }
                return false;
            }
        });

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    private void card_list_api()
    {
        progressBar.setVisibility(View.VISIBLE);

        if(Connectivity.isConnected(getApplicationContext())){
            model.clear();
            String card_list_url="http://rentapp-env.av3qgyvtfy.ap-south-1.elasticbeanstalk.com/rentapp/view_card_details"+"?user_id="+userid;
            Log.e("card_list_url",card_list_url);

            StringRequest stringRequest=new StringRequest(Request.Method.GET, card_list_url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.e("response",response);

                    try {
                        JSONArray jsonArray=new JSONArray(response);

                        JSONObject jsonObject=jsonArray.getJSONObject(0);

                        if(jsonObject.getString("Status").equals("True"))
                        {
                            progressBar.setVisibility(View.INVISIBLE);
                            Toast.makeText(getApplicationContext(),jsonObject.getString("Message"),Toast.LENGTH_SHORT).show();

                            JSONArray jsonArray1 = jsonObject.getJSONArray("Card Data");
                            final int[] click_stat = {0};
                            if (jsonArray1.length() == 0)
                            {
                                cardlist.setEnabled(false);
                            }else
                            {
                                //click_stat[0] = 1;
                                cardlist.setEnabled(true);
                            }
                            for (int i=0;i<jsonArray1.length();i++)
                            {
                                JSONObject jsonObject1 = jsonArray1.getJSONObject(i);
                                card_list_model models = new card_list_model();

                                models.setCard_type(jsonObject1.getString("card_type"));
                                models.setTicket_number(jsonObject1.getString("ticket_number"));
                                models.setUser_name(jsonObject1.getString("user_name"));
                                models.setCard_holder_name(jsonObject1.getString("card_holder_name"));
                                models.setValidity(jsonObject1.getString("validity"));
                                models.setSecurity_code(jsonObject1.getString("security_code"));
                                models.setCard_id(jsonObject1.getString("card_id"));
                                models.setStatus(jsonObject1.getString("status"));
                                models.setAccount_number(jsonObject1.getString("account_no"));

                                model.add(models);
                                modelstr.add(jsonObject1.getString("account_no"));
                            }
                            //adapter.notifyDataSetChanged();
                            cardlist.setAdapter(new ArrayAdapter<String>(Payment_details.this,R.layout.spiner,modelstr));

                           /* if (click_stat[0] == 1)
                            {*/
                                cardlist.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                    @Override
                                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
                                    {
                                        cvv.setText("");
                                        for (int i= 0;i<model.size();i++)
                                        {
                                            if (cardlist.getSelectedItem().toString().equals(modelstr.get(i)))
                                            {
                                                //pos = i;
                                                name.setText(Html.fromHtml(model.get(i).getCard_holder_name()));

                                                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                                                Date date = new Date();
                                                try
                                                {
                                                    date = format.parse(model.get(i).getValidity());
                                                } catch (ParseException e)
                                                {
                                                    e.printStackTrace();
                                                }

                                                format.applyPattern("MM-yy");
                                                validity.setText(Html.fromHtml(format.format(date)));
                                                account_no.setText(Html.fromHtml(model.get(i).getAccount_number()));
                                                ticket_no.setText(Html.fromHtml(model.get(i).getTicket_number()));

                                                card_id = model.get(i).getCard_id();
                                                card_no = model.get(i).getAccount_number();
                                                card_expiry = model.get(i).getValidity();
                                            }
                                        }
                                    }

                                    @Override
                                    public void onNothingSelected(AdapterView<?> parent) {
                                    }
                                });
                           /* }else
                            {
                                //card_id = model.get(i).getCard_id();
                                card_no = account_no.getText().toString();
                                card_expiry = validity.getText().toString();
                            }*/


                        }else
                        {
                            progressBar.setVisibility(View.INVISIBLE);
                            Toast.makeText(getApplicationContext(),jsonObject.getString("Message"),Toast.LENGTH_SHORT).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.INVISIBLE);
                }
            });
            Mysingleton.getInstance(Payment_details.this).addToRequestQueue(stringRequest);
        }else
        {
            Toast.makeText(getApplicationContext(),"בדוק את חיבור האינטרנט",Toast.LENGTH_SHORT).show();
        }
    }

    private void userptsapi()
    {
        progressBar.setVisibility(View.VISIBLE);
        if(Connectivity.isConnected(getApplicationContext())){

            // Log.d("userrname", String.valueOf(username)+ password);
            String userpts= Staticurl.userpoint+"?id="+userid;

            Log.e("userpts", userpts);

            StringRequest stringRequest=new StringRequest(Request.Method.GET,
                    userpts,
                    new Response.Listener<String>() {
                        @SuppressLint("ResourceAsColor")
                        @Override
                        public void onResponse(String response) {

                            Log.e("res", response);

                            try {
                                JSONArray jsonArray=new JSONArray(response);
                                JSONObject jsonObject=jsonArray.getJSONObject(0);
                                if(jsonObject.getString("Status").equals("True"))
                                {
                                    progressBar.setVisibility(View.INVISIBLE);
                                    points.setText(jsonObject.getString("User Points"));
                                    amt_per_point = Double.parseDouble(jsonObject.getString("Amount Per Points"));
                                    point_got = Integer.parseInt(jsonObject.getString("User Points"));
                                }else
                                {
                                    progressBar.setVisibility(View.INVISIBLE);
                                    //Toast.makeText(getApplicationContext(),jsonObject.getString("Message"),Toast.LENGTH_SHORT).show();
                                    points.setText("0");
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressBar.setVisibility(View.INVISIBLE);
                }
            });
            Mysingleton.getInstance(Payment_details.this).addToRequestQueue(stringRequest);
        }
        else
        {
            progressBar.setVisibility(View.INVISIBLE);
            Toast.makeText(getApplicationContext(),"בדוק את חיבור האינטרנט",Toast.LENGTH_SHORT).show();
        }
    }

    private void used_points()
    {
        //?id=12&used_points=7
        progressBar.setVisibility(View.VISIBLE);
        String used_pts = "";
        if (enterpoints.getText().toString().equals(""))
        {
            used_pts = "0";
        }
        else
        {
            used_pts = enterpoints.getText().toString();
        }
        String used_points_url = Staticurl.used_points
                + "?id=" + userid
                + "&used_points=" + used_pts;

        Log.e("used_points_url", used_points_url);

        if (Connectivity.isConnected(getApplicationContext()))
        {
            StringRequest request = new StringRequest(Request.Method.GET, used_points_url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response)
                {
                    try
                    {
                        JSONArray jsonArray = new JSONArray(response);
                        JSONObject jsonObject = jsonArray.getJSONObject(0);

                        if (jsonObject.getString("Status").equals("True"))
                        {
                            progressBar.setVisibility(View.INVISIBLE);
                            //Toast.makeText(getApplicationContext(),jsonObject.getString("Message"),Toast.LENGTH_SHORT).show();
                        }
                        else
                        {
                            progressBar.setVisibility(View.INVISIBLE);
                           //Toast.makeText(getApplicationContext(),jsonObject.getString("Message"),Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.INVISIBLE);
                }
            });
            Mysingleton.getInstance(getApplicationContext()).addToRequestQueue(request);
        }else
        {
            progressBar.setVisibility(View.INVISIBLE);
            Toast.makeText(getApplicationContext(),"בדוק את חיבור האינטרנט",Toast.LENGTH_SHORT).show();
        }
    }

    private void make_payment()
    {
        progressBar.setVisibility(View.VISIBLE);
        String paymenturl = "https://gateway20.pelecard.biz/services/DebitRegularType";

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        Date date = new Date();

        try
        {
            date = format.parse(validity.getText().toString());
        } catch (ParseException e) {
            e.printStackTrace();
        }

        format.applyPattern("MMyy");
        String expiry = format.format(date);
        Log.e("card_id_no_expiry",card_id + " " + card_no + " " + expiry);

        if (Connectivity.isConnected(getApplicationContext()))
        {
            JSONObject jsonObject = new JSONObject();
            try
            {
                jsonObject.put("terminalNumber","0962210");
                jsonObject.put("user","testpelecard3");
                jsonObject.put("password","Q3EJB8Ah");
                jsonObject.put("shopNumber","");
                jsonObject.put("creditCard",account_no.getText().toString());//5326100320033040 5326101300003698
                jsonObject.put("creditCardDateMmYy",expiry);
                jsonObject.put("token","");
                jsonObject.put("total","100"); //paying_amt
                jsonObject.put("currency","1");
                jsonObject.put("cvv2",cvv.getText().toString());//767
                jsonObject.put("id","123");
                jsonObject.put("authorizationNumber","");
                jsonObject.put("paramX","test");
                jsonObject.put("QAResultStatus","");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, paymenturl, jsonObject, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response)
                {
                    // JSONObject jsonObject = new JSONObject(response);
                    try {
                        if (response.getString("StatusCode").equals("000"))
                        {
                            progressBar.setVisibility(View.INVISIBLE);
                            Toast.makeText(getApplicationContext(),response.getString("ErrorMessage"),Toast.LENGTH_SHORT).show();//מבצע הצלחה

                            if (s_status.equals("3"))
                            {
                                make_payment_api();
                            }
                            else if (s_status.equals("2"))
                            {
                                make_payment_final();
                            }
                        }else
                        {
                            progressBar.setVisibility(View.INVISIBLE);
                            Toast.makeText(getApplicationContext(),response.getString("ErrorMessage"),Toast.LENGTH_SHORT).show();//פעולה נכשלה
                        }
                    } catch (JSONException e)
                    {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressBar.setVisibility(View.INVISIBLE);
                }
            });
            Mysingleton.getInstance(getApplicationContext()).addToRequestQueue(request);
        }
        else
        {
            progressBar.setVisibility(View.INVISIBLE);
            Toast.makeText(getApplicationContext(),"בדוק את חיבור האינטרנט",Toast.LENGTH_SHORT).show();
        }
    }

    private double make_payment_api ()
    {
        progressBar.setVisibility(View.VISIBLE);
        if (selected_extra.equals(""))
        {
            selected_extra = "0";
        }
        if (insurance_status.equals(""))
        {
            insurance_status = "0";
        }
        String orderurl = null;
        try {
            orderurl = Staticurl.make_an_order
                    + "?user_id=" + userid
                    + "&city=" + URLEncoder.encode(company_name,"UTF-8").toString()//city name
                    + "&order_date=" + order_date
                    + "&order_time=" + order_time
                    + "&return_date=" + return_date
                    + "&selected_car_ids=" + selected_cars
                    + "&authority=0"
                    + "&extra_list_ids=" + selected_extra
                    + "&insurance_status=" + insurance_status
                    + "&bill_amt=" + sum_bill
                    + "&paid_amt=" + paying_amt
                    + "&pending_amt=" + pending_amt
                    + "&bill_status=" + "0";
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        Log.e("orderurl",orderurl);

        if (Connectivity.isConnected(getApplicationContext()))
        {
            StringRequest request = new StringRequest(Request.Method.GET, orderurl, new Response.Listener<String>() {
                @Override
                public void onResponse(String response)
                {
                    Log.e("orderresponce",response);
                    try
                    {
                        JSONArray jsonArray = new JSONArray(response);
                        JSONObject jsonObject = jsonArray.getJSONObject(0);

                        if (jsonObject.getString("Status").equals("True"))
                        {
                            editor.remove("ORDER_ID");
                            editor.apply();
                            editor.putString("ORDER_ID",jsonObject.getString("Order Id"));
                            editor.commit();
                            editor.apply();

                            Toast.makeText(getApplicationContext(),"ההזמנה נוספה בהצלחה",Toast.LENGTH_SHORT).show();
                            progressBar.setVisibility(View.INVISIBLE);

                            Intent intent = new Intent(Payment_details.this, Previous_orders.class);
                            intent.setFlags(intent.getFlags() | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                            intent.putExtra("status","3");
                            intent.putExtra("stat_back","2");
                            startActivity(intent);

                        }else
                        {
                            progressBar.setVisibility(View.INVISIBLE);
                            Toast.makeText(getApplicationContext(),"הוספת ההזמנות נכשלה",Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressBar.setVisibility(View.INVISIBLE);
                }
            });
            Mysingleton.getInstance(getApplicationContext()).addToRequestQueue(request);
        }
        Log.e("sum_bill", String.valueOf(comission_price));
        return comission_price;
    }

    private void make_payment_final()
    {
        progressBar.setVisibility(View.VISIBLE);
        //?order_id=1&bill_amt=1000&paid_amt=1000&pending_amt=0&bill_status=1
        paid_amt = pending_amt;
        pending_amt = 0.0;
        String makepaymenturl = Staticurl.make_payment
                + "?order_id=" + orderid
                + "&bill_amt=" + sum_bill
                + "&paid_amt=" + commission_price
                + "&pending_amt=" + pending_amt
                + "&bill_status=" + "1";

        Log.e("makepaymenturl",makepaymenturl);

        if (Connectivity.isConnected(getApplicationContext()))
        {
            final StringRequest request = new StringRequest(Request.Method.GET, makepaymenturl, new Response.Listener<String>() {
                @Override
                public void onResponse(String response)
                {
                    Log.e("makepaymentrespnce",response);
                    try
                    {
                        JSONArray jsonArray = new JSONArray(response);
                        JSONObject jsonObject = jsonArray.getJSONObject(0);

                        if (jsonObject.getString("Status").equals("True"))
                        {
                            Toast.makeText(getApplicationContext(),"ההזמנה נוספה בהצלחה",Toast.LENGTH_SHORT).show();
                            // pay_stat = "1";
                            progressBar.setVisibility(View.INVISIBLE);
                            return_order();
                        }
                        else
                        {
                            progressBar.setVisibility(View.INVISIBLE);
                            Toast.makeText(getApplicationContext(),"הוספת ההזמנות נכשלה",Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });
            Mysingleton.getInstance(getApplicationContext()).addToRequestQueue(request);
        }
        else
        {
            Toast.makeText(getApplicationContext(),"בדוק את חיבור האינטרנט",Toast.LENGTH_SHORT).show();
        }
    }

    private void return_order()
    {
        //?order_id=1&return_time=10:30&paid_amt=1000&pending_amt=0&bill_status=1
        progressBar.setVisibility(View.VISIBLE);
        calendar = Calendar.getInstance();
        returntime = calendar.get(Calendar.HOUR_OF_DAY) + ":" + calendar.get(Calendar.MINUTE);

        String return_order_url = Staticurl.return_order
                + "?cid=" + userid
                + "&return_time=" + returntime
                + "&order_id=" + orderid
                + "&paid_amt=" + (paid_amt + paying_amt)
                + "&pending_amt=" + pending_amt
                + "&bill_status=" + "1";

        Log.e("return_order_url",return_order_url);

        if (Connectivity.isConnected(getApplicationContext()))
        {
            StringRequest request = new StringRequest(Request.Method.GET, return_order_url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response)
                {
                    Log.e("return_order_responce",response);

                    try
                    {
                        JSONArray jsonArray = new JSONArray(response);
                        JSONObject jsonObject = jsonArray.getJSONObject(0);

                        if (jsonObject.getString("Status").equals("True"))
                        {
                            progressBar.setVisibility(View.INVISIBLE);
                           // return_stat = "1";

                            editor.remove("ORDER_ID");
                            editor.apply();
                            Toast.makeText(getApplicationContext(),"ההזמנה הוחזרה בהצלחה",Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(Payment_details.this, Previous_orders.class);
                            intent.setFlags(intent.getFlags() | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                            intent.putExtra("status","1");
                            intent.putExtra("stat_back","2");
                            intent.putExtra("order_id",orderid);
                            startActivity(intent);
                        }else
                        {
                            progressBar.setVisibility(View.INVISIBLE);
                            Toast.makeText(getApplicationContext(),"החזרת ההזמנה נכשלה",Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressBar.setVisibility(View.INVISIBLE);
                }
            });
            Mysingleton.getInstance(getApplicationContext()).addToRequestQueue(request);
        }else
        {
            Toast.makeText(getApplicationContext(),"בדוק את חיבור האינטרנט",Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case android.R.id.home:

               /* if (s_status.equals("3"))
                {*/
                Intent intent = new Intent(Payment_details.this, MainActivity.class);
                intent.setFlags(intent.getFlags() | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("UserId",userid);
                startActivity(intent);
               /* }else
                {
                    finish();
                }*/

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void setDate(int id)
    {
        showDialog(id);
    }

    @SuppressLint("NewApi")
    @Override
    protected Dialog onCreateDialog(int id)
    {
        // TODO Auto-generated method stub
                datePickerDialog = new DatePickerDialog(this, myDateListener, year, month, day);
                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
                return datePickerDialog;
    }
    private DatePickerDialog.OnDateSetListener myDateListener = new DatePickerDialog.OnDateSetListener()
    {
        @SuppressLint("ResourceAsColor")
        @Override
        public void onDateSet(DatePicker arg0, int arg1, int arg2, int arg3)
        {
            // TODO Auto-generated method stub
            // arg1 = year
            // arg2 = month
            // arg3 = day
            arg0.setBackgroundColor(R.color.blackblue);
            arg0.setMinDate(System.currentTimeMillis());
                showDate(validity,arg1, arg2+1, arg3);
        }
    };

    private void showDate(EditText txt,int year, int month, int day)
    {
        txt.setText(Html.fromHtml(new StringBuilder().append(year).append("-").append(month).append("-").append(00).toString()));
    }

    private void setdialog() {

        final Dialog dialog=new Dialog(Payment_details.this);

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.activity_yearmonthpicker);

        final TextView month_name,year_name;
        Button ok,cancel;
        final NumberPicker year_picker,month_picker;
        final LinearLayout year_layout,month_layout;

        month_name=dialog.findViewById(R.id.month_name);
        year_name=dialog.findViewById(R.id.year_name);
        ok=dialog.findViewById(R.id.okbtn);
        cancel=dialog.findViewById(R.id.cancelbtn);
        year_picker=dialog.findViewById(R.id.year_picker);
        month_picker=dialog.findViewById(R.id.month_picker);
        year_layout=dialog.findViewById(R.id.linearlayout_year);
        month_layout=dialog.findViewById(R.id.linearlayout_month);

        final  String[] monthseng=new String[]{"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
        //final String[] months=new String[]{"ינואר","פברואר","מרץ","אפריל","מאי","יוני","יולי","אוגוסט","ספטמבר","אוקטובר","נובמבר","דצמבר"};

        int num=0;
        Calendar calendar=Calendar.getInstance();
        Date dates=new Date();
        dates.setTime(calendar.getTimeInMillis());
        String year="",month="";
        year=String.valueOf(calendar.get(Calendar.YEAR));
        //month= String.valueOf(calendar.get(Calendar.MONTH));

        month_picker.setWrapSelectorWheel(true);

        SimpleDateFormat simpleDateFormat=new SimpleDateFormat();
        String datestr=simpleDateFormat.format(dates);
        try {
            dates=simpleDateFormat.parse(datestr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        simpleDateFormat.applyPattern("MMMM");
        month=simpleDateFormat.format(dates);

        year_name.setText(year);
        month_name.setText(month);

        year_picker.setMinValue(calendar.get(Calendar.YEAR));
        year_picker.setMaxValue((calendar.get(Calendar.YEAR)+50));

        month_picker.setMinValue(0);
        month_picker.setMaxValue(11);

        month_picker.setDisplayedValues(months);

        /*if (status == 0)
        {
            year_picker.setValue(year_chosen);

            month_picker.setValue(month_chosen);
        }*/

        month_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                month_picker.setVisibility(View.VISIBLE);
                month_layout.setVisibility(View.VISIBLE);
                year_picker.setVisibility(View.INVISIBLE);
                year_layout.setVisibility(View.GONE);
            }
        });

        year_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                month_picker.setVisibility(View.INVISIBLE);
                month_layout.setVisibility(View.GONE);
                year_layout.setVisibility(View.VISIBLE);
                year_picker.setVisibility(View.VISIBLE);
            }
        });

        year_picker.setOnScrollListener(new NumberPicker.OnScrollListener() {
            @Override
            public void onScrollStateChange(NumberPicker view, int scrollState) {
                year_name.setText(String.valueOf(year_picker.getValue()));
            }
        });

        month_picker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                month_name.setText(months[month_picker.getValue()].toString());
               // monthstr=monthseng[month_picker.getValue()].toString();
                //num=month_picker.getValue();
            }
        });

        dialog.show();

        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Log.e("monththere", String.valueOf(month_picker.getValue()));
                showdate=year_name.getText().toString()+"-"+(month_picker.getValue()+1)+"-00";
                validity.setText(year_name.getText().toString()+"-"+month_name.getText().toString());

                dialog.dismiss();
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });
    }

    @Override
    public void onBackPressed()
    {
        /*if (s_status.equals("3"))
        {*/
        Intent intent = new Intent(Payment_details.this, MainActivity.class);
        intent.setFlags(intent.getFlags() | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("UserId",userid);
        startActivity(intent);
     /*  }else
        {
            super.onBackPressed();
        }*/
    }
}
