package makos.com.carrent.shooting_damage;

/**
 * Created by pc2 on 14-12-2018.
 */

public class order_damagelist_model {
    private String damage_id;
    private String order_id;
    private String damage_name;
    private String  damage_image;

    public String getDamage_id() {
        return damage_id;
    }

    public void setDamage_id(String damage_id) {
        this.damage_id = damage_id;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getDamage_name() {
        return damage_name;
    }

    public void setDamage_name(String damage_name) {
        this.damage_name = damage_name;
    }

    public String getDamage_image() {
        return damage_image;
    }

    public void setDamage_image(String damage_image) {
        this.damage_image = damage_image;
    }
}
