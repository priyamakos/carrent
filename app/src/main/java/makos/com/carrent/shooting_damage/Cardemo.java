package makos.com.carrent.shooting_damage;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageHelper;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import makos.com.carrent.Contact_us.Contact_Adapter;
import makos.com.carrent.Contact_us.Contact_Model;
import makos.com.carrent.Contact_us.Contactus;
import makos.com.carrent.MainActivity;
import makos.com.carrent.Mysingleton.Mysingleton;
import makos.com.carrent.NetworkInfo.Connectivity;
import makos.com.carrent.R;
import makos.com.carrent.login.Login;
import makos.com.carrent.statusurl.Staticurl;

import static android.app.Activity.RESULT_OK;


public class Cardemo extends AppCompatActivity {

    ProgressBar progressBar;
    TextView order_number;
    ImageView addimg,imgadd;
    ImageView  fuelqutimg,milesimg,car_no_img;
    static final int REQUEST_IMAGE_CAPTURE = 1;
    String mCurrentPhotoPath;
    static final int REQUEST_TAKE_PHOTO = 1;
    private final int requestCode = 100;
    RecyclerView recyclerView,recyclerView1;
    Button toview;
    RecyclerView.Adapter adapter,adapter1;
    RecyclerView.LayoutManager layoutManager,layoutManager1;
    String userid;
    String fuel_damage_id,car_no_damage_id,miles_damage_id,car_no_damage_name,fuel_damage_name,miles_damage_name;
    String order_id = "";
    int status=1;
    String value;
    RelativeLayout relativelayout;

    String remove_img;
    String carimg,fuleimg,miles_img,imgpopup;

    String removestat = "1";
    String previous_orderid = "" ,current_orderid = "";

    SharedPreferences preferences;
    SharedPreferences.Editor editor;

    ArrayList<order_damagelist_model> models = new ArrayList<order_damagelist_model>();
    ArrayList<damage_list_model> model1 = new ArrayList<damage_list_model>();
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cardemo);

        order_number=findViewById(R.id.number);

        progressBar=findViewById(R.id.progressbar);
        relativelayout=findViewById(R.id.relativelayout);
        addimg=findViewById(R.id.addimg);
        fuelqutimg = findViewById(R.id.img1);
        milesimg=findViewById(R.id.img2);
        car_no_img=findViewById(R.id.img3);

        recyclerView = findViewById(R.id.recycler);
        recyclerView1=findViewById(R.id.recycler1);

        adapter = new Order_damage_list_Adapter(Cardemo.this,models);
        layoutManager = new LinearLayoutManager(getApplicationContext());

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

        adapter1 = new damage_list_image_adapter(Cardemo.this,model1);
        layoutManager1= new GridLayoutManager(this,5);

        recyclerView1.setLayoutManager(layoutManager1);
        recyclerView1.setAdapter(adapter1);

        preferences = getSharedPreferences("makos.com.carrent",MODE_PRIVATE);
        editor = preferences.edit();

        userid = preferences.getString("USER_ID","0");

        Intent intent = getIntent();
       // userid = intent.getStringExtra("userid");
        Log.e("userid",userid);

        current_orderid = preferences.getString("ORDER_ID","0");
        Log.e("currentorderid",current_orderid);

        order_number.setText(current_orderid);
        damage_list_api();

        if (!current_orderid.equals("0"))
        {
            order_damage_list_api(current_orderid,"1");//current order id
        }

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        fuelqutimg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(order_number.getText().toString().equals("0"))
                {
                  not_add_img();

                }else {
                    value="fuel_quntity";

                    if(fuel_damage_id==null){
                        Intent fuelintent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        fuelintent.putExtra("damage_value","fuel_quntity");

                        ///  fuelintent.putExtra("damage_name","fuel_quntity");
                        startActivityForResult(fuelintent, requestCode);
                    }else {
                        Log.e("caruri",carimg);
                        popoup_img(fuleimg);
                    }
                }

         /*    //   Intent fuelintent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                 Intent fuelintent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                fuelintent.putExtra("damage_value","fuel_quntity");

              ///  fuelintent.putExtra("damage_name","fuel_quntity");
                startActivityForResult(fuelintent, requestCode);*/
            }
        });

        milesimg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(order_number.getText().toString().equals("0")){
                    not_add_img();
                }else {
                    value="miles";
                    // Intent milesintent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    if (miles_damage_id==null) {
                        Intent milesintent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        milesintent.putExtra("damage_value", "miles");
                        // milesintent.putExtra("damage_name","miles");
                        startActivityForResult(milesintent, requestCode);
                    }else {
                        Log.e("milesuri",miles_img);
                        popoup_img(miles_img);
                    }
                }

            }
        });

        car_no_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(order_number.getText().toString().equals("0")){
                    not_add_img();
                }else {
                    value="car no";
                    //  Intent carnointent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    if(car_no_damage_id==null) {
                        Intent carnointent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        carnointent.putExtra("damage_value", "car no");
                        //  carnointent.putExtra("damage_name","car no");
                        startActivityForResult(carnointent, requestCode);
                    }else {
                        Log.e("fueluri",fuleimg);
                        popoup_img(carimg);
                    }
                }

            }
        });

        addimg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(order_number.getText().toString().equals("0")){
                    not_add_img();
                }else {
                    value="damage";
                    // Intent damageintent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                    Intent damageintent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    damageintent.putExtra("damage_value", "damage");
                    //  carnointent.putExtra("damage_name","car no");
                    startActivityForResult(damageintent, requestCode);
                }


            }
        });

        milesimg.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                Log.e("Long click","long click");
                remove_img="miles_img";

                popoup("miles_img");
                return true;
            }
        });

        car_no_img.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                remove_img="car_no_img";

                popoup("car_no_img");
                return true;
            }
        });

        fuelqutimg.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                remove_img="fuel_img";
                popoup("fuel_img");
                return true;
            }
        });

        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), recyclerView,
                new RecyclerTouchListener.ClickListener() {
                    @SuppressLint("ResourceAsColor")
                    @Override
                    public void onClick(View view, final int position)
                    {
                        previous_orderid = models.get(position).getOrder_id();
                       toview=view.findViewById(R.id.toview);
                      // toremove=view.findViewById(R.id.remove);
                       toview.setOnClickListener(new View.OnClickListener() {
                           @Override
                           public void onClick(View v) {
                                addimg.setVisibility(View.INVISIBLE);
                               Toast.makeText(getApplicationContext(),"orderno"+models.get(position).getOrder_id(),Toast.LENGTH_SHORT).show();
                              order_id=models.get(position).getOrder_id();
                               order_number.setText(order_id);
                               order_damage_list_api(order_id,"1");//previous order id
                           }
                       });

                      /*  fuelqutimg.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if(status==2)
                                {
                                    order_id=models.get(position).getOrder_id();
                                    remove_api(fuel_damage_id,order_id,"fuel");
                                      //  status=1;
                                }
                            }
                        });
                        car_no_img.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if(status==2)
                                {
                                    order_id=models.get(position).getOrder_id();
                                   // Log.e("car_da_id",car_no_damage_id);
                                        remove_api(car_no_damage_id, order_id,"carno");
                                       // status = 1;

                                }
                            }
                        });
                        milesimg.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if(status==2)
                                {
                                    order_id=models.get(position).getOrder_id();
                                   // Log.e("mile",miles_damage_id);
                                        remove_api(miles_damage_id, order_id,"miles");
                                       // status = 1;
                                }
                            }
                        });

                        milesimg.setOnLongClickListener(new View.OnLongClickListener() {
                            @Override
                            public boolean onLongClick(View v) {
                                Log.e("Long click","long click");
                                return true;
                            }
                        });

                       toremove.setOnClickListener(new View.OnClickListener() {
                           @Override
                           public void onClick(View v) {
                               popoup();
                           }
                       });*/
                    }
                    @Override
                    public void onLongClick(View view, int position)
                    {

                    }

                }));

        recyclerView1.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), recyclerView1,
                new RecyclerTouchListener.ClickListener() {
                    @SuppressLint("ResourceAsColor")
                    @Override
                    public void onClick(View view, final int position)
                    {
                       /* Toast.makeText(getApplicationContext(),model1.get(position).getDamage_id()+" " +model1.get(position).getOrder_id(),Toast.LENGTH_SHORT).show();

                        if(status==2)// && order_id != orderid in shared preference
                        {
                            remove_api(model1.get(position).getDamage_id(),model1.get(position).getOrder_id(),"damage");
                        }*/

                       if(!(model1.get(position).getDamage_id() ==null)){
                        final Dialog dialog = new Dialog(Cardemo.this);
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialog.setCancelable(true);
                        dialog.setContentView(R.layout.view_image);
                        ImageView img=dialog.findViewById(R.id.imageview);

                        Picasso.with(getApplicationContext()).load(model1.get(position).getDamage_img()).fit().placeholder(R.drawable.ic_camera_alt_black_24dp).error(R.drawable.carlogo).into(img);

                        dialog.show();
                       }
                    }
                    @Override
                    public void onLongClick(View view, final int position)
                    {

                        final Dialog dialog = new Dialog(Cardemo.this);
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialog.setCancelable(true);
                        dialog.setContentView(R.layout.remove_image);
                        Button ok=dialog.findViewById(R.id.ok);

                        ok.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                remove_api(model1.get(position).getDamage_id(),model1.get(position).getOrder_id(),"damage");
                                dialog.dismiss();
                            }
                        });
                        dialog.show();
                    }
                }));
    }

    private void not_add_img() {
        final Dialog dialog=new Dialog(Cardemo.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.notaddimage);
        Button ok=dialog.findViewById(R.id.ok);

        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private void popoup_img(String imgpopup) {
        final Dialog dialog = new Dialog(Cardemo.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.view_image);
        ImageView img=dialog.findViewById(R.id.imageview);

        Log.e("imgpopup",imgpopup);
        Picasso.with(getApplicationContext()).load(imgpopup).fit().placeholder(R.drawable.ic_camera_alt_black_24dp).error(R.drawable.carlogo).into(img);

        dialog.show();
    }

    private void popoup(final String r_img)
    {
        final Dialog dialog = new Dialog(Cardemo.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.remove_image);
        Button ok=dialog.findViewById(R.id.ok);


        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(r_img.equals("miles_img")){
                    remove_api(miles_damage_id,current_orderid,"miles");
                    dialog.dismiss();
                }else if(r_img.equals("car_no_img")){
                    remove_api(car_no_damage_id,current_orderid,"carno");
                    dialog.dismiss();
                }else if(r_img.equals("fuel_img")){
                    remove_api(fuel_damage_id,current_orderid,"fuel");
                    dialog.dismiss();
                }
              // remove_api(current_orderid);
            }
        });
        dialog.show();
    }

    private static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private Cardemo.RecyclerTouchListener.ClickListener clickListener;

        public RecyclerTouchListener(Context applicationContext, final RecyclerView recyclerView, final Cardemo.RecyclerTouchListener.ClickListener clickListener)
        {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(applicationContext, new GestureDetector.SimpleOnGestureListener()
            {
                @Override
                public boolean onSingleTapUp(MotionEvent e)
                {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e)
                {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null)
                    {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }
        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e)
        {
            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e))
            {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }
        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e)
        {
        }
        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept)
        {
        }
        public interface ClickListener
        {
            void onClick(View view, int position);
            void onLongClick(View view, int position);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (this.requestCode == requestCode && resultCode == RESULT_OK) {
           Log.e("data", String.valueOf(data.getExtras().get("data")));
            Bitmap bitmap = (Bitmap) data.getExtras().get("data");
          //  img1.setImageBitmap(bitmap);
            uploadBitmap(bitmap);


       /*     Uri imageUri = data.getData();
          Log.e("uri", String.valueOf(imageUri));
            try
            {
                //getting bitmap object from uri
               Bitmap  bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), imageUri);
               // Bitmap bitmap = MediaStore.Images.Media.getBitmap();
                //displaying selected image to imageview
               // Intent in=getIntent();

                Log.e("value",value);
             /*   if("fuel_quntity".equals(data.getStringExtra("damage_value"))){
                    fuelqutimg.setImageBitmap(bitmap);
                }else if("miles".equals(data.getStringExtra("damage_value"))){
                    milesimg.setImageBitmap(bitmap);
                }else if("car no".equals(data.getStringExtra("damage_value"))){
                    car_no_img.setImageBitmap(bitmap);
                }*/
          /*      if(value.equals("fuel_quntity")){
                    fuelqutimg.setImageBitmap(bitmap);
                }else if(value.equals("miles")){
                    milesimg.setImageBitmap(bitmap);
                }else if(value.equals("car no")){
                    car_no_img.setImageBitmap(bitmap);
                }else {}*/



                //fuelqutimg.setImageBitmap(bitmap);

                //calling the method uploadBitmap to upload image
             /*   Log.e("bitmap", String.valueOf(bitmap));
                uploadBitmap(bitmap);
            } catch (IOException e)
            {
                e.printStackTrace();
            }*/
        }
    }

    private void uploadBitmap(final Bitmap bitmap)
    {
        String targeturl ="http://rentapp-env.av3qgyvtfy.ap-south-1.elasticbeanstalk.com/fileUploadServlet";
        Log.e("updateprofimg", targeturl);

        progressBar.setVisibility(View.VISIBLE);
        //our custom volley request
        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST, targeturl,
                new Response.Listener<NetworkResponse>() {

                    @Override
                    public void onResponse(NetworkResponse response) {
                        try {
                            Log.e("response", String.valueOf(response));
                            JSONArray jsonArray = new JSONArray(new String(response.data));

                            JSONObject jsonObject = jsonArray.getJSONObject(0);
                            if (jsonObject.getString("Status").equals("True")) {
                                Toast.makeText(getApplicationContext(),"התמונה הועלתה בהצלחה",Toast.LENGTH_SHORT).show();
                                order_damage_list_api(current_orderid,"1");
                                progressBar.setVisibility(View.INVISIBLE);

                            } else {
                                Toast.makeText(getApplicationContext(), "תמונת הנזק אינה נטענת", Toast.LENGTH_SHORT).show();
                                progressBar.setVisibility(View.INVISIBLE);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }) {

            /*
             * If you want to add more parameters with the image
             * you can do it here
             * here we have only one parameter with the image
             * which is tags
             * */
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();


               if(value.equals("fuel_quntity")){
                    params.put("damage_name", "fuel_quntity");
               }else if(value.equals("miles")){
                    params.put("damage_name", "miles");
              }else if(value.equals("car no")){
                    params.put("damage_name", "car no");
                }else {
                   params.put("damage_name", "damage");
               }

                //params.put("damage_name", "fuel_quntity");
                Log.e("currentoid",current_orderid);
                params.put("order_id",current_orderid);
                return params;
            }

            /*
             * Here we are passing image by renaming it with a unique name
             * */
            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                long imagename = System.currentTimeMillis();
                params.put("file", new DataPart(imagename + ".png", getFileDataFromDrawable(bitmap)));
              //  Log.e("bitmap", String.valueOf(bitmap));
               // params.put("file", new DataPart( getFileDataFromDrawable(bitmap)));
                return params;
            }
        };

        //adding the request to volley
        Volley.newRequestQueue(this).add(volleyMultipartRequest);
    }

    public byte[] getFileDataFromDrawable(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 80, byteArrayOutputStream);
        Log.e("array", String.valueOf(byteArrayOutputStream.toByteArray()));
        return byteArrayOutputStream.toByteArray();
    }

    private void damage_list_api(){

            String damageurl = Staticurl.damage_list+"?user_id="+userid;

            Log.e("damageurl",damageurl);
            progressBar.setVisibility(View.VISIBLE);
            if (Connectivity.isConnected(getApplicationContext()))
            {
                StringRequest  request = new StringRequest(Request.Method.GET, damageurl, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response)
                    {
                        try
                        {
                            Log.e("damageres",response);
                            JSONArray jsonArray = new JSONArray(response);
                            JSONObject jsonObject = jsonArray.getJSONObject(0);

                            if (jsonObject.getString("Status").equals("True"))
                            {
                                JSONArray jsonArray1 = jsonObject.getJSONArray("Order Id List Data");

                                for (int i = 0; i < jsonArray1.length() ; i ++)
                                {
                                    order_damagelist_model model = new order_damagelist_model();

                                    model.setOrder_id(jsonArray1.get(i).toString());

                                    models.add(model);
                                }
                                progressBar.setVisibility(View.INVISIBLE);
                                adapter.notifyDataSetChanged();
                            }
                            else {
                                progressBar.setVisibility(View.INVISIBLE);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressBar.setVisibility(View.INVISIBLE);
                    }
                });
                Mysingleton.getInstance(getApplicationContext()).addToRequestQueue(request);
            }
            else
            {
                Toast.makeText(getApplicationContext(),"בדוק את החיבור לאינטרנט",Toast.LENGTH_SHORT).show();
            }
        }


    private void order_damage_list_api(String order_id, final String stat) {

        String damage_image = Staticurl.order_damage_list
                + "?order_id="+order_id;

        Log.e("damage_image",damage_image);
        model1.clear();
        car_no_img.setImageResource(R.drawable.ic_camera_alt_black_24dp);
        fuelqutimg.setImageResource(R.drawable.ic_camera_alt_black_24dp);
        milesimg.setImageResource(R.drawable.ic_camera_alt_black_24dp);

        progressBar.setVisibility(View.VISIBLE);
        if (Connectivity.isConnected(getApplicationContext()))
        {
            final StringRequest  request = new StringRequest(Request.Method.GET, damage_image, new Response.Listener<String>() {
                @Override
                public void onResponse(String response)
                {
                    try
                    {
                        Log.e("damagereslist",response);
                        JSONArray jsonArray = new JSONArray(response);
                        JSONObject jsonObject = jsonArray.getJSONObject(0);

                        if (jsonObject.getString("Status").equals("True"))
                        {
                            JSONArray jsonArray1 = jsonObject.getJSONArray("Order Wise Damage List Data");


                            for (int i = 0; i < jsonArray1.length() ; i ++) {

                                damage_list_model model = new damage_list_model();

                                JSONObject jsonObject1 = jsonArray1.getJSONObject(i);

                                if (jsonObject1.getString("damage_name").equals("car no")) {

                                    car_no_damage_id = jsonObject1.getString("damage_id");
                                    car_no_damage_name=jsonObject1.getString("damage_name");
                                    carimg=jsonObject1.getString("damage_image");
                                    Log.e("uricar_no",carimg);
                                    Picasso.with(getApplicationContext()).load(jsonObject1.getString("damage_image")).fit().placeholder(R.drawable.ic_camera_alt_black_24dp).error(R.drawable.carlogo).into(car_no_img);
                                    Log.e("damagename",car_no_damage_name);

                                } else {

                                    if (car_no_img.getDrawable() == getResources().getDrawable(R.drawable.ic_camera_alt_black_24dp)) {
                                        car_no_img.setImageResource(R.drawable.ic_camera_alt_black_24dp);
                                        Log.e("damageid",car_no_damage_id);
                                    }
                                }
                                if (jsonObject1.getString("damage_name").equals("fuel_quntity")) {

                                    fuel_damage_id = jsonObject1.getString("damage_id");
                                    fuel_damage_name=jsonObject1.getString("damage_name");
                                    fuleimg=jsonObject1.getString("damage_image");
                                    Log.e("f_id", fuel_damage_id);
                                    Picasso.with(getApplicationContext()).load(jsonObject1.getString("damage_image")).fit().placeholder(R.drawable.ic_camera_alt_black_24dp).error(R.drawable.carlogo).into(fuelqutimg);
                                    Log.e("fuelname",fuel_damage_name);

                                } else {
                                    if (fuelqutimg.getDrawable() == getResources().getDrawable(R.drawable.ic_camera_alt_black_24dp)) {
                                        fuelqutimg.setImageResource(R.drawable.ic_camera_alt_black_24dp);
                                        Log.e("fuelid",fuel_damage_id);
                                    }
                                }
                                if (jsonObject1.getString("damage_name").equals("miles")) {
                                    miles_damage_id = jsonObject1.getString("damage_id");
                                    miles_damage_name=jsonObject1.getString("damage_name");
                                    miles_img=jsonObject1.getString("damage_image");
                                    Picasso.with(getApplicationContext()).load(jsonObject1.getString("damage_image")).fit().placeholder(R.drawable.ic_camera_alt_black_24dp).error(R.drawable.carlogo).into(milesimg);
                                    Log.e("milename",miles_damage_name);
                                } else {
                                    if (milesimg.getDrawable() == getResources().getDrawable(R.drawable.ic_camera_alt_black_24dp)) {
                                        milesimg.setImageResource(R.drawable.ic_camera_alt_black_24dp);
                                        Log.e("milesid",miles_damage_id);
                                    }
                                }
                                if (jsonObject1.getString("damage_name").equals("damage"))
                                {
                                    Log.e("damage","damage");
                                    model.setDamage_name(jsonObject1.getString("damage_name"));
                                    model.setDamage_img(jsonObject1.getString("damage_image"));
                                    model.setDamage_id(jsonObject1.getString("damage_id"));
                                    model.setOrder_id(jsonObject1.getString("order_id"));
                                    model1.add(model);
                                }

                            }
                            progressBar.setVisibility(View.INVISIBLE);
                            adapter1.notifyDataSetChanged();
                        }
                        else {
                            progressBar.setVisibility(View.INVISIBLE);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressBar.setVisibility(View.INVISIBLE);
                }
            });
            Mysingleton.getInstance(getApplicationContext()).addToRequestQueue(request);
        }
        else
        {
            Toast.makeText(getApplicationContext(),"בדוק את החיבור לאינטרנט",Toast.LENGTH_SHORT).show();
        }

    }

    private void remove_api(String damage_id, final String order_id, final String damage_name) {
        String removeurl = Staticurl.remove_image
                + "?order_id="+order_id+
                "&damage_id="+damage_id;
        Log.e("removeurl",removeurl);

        progressBar.setVisibility(View.VISIBLE);
        if (Connectivity.isConnected(getApplicationContext()))
        {
            StringRequest  request = new StringRequest(Request.Method.GET, removeurl, new Response.Listener<String>() {
                @Override
                public void onResponse(String response)
                {
                    try
                    {
                        JSONArray jsonArray = new JSONArray(response);
                        JSONObject jsonObject = jsonArray.getJSONObject(0);

                        if (jsonObject.getString("Status").equals("True"))
                        {
                           Toast.makeText(getApplicationContext(),"תמונת הנזק מוסרת",Toast.LENGTH_SHORT).show();

                           removestat = "0";
                            status = 1;

                            if (damage_name.equals("fuel"))
                            {
                                fuel_damage_id=null;
                                fuelqutimg.setImageResource(R.drawable.ic_camera_alt_black_24dp);
                            }else if (damage_name.equals("carno"))
                            {
                                car_no_damage_id=null;
                                car_no_img.setImageResource(R.drawable.ic_camera_alt_black_24dp);
                            }else if (damage_name.equals("miles"))
                            {
                                miles_damage_id=null;
                                milesimg.setImageResource(R.drawable.ic_camera_alt_black_24dp);
                            }
                            order_damage_list_api(order_id,"2");
                            progressBar.setVisibility(View.INVISIBLE);
                        }else
                            {
                            progressBar.setVisibility(View.INVISIBLE);
                            Snackbar snackbar=Snackbar.make(relativelayout,"התמונה אינה קיימת",Snackbar.LENGTH_SHORT);
                            snackbar.show();
                           // Toast.makeText(getApplicationContext(),jsonObject.getString("Message"),Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressBar.setVisibility(View.INVISIBLE);

                }
            });
            Mysingleton.getInstance(getApplicationContext()).addToRequestQueue(request);
        }
        else
        {
            Toast.makeText(getApplicationContext(),"בדוק את החיבור לאינטרנט",Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}



