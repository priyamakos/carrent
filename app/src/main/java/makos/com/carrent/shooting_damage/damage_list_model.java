package makos.com.carrent.shooting_damage;

/**
 * Created by pc2 on 15-12-2018.
 */

public class damage_list_model {
    private String damage_id;
    private String order_id;
    private String damage_name;
    private String damage_img;

    public String getDamage_id() {
        return damage_id;
    }

    public void setDamage_id(String damage_id) {
        this.damage_id = damage_id;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getDamage_name() {
        return damage_name;
    }

    public void setDamage_name(String damage_name) {
        this.damage_name = damage_name;
    }

    public String getDamage_img() {
        return damage_img;
    }

    public void setDamage_img(String damage_img) {
        this.damage_img = damage_img;
    }
}
