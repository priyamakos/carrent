package makos.com.carrent.shooting_damage;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

import makos.com.carrent.Contact_us.Contact_Adapter;
import makos.com.carrent.Contact_us.Contact_Model;
import makos.com.carrent.R;

/**
 * Created by pc2 on 14-12-2018.
 */

public class Order_damage_list_Adapter extends RecyclerView.Adapter<Order_damage_list_Adapter.Damage_Holder> {

    Activity activity;
    ArrayList<order_damagelist_model> models = new ArrayList<order_damagelist_model>();

    public Order_damage_list_Adapter(Activity activity, ArrayList<order_damagelist_model> models)
    {
        this.activity = activity;
        this.models = models;
    }

    @NonNull
    @Override
    public Damage_Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.oder_damage_list,viewGroup,false);
        Damage_Holder holder = new Order_damage_list_Adapter.Damage_Holder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull Damage_Holder damage_holder, int i) {

        TextView orderno = damage_holder.orderno;

        orderno.setText(models.get(i).getOrder_id());


    }

    @Override
    public int getItemCount() {
        return models.size();
    }


    public static class Damage_Holder extends RecyclerView.ViewHolder
    {
        TextView orderno;

        public Damage_Holder(@NonNull View itemView)
        {
            super(itemView);

            orderno = itemView.findViewById(R.id.orderno2);


        }
    }
}

