package makos.com.carrent.shooting_damage;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import makos.com.carrent.R;

/**
 * Created by pc2 on 15-12-2018.
 */

public class damage_list_image_adapter extends RecyclerView.Adapter<damage_list_image_adapter.Damage_image_holder> {

        Activity activity;
        ArrayList<damage_list_model> models = new ArrayList<damage_list_model>();

public damage_list_image_adapter(Activity activity, ArrayList<damage_list_model> models)
        {
        this.activity = activity;
        this.models = models;
        }

@NonNull
@Override
public Damage_image_holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.damage_image,viewGroup,false);
        Damage_image_holder holder = new damage_list_image_adapter.Damage_image_holder(view);
        return holder;
        }

@Override
public void onBindViewHolder(@NonNull Damage_image_holder damage_holder, int i) {

      // TextView damage_name = damage_holder.damage_name;

   // damage_name.setText(models.get(i).getDamage_name());

    ImageView damage_img=damage_holder.imageView;
    damage_img.setImageResource(R.drawable.ic_camera_alt_black_24dp);
    Log.e("hii",models.get(i).getDamage_name());
 //   Picasso.with(activity).load(models.get(i).getDamage_img()).fit().placeholder(R.drawable.carlogo).error(R.drawable.carlogo).into(damage_img);
    if(!models.get(i).getDamage_name().equals("car no") && !models.get(i).getDamage_name().equals("fuel_quntity") && !models.get(i).getDamage_name().equals("miles"))
    {
        Picasso.with(activity).load(models.get(i).getDamage_img()).fit().placeholder(R.drawable.carlogo).error(R.drawable.carlogo).into(damage_img);
    }

}

@Override
public int getItemCount() {

    return models.size();

}


public static class Damage_image_holder extends RecyclerView.ViewHolder
{
    ImageView imageView;
    TextView damage_name;

    public Damage_image_holder(@NonNull View itemView)
    {
        super(itemView);
        imageView=itemView.findViewById(R.id.damage_img);
        damage_name=itemView.findViewById(R.id.damage_name);
    }
}
}
