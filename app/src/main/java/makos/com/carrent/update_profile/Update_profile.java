package makos.com.carrent.update_profile;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.BidiFormatter;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import makos.com.carrent.Mysingleton.Mysingleton;
import makos.com.carrent.NetworkInfo.Connectivity;
import makos.com.carrent.R;
import makos.com.carrent.statusurl.Staticurl;

public class Update_profile extends AppCompatActivity {
    EditText username,address,emailid;
    String userid;
    ProgressBar progressBar;

    SharedPreferences preferences;
    SharedPreferences.Editor editor;

    String demo;
    Button save;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_profile);


        username=findViewById(R.id.username);
        address=findViewById(R.id.address);
        emailid=findViewById(R.id.emailid);
        save=findViewById(R.id.save);
        progressBar=findViewById(R.id.progressbar);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        preferences = getSharedPreferences("makos.com.carrent",MODE_PRIVATE);
        editor = preferences.edit();

        userid = preferences.getString("USER_ID","0");

        Intent in=getIntent();
       // userid=in.getStringExtra("userid");

        getprofile();
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                validation();

            }
        });

    }

    private void getprofile() {

        if(Connectivity.isConnected(getApplicationContext())){

            String get_profile_url= Staticurl.get_useprofile+"?user_id="+userid;

            Log.e("url",get_profile_url);
            StringRequest request = new StringRequest(Request.Method.GET, get_profile_url, new Response.Listener<String>() {
                @SuppressLint("NewApi")
                @Override
                public void onResponse(String response)
                {
                    try
                    {
                        Log.e("response",response);
                        JSONArray jsonArray = new JSONArray(response);
                        JSONObject jsonObject = jsonArray.getJSONObject(0);

                        if (jsonObject.getString("Status").equals("True"))
                        {
                            JSONArray jsonArray1=jsonObject.getJSONArray("User Profile Data");
                           JSONObject jsonObject1=jsonArray1.getJSONObject(0);



                                username.setText(Html.fromHtml(jsonObject1.getString("name")));

                            address.setText(Html.fromHtml(jsonObject1.getString("address")));
                           emailid.setText(Html.fromHtml(jsonObject1.getString("email_id")));
                            progressBar.setVisibility(View.INVISIBLE);
                        }else {
                            progressBar.setVisibility(View.INVISIBLE);
                            Toast.makeText(getApplicationContext(),"עדכון הפרופיל נכשל",Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressBar.setVisibility(View.INVISIBLE);
                }
            });
            Mysingleton.getInstance(getApplicationContext()).addToRequestQueue(request);

        }else {
            Toast.makeText(getApplicationContext(),"בדוק את החיבור לאינטרנט",Toast.LENGTH_SHORT).show();
        }

    }

    private void validation() {

        if(username.getText().toString().equals("")){
            Toast.makeText(getApplicationContext(),"הכנס שם משתמש",Toast.LENGTH_SHORT).show();
        }else if(emailid.getText().toString().equals("")){
            Toast.makeText(getApplicationContext(),"הזן מזהה דוא\"ל",Toast.LENGTH_SHORT).show();
        }else if(isValidMail(emailid.getText().toString()))
        {
             if(address.getText().toString().equals(""))
             {
                Toast.makeText(getApplicationContext(),"הכנס כתובת",Toast.LENGTH_SHORT).show();
             }else {
                 update_api();
             }

        }
    }

    private boolean isValidMail(String email) {
        boolean check;
        Pattern p;
        Matcher m;

        String EMAIL_STRING = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        p = Pattern.compile(EMAIL_STRING);

        m = p.matcher(email);
        check = m.matches();

        if(!check) {
            Toast.makeText(getApplicationContext(),"הזן מזהה דוא\"ל חוקי",Toast.LENGTH_SHORT).show();
            // txtEmail.setError("Not Valid Email");
        }
        return check;
    }
    @SuppressLint("NewApi")
    private void update_api() {
        String update_url = null;
        try {


            //demo=URLEncoder.encode(username.getText().toString(),"UTF-8");
           // demo=Html.toHtml(username.getText());
            update_url = Staticurl.update_profile
                    +"?id="+userid+
                    "&username="+ URLEncoder.encode(Html.toHtml(username.getText()),"UTF-8").toString().trim()+
                    "&address="+ URLEncoder.encode(Html.toHtml(address.getText()),"UTF-8").toString().trim()+
                    "&email_id="+URLEncoder.encode(emailid.getText().toString(),"UTF-8").toString().trim();


        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }


        Log.e("removeurl",update_url);

        progressBar.setVisibility(View.VISIBLE);

        if (Connectivity.isConnected(getApplicationContext()))
        {
            StringRequest request = new StringRequest(Request.Method.GET, update_url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response)
                {
                    try
                    {
                        JSONArray jsonArray = new JSONArray(response);
                        JSONObject jsonObject = jsonArray.getJSONObject(0);
                        Log.e("resup",response);
                        if (jsonObject.getString("Status").equals("True"))
                        {
                            Toast.makeText(getApplicationContext(),"הפרופיל עודכן בהצלחה",Toast.LENGTH_SHORT).show();
                            getprofile();
                            progressBar.setVisibility(View.INVISIBLE);
                        }else {

                            Toast.makeText(getApplicationContext(),"עדכון הפרופיל נכשל",Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });



            Mysingleton.getInstance(getApplicationContext()).addToRequestQueue(request);
        }
        else
        {
            Toast.makeText(getApplicationContext(),"בדוק את החיבור לאינטרנט",Toast.LENGTH_SHORT).show();
        }

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
