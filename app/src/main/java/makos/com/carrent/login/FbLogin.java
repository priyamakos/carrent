package makos.com.carrent.login;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.Arrays;

import makos.com.carrent.MainActivity;
import makos.com.carrent.R;

/**
 * Created by pc2 on 04-12-2018.
 */

public class FbLogin extends AppCompatActivity {

    private static final String TAG = "FacebookLogin";
    public SharedPreferences sharedPreferences;
    // [END declare_auth]
    String gmailEmail, targetUrl;
    SharedPreferences.Editor editor;
    // [START declare_auth]
    private FirebaseAuth mAuth;
    private CallbackManager mCallbackManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fbloginlayout);
        FacebookSdk.sdkInitialize(getApplicationContext());

        // [START initialize_auth]
        // Initialize Firebase Auth
        mAuth = FirebaseAuth.getInstance();
        // [END initialize_auth]

        // [START initialize_fblogin]
        // Initialize Facebook Login button
        mCallbackManager = CallbackManager.Factory.create();

        LoginManager.getInstance().registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.d(TAG, "facebook:onSuccess:" + loginResult);
                handleFacebookAccessToken(loginResult.getAccessToken());
            }

            @Override
            public void onCancel() {
                Log.d(TAG, "facebook:onCancel");
                // [START_EXCLUDE]
                updateUI(null);
                // [END_EXCLUDE]
            }

            @Override
            public void onError(FacebookException error) {
                Log.d(TAG, "facebook:onError", error);
                // [START_EXCLUDE]
                updateUI(null);
                // [END_EXCLUDE]
            }

        });
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("email"));
    }

    // [START on_start_check_user]
    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        //FirebaseUser currentUser = mAuth.getCurrentUser();
        // updateUI(currentUser);
    }

    // [END on_start_check_user]

    // [START on_activity_result]
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Pass the activity result back to the Facebook SDK
        super.onActivityResult(requestCode, resultCode, data);
        mCallbackManager.onActivityResult(requestCode, resultCode, data);
    }
    // [END on_activity_result]

    // [START auth_with_facebook]
    private void handleFacebookAccessToken(AccessToken token)
    {
        Log.d(TAG, "handleFacebookAccessToken:" + token);
        // [START_EXCLUDE silent]
        //showProgressDialog();
        // [END_EXCLUDE]

        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>()
                {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task)
                    {
                      //  if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                           // Log.e("tag", "signInWithCredential:success");
                            Log.e("operation","success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            Log.e("user", String.valueOf(user));
                            updateUI(user);

                       /* } else {
                            // If sign in fails, display a message to the user.
                            Log.e("operation","fail");
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            Toast.makeText(FbLogin.this, "Authentication failed: You Already Register User", Toast.LENGTH_SHORT).show();
                            updateUI(null);
                            Intent intent = new Intent(getApplicationContext(), Login.class);
                            startActivity(intent);
                        }*/

                        // [START_EXCLUDE]
                        //  hideProgressDialog();
                        // [END_EXCLUDE]
                    }
                });
    }

    private void updateUI(FirebaseUser user) {
        Log.e("updateUI", "updateUI call");

        //hideProgressDialog();

       // if (user != null) {

            Log.e("email: ", user.getEmail());
            Log.e("Uid: ", user.getUid());

            gmailEmail = user.getEmail();

            //gmailDetails.setEmail(user.getEmail());
            //gmailDetails.setPersonname(user.getDisplayName());
     /*   }
        else {
            Toast.makeText(getApplicationContext(),"Not found",Toast.LENGTH_SHORT).show();
        }*/
    }

}
