package makos.com.carrent.login;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import makos.com.carrent.Account_Register.Register;
import makos.com.carrent.Forgot_password.Forget_Password;
import makos.com.carrent.MainActivity;
import makos.com.carrent.Mysingleton.Mysingleton;
import makos.com.carrent.NetworkInfo.Connectivity;
import makos.com.carrent.R;
import makos.com.carrent.statusurl.Staticurl;


public class Login extends AppCompatActivity implements LoginPresenter.loginView {

    Button loginbtn,registration;
    EditText username,password;
    Spinner spinner;
    Locale myLocale;
    TextView forgotpass;
    LoginButton loginButton;
    String currentLanguage = "Hebrew", currentLang;
    String userid = "";
    LinearLayout fblogin;
    ProgressBar progressBar;
    String gmailEmail, targetUrl;
    public SharedPreferences preferences;
    SharedPreferences.Editor editor;
    String name;
//RelativeLayout layout3;
    private static final String TAG = "FacebookLogin";

    private TextView mStatusTextView;
    private TextView mDetailTextView;

    // [START declare_auth]
    private FirebaseAuth mAuth;
    // [END declare_auth]

    private CallbackManager mCallbackManager;

    private Handler handler;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        username=findViewById(R.id.username);
        password=findViewById(R.id.password);
        registration=findViewById(R.id.registration);
        fblogin=findViewById(R.id.layout3);
        progressBar=findViewById(R.id.progressbar);
        FacebookSdk.sdkInitialize(getApplicationContext());
      //  mStatusTextView = findViewById(R.id.status);
      //  mDetailTextView = findViewById(R.id.detail);
        mAuth = FirebaseAuth.getInstance();
        // [END initialize_auth]
       // layout3 = findViewById(R.id.layout3);
        LoginManager.getInstance().logOut();
        // [START initialize_fblogin]
        // Initialize Facebook Login button
        mCallbackManager = CallbackManager.Factory.create();
        loginButton = findViewById(R.id.btnlogin);
        preferences = getSharedPreferences("makos.com.carrent",MODE_PRIVATE);
        editor = preferences.edit();
     /* layout3.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v) {
              loginButton.performClick();
          }
      });*/
        forgotpass = findViewById(R.id.forgotpass);

        forgotpass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(Login.this, Forget_Password.class);
                startActivity(intent);
            }
        });

        registration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(Login.this, Register.class);
                startActivity(intent);
            }
        });

   fblogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(Login.this);
                builder.setMessage("?קראתי ואני מסכים עם התנאים וההגבלות").setPositiveButton("כן", dialogClickListener)
                        .setNegativeButton("לא", dialogClickListener).show();

          //      Intent intent=new Intent(getApplicationContext(),FbLogin.class);
            //    startActivity(intent);
            }
        });


        loginButton.setReadPermissions("email", "public_profile");
        loginButton.registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                // Log.d(TAG, "facebook:onSuccess:" + loginResult);
                handleFacebookAccessToken(loginResult.getAccessToken());
            }

            @Override
            public void onCancel() {
                //Log.d(TAG, "facebook:onCancel");
                // ...
            }

            @Override
            public void onError(FacebookException error) {
                //    Log.d(TAG, "facebook:onError", error);
                // ...
            }
        });

        // useridString.valueOf(username.getText());
         //passwrd= String.valueOf(password.getText());
        loginbtn = findViewById(R.id.loginbtn);

        loginbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

               /* Intent intent=new Intent(Login.this,MainActivity.class);
                startActivity(intent);*/
               // validation();
                if(username.getText().toString().equals("")){
                    Toast.makeText(getApplicationContext(),"הכנס שם משתמש",Toast.LENGTH_SHORT).show();
                }else if(isValidMail(username.getText().toString()))
                {
                    if (password.getText().toString().equals("")) {
                        Toast.makeText(getApplicationContext(), "הזן את הסיסמה", Toast.LENGTH_SHORT).show();
                    } else {
                        login();
                    }
                }

            }
        });

        currentLanguage = getIntent().getStringExtra(currentLang);

        spinner = findViewById(R.id.spinner);

        List<String> list = new ArrayList<String>();

        list.add("Select language");
        list.add("English");
        list.add("Hebrew");

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, list);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                switch (position) {
                    case 0:
                        break;
                    case 1:
                        setLocale("en");
                        break;
                    case 2:
                        setLocale("Hebrew");
                        break;
                    /*case 3:
                        setLocale("fr");
                        break;
                    case 4:
                        setLocale("hi");
                        break;*/
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
    }
    DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            switch (which){
                case DialogInterface.BUTTON_POSITIVE:
                    loginButton.performClick();
                    //Yes button clicked
                    break;

                case DialogInterface.BUTTON_NEGATIVE:
                    Toast.makeText(Login.this,"Please Accept Our Policy",Toast.LENGTH_SHORT).show();
                    //No button clicked
                    break;
            }
        }
    };
    private void handleFacebookAccessToken(AccessToken token) {
        Log.d(TAG, "handleFacebookAccessToken:" + token);

        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithCredential:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            Log.d("emailId",user.getEmail());
                           // email.setText(user.getEmail());
                         //   name.setText(user.getDisplayName());
                            updateUI(user);
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                          //  Toast.makeText(MainActivity.this, "Authentication failed.",
                               //     Toast.LENGTH_SHORT).show();
                            //  updateUI(null);
                        }

                        // ...
                    }
                });
    }


        private boolean isValidMail(String email) {
            boolean check;
            Pattern p;
            Matcher m;

            String EMAIL_STRING = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                    + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

            p = Pattern.compile(EMAIL_STRING);

            m = p.matcher(email);
            check = m.matches();

            if(!check) {
                Toast.makeText(getApplicationContext(),"הזן מזהה דוא\"ל חוקי",Toast.LENGTH_SHORT).show();
                // txtEmail.setError("Not Valid Email");
            }
            return check;
        }

    private void login(){
        progressBar.setVisibility(View.VISIBLE);
        if(Connectivity.isConnected(getApplicationContext())){

            Log.d("userrname", String.valueOf(username)+ password);
            String login_auth= null;
            try {
                login_auth = Staticurl.login+
                        "?email_id="+ URLEncoder.encode(username.getText().toString(),"UTF-8").toString()+
                           "&password="+URLEncoder.encode(Html.toHtml(password.getText()),"UTF-8").toString();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

            Log.e("login", login_auth);


            StringRequest stringRequest=new StringRequest(Request.Method.GET,
                    login_auth,
                     new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {

                            Log.e("loginres", response);

                            try {
                                JSONArray jsonArray = new JSONArray(response);
                                final JSONObject jsonObject = jsonArray.getJSONObject(0);
                                if (jsonObject.getString("Status").equals("True")) {
                                    userid = jsonObject.getString("User ID");


                                    Toast.makeText(getApplicationContext(),"התחבר בהצלחה",Toast.LENGTH_SHORT).show();
                                    editor.clear();
                                    editor.apply();
                                    editor.putString("USER_ID", userid);
                                    editor.putBoolean("STATUS", true);
                                    editor.commit();
                                    editor.apply();
                                    Intent intent = new Intent(Login.this, MainActivity.class);
                                    intent.putExtra("UserId", userid);
                                    startActivity(intent);

                                    /*handler = new Handler();
                                    handler.postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            try {
                                                Toast.makeText(getApplicationContext(), jsonObject.getString("Message"), Toast.LENGTH_SHORT).show();
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }

                                        }
                                    }, 2000);*/
                                    progressBar.setVisibility(View.INVISIBLE);
                                } else
                                {
                                    Toast.makeText(getApplicationContext(),"ההתחברות נכשלה, בדוק את שם המשתמש והסיסמה",Toast.LENGTH_SHORT).show();
                                    progressBar.setVisibility(View.INVISIBLE);
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }



                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressBar.setVisibility(View.INVISIBLE);

                }
            });
            Mysingleton.getInstance(Login.this).addToRequestQueue(stringRequest);

        }

        else {
            Toast.makeText(getApplicationContext(), "בדוק את החיבור לאינטרנט", Toast.LENGTH_SHORT).show();
        }
 }

    public void setLocale(String localeName) {
        if (!localeName.equals(currentLanguage)) {
            myLocale = new Locale(localeName);
            Resources res = getResources();
            DisplayMetrics dm = res.getDisplayMetrics();
            Configuration conf = res.getConfiguration();
            conf.locale = myLocale;
            res.updateConfiguration(conf, dm);
            Intent refresh = new Intent(this, Login.class);
            refresh.putExtra(currentLang, localeName);
            startActivity(refresh);
        } else {
           // Toast.makeText(Login.this, "Language already selected!", Toast.LENGTH_SHORT).show();
        }
    }

    public void onBackPressed() {
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
        System.exit(0);

       }

    @Override
    public void showProgressDialog() {

    }

    @Override
    public void hideProgressDiaglog() {

    }

    @Override
    public void onloginsucess(int flag) {

    }

    @Override
    public void onloginfailed(int flag) {

    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Pass the activity result back to the Facebook SDK
        super.onActivityResult(requestCode, resultCode, data);
        mCallbackManager.onActivityResult(requestCode, resultCode, data);
    }
    private void updateUI(FirebaseUser user) {
        Log.e("updateUI", "updateUI call");

        //hideProgressDialog();

        // if (user != null) {

        Log.e("email: ", user.getEmail());
        Log.e("Uid: ", user.getUid());

        gmailEmail = user.getEmail();
        name =  user.getDisplayName();
        register_api(gmailEmail,name,user.getUid());

        //gmailDetails.setEmail(user.getEmail());
        //gmailDetails.setPersonname(user.getDisplayName());
     /*   }
        else {
            Toast.makeText(getApplicationContext(),"Not found",Toast.LENGTH_SHORT).show();
        }*/
    }
    private void register_api(String Email,String username,final String userid) {
        progressBar.setVisibility(View.VISIBLE);
        if(Connectivity.isConnected(getApplicationContext())){

            Log.d("userrname", String.valueOf(username)+ password);
            String reg_auth= Staticurl.register+
                    "?username="+username.replace(" ","%20")+
                    "&address="+
                    "&email_id="+Email+
                    "&password=demo"+"&login_through=1" +
                    "&policy=1";

            Log.d("login", reg_auth);

            StringRequest stringRequest=new StringRequest(Request.Method.GET,
                    reg_auth,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {

                            try {
                                JSONArray jsonArray=new JSONArray(response);
                                Log.e("response",response);
                                JSONObject jsonObject=jsonArray.getJSONObject(0);
                                if(jsonObject.getString("Status").equals("True"))
                                {

                                    Log.e("id",jsonObject.getString("User_id"));
                                    editor.clear();
                                    editor.apply();
                                    editor.putString("USER_ID", jsonObject.getString("User_id"));
                                    editor.putBoolean("STATUS", true);
                                    editor.commit();
                                    editor.apply();
                                    Intent intent = new Intent(Login.this, MainActivity.class);
                                    intent.putExtra("UserId", userid);
                                    startActivity(intent);
                                }
                                else
                                {
                                    editor.clear();
                                    editor.apply();
                                    editor.putString("USER_ID", jsonObject.getString("User_id"));
                                    editor.putBoolean("STATUS", true);
                                    editor.commit();
                                    editor.apply();
                                    Intent intent = new Intent(Login.this, MainActivity.class);
                                    intent.putExtra("UserId", jsonObject.getString("User_id"));
                                    startActivity(intent);
                                    progressBar.setVisibility(View.INVISIBLE);
                                    Toast.makeText(getApplicationContext(),"הרשמה נכשלה",Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e)
                            {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressBar.setVisibility(View.INVISIBLE);
                }
            });
            Mysingleton.getInstance(Login.this).addToRequestQueue(stringRequest);
        }
        else
        {
            Toast.makeText(getApplicationContext(), "בדוק את החיבור לאינטרנט", Toast.LENGTH_SHORT).show();
        }
    }

}
