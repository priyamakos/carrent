package makos.com.carrent.points;

/**
 * Created by pc2 on 03-12-2018.
 */

public class Points_model {

    private String points_desc;
    private String points_amt;

    public String getPoints_desc() {
        return points_desc;
    }

    public void setPoints_desc(String points_desc) {
        this.points_desc = points_desc;
    }

    public String getPoints_amt() {
        return points_amt;
    }

    public void setPoints_amt(String points_amt) {
        this.points_amt = points_amt;
    }



}
