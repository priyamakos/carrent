package makos.com.carrent.points;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import makos.com.carrent.MainActivity;
import makos.com.carrent.Mysingleton.Mysingleton;
import makos.com.carrent.NetworkInfo.Connectivity;
import makos.com.carrent.R;
import makos.com.carrent.profile.Profile;
import makos.com.carrent.statusurl.Staticurl;

public class Points extends AppCompatActivity {

    TextView msg;
    RecyclerView recyclerView;
    RecyclerView.Adapter adapter;
    RecyclerView.LayoutManager layoutManager;
    Button back_to_profile,back_to_lobby;
    ArrayList<Points_model>models;
    ProgressBar progressBar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_points);


        progressBar=findViewById(R.id.progressbar);
        msg=findViewById(R.id.nopts);
        back_to_lobby=findViewById(R.id.bck_to_lobby);
        back_to_profile=findViewById(R.id.bck_to_profile);

        models=new ArrayList<Points_model>();
        recyclerView=findViewById(R.id.recycler);

        adapter=new Points_adapter(Points.this,models);
        layoutManager=new LinearLayoutManager(getApplicationContext());

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

        Points_api();

        back_to_lobby.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent in= new Intent(getApplicationContext(), MainActivity.class);
                startActivity(in);
            }
        });

        back_to_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in= new Intent(getApplicationContext(), Profile.class);
                startActivity(in);
            }
        });

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    private void Points_api() {

        progressBar.setVisibility(View.VISIBLE);
        if(Connectivity.isConnected(getApplicationContext())){

            final String points_url= Staticurl.pointdetails;

            StringRequest stringRequest=new StringRequest(Request.Method.GET, points_url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    try {
                        JSONArray jsonArray=new JSONArray(response);
                        JSONObject jsonObject=jsonArray.getJSONObject(0);

                        if(jsonObject.getString("Status").equals("True")){
                            JSONArray jsonArray1=jsonObject.getJSONArray("Point Data");

                            for (int i=0;i<jsonArray1.length();i++){
                                JSONObject jsonObject1=jsonArray1.getJSONObject(i);
                                Points_model model=new Points_model();

                                model.setPoints_desc(jsonObject1.getString("points_desc"));
                                model.setPoints_amt(jsonObject1.getString("points_amt"));

                                models.add(model);
                            }
                            progressBar.setVisibility(View.INVISIBLE);
                            adapter.notifyDataSetChanged();
                        }
                        else
                        {
                            progressBar.setVisibility(View.INVISIBLE);
                            Toast.makeText(getApplicationContext(),jsonObject.getString("Message"),Toast.LENGTH_SHORT).show();
                        }
                        if (models.size() == 0)
                        {
                            msg.setVisibility(View.VISIBLE);
                        }else
                        {
                            msg.setVisibility(View.INVISIBLE);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressBar.setVisibility(View.INVISIBLE);
                }
            });

            Mysingleton.getInstance(getApplicationContext()).addToRequestQueue(stringRequest);

        }else {
            Toast.makeText(getApplicationContext(),"Check Internet Connection",Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
