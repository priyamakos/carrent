package makos.com.carrent.points;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import makos.com.carrent.R;

/**
 * Created by pc2 on 03-12-2018.
 */

public class Points_adapter extends RecyclerView.Adapter<Points_adapter.Points_View>{

    Activity activity;
    ArrayList<Points_model> models=new ArrayList<Points_model>();

    public Points_adapter(Activity activity,ArrayList<Points_model> models){

        this.activity=activity;
        this.models=models;
    }

    @NonNull
    @Override
    public Points_View onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view= LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.customdesign_pts,viewGroup,false);
        Points_View points_view=new Points_View(view);
        return points_view;
    }

    @Override
    public void onBindViewHolder(@NonNull Points_View points_view, int i) {
      TextView pts_desc=points_view.pts_desc;
      TextView pts_amts=points_view.pts_amts;

      pts_desc.setText(Html.fromHtml(models.get(i).getPoints_desc()));
      pts_amts.setText(Html.fromHtml(models.get(i).getPoints_amt()));

    }

    @Override
    public int getItemCount() {
        return models.size();
    }

    public static class Points_View extends RecyclerView.ViewHolder{
        TextView pts_desc,pts_amts;

        public Points_View(@NonNull View itemView) {
            super(itemView);
            pts_desc=itemView.findViewById(R.id.pts_desc);
            pts_amts=itemView.findViewById(R.id.pts_amt);
        }
    }
}
