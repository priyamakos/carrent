package makos.com.carrent.invoice;

import java.util.ArrayList;

import makos.com.carrent.Car_list.Car_List_Model;

/**
 * Created by PC1 on 03-12-2018.
 */

public class Invice_Model
{
    private String Invoice_No;
    private String Return_Time;
    private String Insurance;
    private String Order_No;
    private String Bill_Amount;
    private String Order_Date;
    private String Return_Date;
    private String Order_Time;
    private String car;
    private String extra;

    /*public ArrayList<Car_List_Model> car = new ArrayList<Car_List_Model>();
    public ArrayList<Extras_Model> extra = new ArrayList<Extras_Model>();*/

    public String getCar() {
        return car;
    }

    public void setCar(String car) {
        this.car = car;
    }

    public String getExtra() {
        return extra;
    }

    public void setExtra(String extra) {
        this.extra = extra;
    }

    public String getInvoice_No() {
        return Invoice_No;
    }

    public void setInvoice_No(String invoice_No) {
        Invoice_No = invoice_No;
    }

    public String getReturn_Time() {
        return Return_Time;
    }

    public void setReturn_Time(String return_Time) {
        Return_Time = return_Time;
    }

    public String getInsurance() {
        return Insurance;
    }

    public void setInsurance(String insurance) {
        Insurance = insurance;
    }

    public String getOrder_No() {
        return Order_No;
    }

    public void setOrder_No(String order_No) {
        Order_No = order_No;
    }

    public String getBill_Amount() {
        return Bill_Amount;
    }

    public void setBill_Amount(String bill_Amount) {
        Bill_Amount = bill_Amount;
    }

    public String getOrder_Date() {
        return Order_Date;
    }

    public void setOrder_Date(String order_Date) {
        Order_Date = order_Date;
    }

    public String getReturn_Date() {
        return Return_Date;
    }

    public void setReturn_Date(String return_Date) {
        Return_Date = return_Date;
    }

    public String getOrder_Time() {
        return Order_Time;
    }

    public void setOrder_Time(String order_Time) {
        Order_Time = order_Time;
    }
}
