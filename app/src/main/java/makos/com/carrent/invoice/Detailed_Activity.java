package makos.com.carrent.invoice;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Adapter;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import makos.com.carrent.Car_list.Car_List_Model;
import makos.com.carrent.Car_list.Previous_orders;
import makos.com.carrent.Mysingleton.Mysingleton;
import makos.com.carrent.NetworkInfo.Connectivity;
import makos.com.carrent.R;
import makos.com.carrent.statusurl.Staticurl;

public class Detailed_Activity extends AppCompatActivity
{
    TextView invoice_no,return_time,insurance,order_no,bill_amt,order_date,return_date,order_time,insurance_title,insurance_price;

    RecyclerView carrecycler;
    RecyclerView extrarecycler;
    RecyclerView.Adapter caradapter;
    RecyclerView.Adapter extraadapter;
    RecyclerView.LayoutManager carlayout;
    RecyclerView.LayoutManager extralayout;
    ProgressBar progressBar;

    SharedPreferences preferences;
    SharedPreferences.Editor editor;

    String stat = "0";

    String car = "",extra = "", userid = "",orderid = "";
    ArrayList<Car_List_Model> carlist = new ArrayList<Car_List_Model>();
    ArrayList<Extras_Model> extralist = new ArrayList<Extras_Model>();

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detailed_invoice_list);

        progressBar=findViewById(R.id.progressbar);
        invoice_no = findViewById(R.id.invoice_no);
        return_time = findViewById(R.id.return_time);
        insurance = findViewById(R.id.insurance);
        order_no = findViewById(R.id.orderno);
        bill_amt = findViewById(R.id.billamt);
        order_date = findViewById(R.id.order_date);
        return_date = findViewById(R.id.return_date);
        order_time = findViewById(R.id.order_time);
        insurance_title = findViewById(R.id.insurance_title);
        insurance_price = findViewById(R.id.insurance_price);

        preferences = getSharedPreferences("makos.com.carrent",MODE_PRIVATE);
        editor = preferences.edit();

        userid = preferences.getString("USER_ID","0");

        Intent intent = getIntent();
        //userid = intent.getStringExtra("user_id");
        orderid = intent.getStringExtra("order_id");
        stat = intent.getStringExtra("status");

        carrecycler = findViewById(R.id.carrecycler);
        caradapter = new Detailed_Car_Adapter(Detailed_Activity.this,carlist);
        carlayout = new LinearLayoutManager(getApplicationContext());
        carrecycler.setLayoutManager(carlayout);
        carrecycler.setAdapter(caradapter);

        extrarecycler = findViewById(R.id.extrarecycler);
        extraadapter = new Detailed_Extras_Adapter(Detailed_Activity.this,extralist);
        extralayout = new LinearLayoutManager(getApplicationContext());
        extrarecycler.setLayoutManager(extralayout);
        extrarecycler.setAdapter(extraadapter);

        invoice_list();

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    private void invoice_list()
    {
        String invoiceurl = Staticurl.orderwise_invoice + "?order_id=" + orderid;

        Log.e("Invoiceurl",invoiceurl);
        progressBar.setVisibility(View.VISIBLE);
        if (Connectivity.isConnected(getApplicationContext()))
        {
            StringRequest request = new StringRequest(Request.Method.GET, invoiceurl, new Response.Listener<String>() {
                @Override
                public void onResponse(String response)
                {
                    Log.d("invoiceresponcce",response);
                    try
                    {
                        JSONArray jsonArray = new JSONArray(response);

                        JSONObject jsonObject = jsonArray.getJSONObject(0);
                        if (jsonObject.getString("Status").equals("True"))
                        {
                            JSONArray jsonArray1 = jsonObject.getJSONArray("Data");
                                for (int i = 0; i<jsonArray1.length();i++)
                                {
                                    JSONObject jsonObject1 = jsonArray1.getJSONObject(i);
                                    Invice_Model model = new Invice_Model();

                                    Log.d("Datalist",jsonObject1.getString("Invoice No"));
                                    invoice_no.setText(Html.fromHtml(jsonObject1.getString("Invoice No")).toString());
                                    return_time.setText(Html.fromHtml(jsonObject1.getString("Return Time")).toString());
                                    if (jsonObject1.getString("Insurance").equals("Yes"))
                                    {
                                        insurance.setText("כן");
                                    }else
                                    {
                                        insurance.setText("לא");
                                    }
                                    order_no.setText(Html.fromHtml(jsonObject1.getString("Order No")).toString());
                                    bill_amt.setText(Html.fromHtml(jsonObject1.getString("Bill Amount")).toString() + "₪");
                                    order_date.setText(Html.fromHtml(jsonObject1.getString("Order Date")).toString());
                                    return_date.setText(Html.fromHtml(jsonObject1.getString("Return Date")).toString());
                                    order_time.setText(Html.fromHtml(jsonObject1.getString("Order Time")).toString());
                                    insurance_title.setText(String.format("%.2f",Double.parseDouble(Html.fromHtml(jsonObject1.getString("Insurance Title")).toString())));
                                    insurance_price.setText(Html.fromHtml(jsonObject1.getString("Insurance Amount")).toString());

                                    JSONArray jsonArray2 = jsonObject1.getJSONArray("Car List");
                                    model.setCar(String.valueOf(jsonArray2));

                                    for (int j = 0; j< jsonArray2.length();j++)
                                    {
                                        JSONObject jsonObject2 = jsonArray2.getJSONObject(j);

                                        Log.e("carlist",jsonObject2.getString("car_name"));
                                        Car_List_Model invoice = new Car_List_Model();
                                        invoice.setCar_name(jsonObject2.getString("car_name"));
                                        invoice.setCompany_name(jsonObject2.getString("car_company"));
                                        invoice.setCar_category(jsonObject2.getString("car_category"));
                                        invoice.setPrice(jsonObject2.getString("list_price"));
                                        invoice.setImg(jsonObject2.getString("image_path"));

                                        carlist.add(invoice);
                                    }
                                    caradapter.notifyDataSetChanged();

                                    JSONArray jsonArray3 = jsonObject1.getJSONArray("Extra List");
                                    model.setExtra(String.valueOf(jsonArray3));
                                    Log.e("carshow", String.valueOf(jsonArray2));

                                    for (int k = 0; k < jsonArray3.length(); k++)
                                    {
                                        JSONObject jsonObject2 = jsonArray3.getJSONObject(k);
                                        Extras_Model extra = new Extras_Model();

                                        Log.d("extralist",jsonObject2.getString("extra_name"));

                                        extra.setExtra_name(jsonObject2.getString("extra_name"));
                                        extra.setPrice(jsonObject2.getString("price"));

                                        extralist.add(extra);
                                    }
                                    progressBar.setVisibility(View.INVISIBLE);
                                    extraadapter.notifyDataSetChanged();

                                    //inviceModel.add(model);
                                }

                            // Log.e("inviocelistcount", String.valueOf(inviceModel.size()));
                        }
                        else
                        {
                            progressBar.setVisibility(View.INVISIBLE);
                            Toast.makeText(getApplicationContext(),"ללא חשבונית",Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressBar.setVisibility(View.INVISIBLE);
                }
            });
            Mysingleton.getInstance(getApplicationContext()).addToRequestQueue(request);
        }
        else
        {
            Toast.makeText(getApplicationContext(),"בדוק את חיבור האינטרנט",Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
