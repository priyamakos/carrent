package makos.com.carrent.invoice;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import makos.com.carrent.Car_list.Car_List_Model;
import makos.com.carrent.R;

/**
 * Created by PC1 on 06-12-2018.
 */

public class Detailed_Car_Adapter extends RecyclerView.Adapter<Detailed_Car_Adapter.Detailed_Car_Holder>
{
    Activity activity;
    ArrayList<Car_List_Model> car = new ArrayList<Car_List_Model>();

    public Detailed_Car_Adapter(Activity activity, ArrayList<Car_List_Model> car)
    {
        this.activity = activity;
        this.car = car;
    }

    @NonNull
    @Override
    public Detailed_Car_Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i)
    {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.detailed_car_list,viewGroup,false);
        Detailed_Car_Holder holder = new Detailed_Car_Holder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull Detailed_Car_Holder holder, int position)
    {
        TextView carname = holder.carname;
        TextView carcomapny = holder.carcomany;
        TextView carcategory = holder.carcategory;
        TextView carprice = holder.listprice;
        ImageView car_img = holder.car_img;

        carname.setText(Html.fromHtml(car.get(position).getCar_name()));
        carcomapny.setText(Html.fromHtml(car.get(position).getCompany_name()));
        carcategory.setText(Html.fromHtml(car.get(position).getCar_category()));
        carprice.setText(String.format("%.2f",Double.parseDouble(Html.fromHtml(car.get(position).getPrice()).toString())) + "₪");

        //car_img.setAlpha(.75f);
        Picasso.with(activity).load(car.get(position).getImg()).placeholder(R.drawable.carlogo).error(R.drawable.carlogo).into(car_img);
    }

    @Override
    public int getItemCount()
    {
        return car.size();
    }

    public static class Detailed_Car_Holder extends RecyclerView.ViewHolder
    {
        TextView carname,carcategory,carcomany,listprice;
        ImageView car_img;
        public Detailed_Car_Holder(@NonNull View itemView)
        {
            super(itemView);
            carname = itemView.findViewById(R.id.carname);
            carcomany = itemView.findViewById(R.id.company);
            carcategory = itemView.findViewById(R.id.category);
            listprice = itemView.findViewById(R.id.price);
            car_img = itemView.findViewById(R.id.img);
        }
    }
}
