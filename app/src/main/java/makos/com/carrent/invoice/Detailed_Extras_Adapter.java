package makos.com.carrent.invoice;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import makos.com.carrent.R;

/**
 * Created by PC1 on 06-12-2018.
 */

public class Detailed_Extras_Adapter extends RecyclerView.Adapter<Detailed_Extras_Adapter.Detailed_Extra_Holder>
{
    Activity activity;
    ArrayList<Extras_Model> extra = new ArrayList<Extras_Model>();

    public Detailed_Extras_Adapter(Activity activity, ArrayList<Extras_Model> extra)
    {
        this.activity = activity;
        this.extra = extra;
    }

    @NonNull
    @Override
    public Detailed_Extra_Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i)
    {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.detailed_extra_list,viewGroup,false);
        Detailed_Extra_Holder holder = new Detailed_Extra_Holder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull Detailed_Extra_Holder holder, int position)
    {
        TextView extraname = holder.extraname;
        TextView extraprice = holder.extraprice;

        extraname.setText(extra.get(position).getExtra_name());
        extraprice.setText(extra.get(position).getPrice() + "₪");
    }

    @Override
    public int getItemCount()
    {
        return extra.size();
    }


    public static class Detailed_Extra_Holder extends RecyclerView.ViewHolder
    {
        TextView extraname,extraprice;
        public Detailed_Extra_Holder(@NonNull View itemView)
        {
            super(itemView);
             extraname = itemView.findViewById(R.id.extra_name);
             extraprice = itemView.findViewById(R.id.price);
        }
    }
}
