package makos.com.carrent.invoice;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;

import makos.com.carrent.Car_list.Car_List_Model;
import makos.com.carrent.R;

/**
 * Created by PC1 on 03-12-2018.
 */

public class Invoice_Adapter extends RecyclerView.Adapter<Invoice_Adapter.Invoice_View>
{
    Activity activity;
    ArrayList<Invice_Model> model = new ArrayList<Invice_Model>();
    ArrayList<Car_List_Model> car = new ArrayList<Car_List_Model>();
    ArrayList<Extras_Model> extra = new ArrayList<Extras_Model>();

    public Invoice_Adapter(Activity activity, ArrayList<Invice_Model> model, ArrayList<Car_List_Model> car, ArrayList<Extras_Model> extra)
    {
        this.activity = activity;
        this.model = model;
        this.car = car;
        this.extra = extra;
    }

    @NonNull
    @Override
    public Invoice_View onCreateViewHolder(@NonNull ViewGroup viewGroup, int i)
    {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.custom_invoice_list,viewGroup,false);
        Invoice_View holder = new Invoice_View(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull Invoice_View holder, final int position)
    {
        TextView orderno = holder.orderno;
        TextView company = holder.company;
        TextView total = holder.total;
        TextView view = holder.view;

        orderno.setText(model.get(position).getOrder_No());
        company.setText(car.get(position).getCompany_name());
        total.setText(model.get(position).getBill_Amount());

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(activity,Detailed_Activity.class);
                intent.putExtra("Invoice_No",model.get(position).getInvoice_No());
                intent.putExtra("Return_Time",model.get(position).getReturn_Time());
                intent.putExtra("Insurance",model.get(position).getInsurance());
                intent.putExtra("Order_No",model.get(position).getOrder_No());
                intent.putExtra("Bill_Amount",model.get(position).getBill_Amount());
                intent.putExtra("Order_Date",model.get(position).getOrder_Date());
                intent.putExtra("Return_Date",model.get(position).getReturn_Date());
                intent.putExtra("Order_Time",model.get(position).getOrder_Time());
                intent.putExtra("car",model.get(position).getCar());
                intent.putExtra("extra",model.get(position).getExtra());
                activity.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount()
    {
        return model.size();
    }

    public static class Invoice_View extends RecyclerView.ViewHolder
    {
        TextView orderno,company,total,view;
        public Invoice_View(@NonNull View itemView)
        {
            super(itemView);
            orderno = itemView.findViewById(R.id.orderno);
            company = itemView.findViewById(R.id.company);
            total = itemView.findViewById(R.id.total);
            view = itemView.findViewById(R.id.view);
        }
    }
}
