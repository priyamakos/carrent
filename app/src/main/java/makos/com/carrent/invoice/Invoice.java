package makos.com.carrent.invoice;

import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import makos.com.carrent.Car_list.Car_List_Model;
import makos.com.carrent.Mysingleton.Mysingleton;
import makos.com.carrent.NetworkInfo.Connectivity;
import makos.com.carrent.R;
import makos.com.carrent.statusurl.Staticurl;

import static android.content.Context.MODE_PRIVATE;

public class Invoice extends Fragment
{

    RecyclerView recyclerView;
    RecyclerView.Adapter adapter;
    RecyclerView.LayoutManager layoutManager;

    ArrayList<Car_List_Model> car = new ArrayList<Car_List_Model>();
    ArrayList<Invice_Model> inviceModel = new ArrayList<Invice_Model>();
    ArrayList<Extras_Model> extras = new ArrayList<Extras_Model>();

    SharedPreferences preferences;
    SharedPreferences.Editor editor;

    String userid = "";
    TextView msg;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.activity_invoice, container, false);

        Bundle bundle = getArguments();

        //userid = bundle.getString("userid");

        preferences = getActivity().getSharedPreferences("makos.com.carrent",MODE_PRIVATE);
        editor = preferences.edit();

        userid = preferences.getString("USER_ID","0");

        Log.e("useridhere",userid);

        recyclerView = view.findViewById(R.id.recycler);
        msg = view.findViewById(R.id.msg);

        adapter = new Invoice_Adapter(getActivity(),inviceModel,car,extras);
        layoutManager = new LinearLayoutManager(getActivity());

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

        invoice_list();

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);
        getActivity().setTitle("חשבוניות");
    }

    private void invoice_list()
    {
        String invoiceurl = Staticurl.invoice + "?cid=" + userid;

        Log.e("Invoiceurl",invoiceurl);
        if (Connectivity.isConnected(getActivity()))
        {
            StringRequest request = new StringRequest(Request.Method.GET, invoiceurl, new Response.Listener<String>() {
                @Override
                public void onResponse(String response)
                {
                    Log.d("invoiceresponcce",response);
                    try
                    {
                        JSONArray jsonArray = new JSONArray(response);

                        JSONObject jsonObject = jsonArray.getJSONObject(0);
                        if (jsonObject.getString("Status").equals("True"))
                        {
                            JSONArray jsonArray1 = jsonObject.getJSONArray("Data");

                            if (jsonArray1.length() == 0)
                            {
                                msg.setVisibility(View.VISIBLE);
                            }
                            else
                            {
                                msg.setVisibility(View.INVISIBLE);
                                for (int i = 0; i<jsonArray1.length();i++)
                                {
                                    JSONObject jsonObject1 = jsonArray1.getJSONObject(i);
                                    Invice_Model model = new Invice_Model();

                                    Log.d("Datalist",jsonObject1.getString("Invoice No"));
                                    model.setInvoice_No(jsonObject1.getString("Invoice No"));
                                    model.setReturn_Time(jsonObject1.getString("Return Time"));
                                    model.setInsurance(jsonObject1.getString("Insurance"));
                                    model.setOrder_No(jsonObject1.getString("Order No"));
                                    model.setBill_Amount(jsonObject1.getString("Bill Amount"));
                                    model.setOrder_Date(jsonObject1.getString("Order Date"));
                                    model.setReturn_Date(jsonObject1.getString("Return Date"));
                                    model.setOrder_Time(jsonObject1.getString("Order Time"));

                                    JSONArray jsonArray2 = jsonObject1.getJSONArray("Car List");
                                    model.setCar(String.valueOf(jsonArray2));

                                    for (int j = 0; j< jsonArray2.length();j++)
                                    {
                                        JSONObject jsonObject2 = jsonArray2.getJSONObject(j);

                                        Log.e("carlist",jsonObject2.getString("car_name"));
                                        Car_List_Model invoice = new Car_List_Model();
                                        invoice.setCar_name(jsonObject2.getString("car_name"));
                                        invoice.setCompany_name(jsonObject2.getString("car_company"));
                                        invoice.setCar_category(jsonObject2.getString("car_category"));
                                        invoice.setPrice(jsonObject2.getString("list_price"));

                                        car.add(invoice);
                                    }

                                    JSONArray jsonArray3 = jsonObject1.getJSONArray("Extra List");
                                    model.setExtra(String.valueOf(jsonArray3));
                                    Log.e("carshow", String.valueOf(jsonArray2));

                                    for (int k = 0; k < jsonArray3.length(); k++)
                                    {
                                        JSONObject jsonObject2 = jsonArray3.getJSONObject(k);
                                        Extras_Model extra = new Extras_Model();

                                        Log.d("extralist",jsonObject2.getString("extra_name"));

                                        extra.setExtra_name(jsonObject2.getString("extra_name"));
                                        extra.setPrice(jsonObject2.getString("price"));

                                        extras.add(extra);
                                    }
                                    inviceModel.add(model);
                                }
                                adapter.notifyDataSetChanged();

                                Log.e("inviocelistcount", String.valueOf(inviceModel.size()));
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });
            Mysingleton.getInstance(getActivity()).addToRequestQueue(request);
        }
        else
        {
            Toast.makeText(getActivity(),"Check Internet Connection",Toast.LENGTH_SHORT).show();
        }
    }
}
