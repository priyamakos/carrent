package makos.com.carrent.back_to_order;

/**
 * Created by PC1 on 14-12-2018.
 */

public class category_list
{
    private String car_category_id;
    private String car_category_name;

    public String getCar_category_id() {
        return car_category_id;
    }

    public void setCar_category_id(String car_category_id) {
        this.car_category_id = car_category_id;
    }

    public String getCar_category_name() {
        return car_category_name;
    }

    public void setCar_category_name(String car_category_name) {
        this.car_category_name = car_category_name;
    }
}
