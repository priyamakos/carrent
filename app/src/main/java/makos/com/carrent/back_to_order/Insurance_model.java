package makos.com.carrent.back_to_order;

/**
 * Created by pc2 on 03-12-2018.
 */

public class Insurance_model
{
    private String insurance_id;
    private String insurance_title;
    private String insurance_details;
    private String insurance_amt;
    private boolean selected = false;
    private int color;

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public String getInsurance_id() {
        return insurance_id;
    }

    public void setInsurance_id(String insurance_id) {
        this.insurance_id = insurance_id;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public String getInsurance_title() {
        return insurance_title;
    }

    public void setInsurance_title(String insurance_title) {
        this.insurance_title = insurance_title;
    }

    public String getInsurance_details() {
        return insurance_details;
    }

    public void setInsurance_details(String insurance_details) {
        this.insurance_details = insurance_details;
    }

    public String getInsurance_amt() {
        return insurance_amt;
    }

    public void setInsurance_amt(String insurance_amt) {
        this.insurance_amt = insurance_amt;
    }



}
