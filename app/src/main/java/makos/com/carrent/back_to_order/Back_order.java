package makos.com.carrent.back_to_order;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.icu.util.GregorianCalendar;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.InputType;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import makos.com.carrent.Car_list.Car_list;
import makos.com.carrent.Mysingleton.Mysingleton;
import makos.com.carrent.NetworkInfo.Connectivity;
import makos.com.carrent.R;
import makos.com.carrent.statusurl.Staticurl;

/**
 * Created by pc2 on 01-12-2018.
 */

public class Back_order extends AppCompatActivity
{
    private DatePicker datePicker;
    private Calendar calendar,calendar1,calendar2;
    private int year, month, day,time;
    private DatePickerDialog datePickerDialog,datePickerDialog1;
    TimePickerDialog mTimePicker;
    Spinner city_list,category,insurance_list;

    LinearLayout orderdate,returndatelay;
    EditText date,ordertime,returndate;
    ImageView calimg;
    int id;
    ProgressBar progressBar;
    TextView selected_company_name;

    Button submit;

    ArrayList<String> citys = new ArrayList<String>();
    ArrayList<String> categorys = new ArrayList<String>();

    ArrayList<city_list_model>city=new ArrayList<city_list_model>();
    ArrayList<category_list> categorylist = new ArrayList<category_list>();

    String company_id = "", category_id = "", user_id = "", city_name = "", comp_id = "", category_name = "";

    SharedPreferences preferences;
    SharedPreferences.Editor editor;

    @SuppressLint("NewApi")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.back_to_order);

        progressBar=findViewById(R.id.progressbar);
        orderdate = findViewById(R.id.orderdate);
        date = findViewById(R.id.date);
        calimg = findViewById(R.id.calimg);
        ordertime = findViewById(R.id.time);
        city_list=findViewById(R.id.city_list);
        category=findViewById(R.id.category_list);
        //insurance_list=findViewById(R.id.insurance_list);
        returndatelay=findViewById(R.id.returndate);
        returndate=findViewById(R.id.return_date);
        submit = findViewById(R.id.submit);

        Intent intent = getIntent();

        //city_name = intent.getStringExtra("selected_company_name");
        company_id = intent.getStringExtra("selected_car_ids");
        //selected_company_name.setText(company_name);

        preferences = getSharedPreferences("makos.com.carrent",MODE_PRIVATE);
        editor = preferences.edit();

        user_id = preferences.getString("USER_ID","0");

        calendar = Calendar.getInstance();
//Locale.forLanguageTag("@calendar=hebrew")
        // @SuppressLint({"NewApi", "LocalSuppress"})GregorianCalendar calendar = new GregorianCalendar(Locale.forLanguageTag("@calendar=hebrew@calendar=hebrew"));
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);
        date.setInputType(InputType.TYPE_NULL);
        returndate.setInputType(InputType.TYPE_NULL);
        orderdate.setFocusable(false);
        date.setFocusable(false);
        returndate.setFocusable(false);

        // user_id = intent.getStringExtra("user_id");

        calimg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                id=999;
                setDate(999);
            }
        });
        date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                id=999;
                setDate(999);
            }
        });
        orderdate.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                id=999;
                setDate(999);
            }
        });

        returndate.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                id=111;
                setDate(111);
            }
        });


        citylist();
        categories_list();
        // extralist();
        //  insurancelist();
        //   extra_list.setAdapter(new ArrayAdapter<Extras_Model>(this,android.R.layout.simple_dropdown_item_1line,extralist));

        ordertime.setInputType(InputType.TYPE_NULL);
        ordertime.setFocusable(false);
        ordertime.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Calendar mcurrentTime = Calendar.getInstance();

                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);

                mTimePicker = new TimePickerDialog(Back_order.this, new TimePickerDialog.OnTimeSetListener()
                {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute)
                    {
                        String am_pm = "hi";
                        Log.d("am_pmhi",am_pm);
                        if (selectedHour >=0 && selectedHour < 12)
                        {
                            am_pm = "AM";
                            Log.d("inam_pm",am_pm);
                        }else
                        {
                            am_pm = "PM";
                        }
                        Log.d("am_pm",am_pm);
                        ordertime.setText(Html.fromHtml(selectedHour + ":" + selectedMinute));
                    }
                }, hour, minute, true);//Yes 24 hour time
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();

            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                validation();
            }
        });

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    private void validation()
    {
        if (date.getText().toString().equals(""))
        {
            Toast.makeText(getApplicationContext(),"בחר תאריך הזמנה",Toast.LENGTH_SHORT).show();
        }
        else if (returndate.getText().toString().equals(""))
        {
            Toast.makeText(getApplicationContext(),"בחר תאריך החזרה",Toast.LENGTH_SHORT).show();
        }
        else if (ordertime.getText().toString().equals(""))
        {
            Toast.makeText(getApplicationContext(),"בחר זמן הזמנה",Toast.LENGTH_SHORT).show();
        }else
        {
            Intent intent = new Intent(Back_order.this, Car_list.class);
            intent.setFlags(intent.getFlags() | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            intent.putExtra("company_id",company_id);
            intent.putExtra("category_id",category_id);
            //intent.putExtra("category_name",category_name);
            intent.putExtra("company_name",city_name);
            intent.putExtra("comp_id",comp_id);
            intent.putExtra("category_name",category_name);
            intent.putExtra("user_id",user_id);
            intent.putExtra("order_date",date.getText().toString());
            intent.putExtra("order_time",ordertime.getText().toString());
            intent.putExtra("return_date",returndate.getText().toString());
            Log.d("args",city_name + " " + category_id);
            startActivity(intent);
        }
    }

    private void citylist()
    {
        progressBar.setVisibility(View.VISIBLE);
        if (Connectivity.isConnected(getApplicationContext()))
        {

            String cityurl=Staticurl.citylist;
            Log.e("cityurl",cityurl);

            StringRequest request = new StringRequest(Request.Method.GET, cityurl, new Response.Listener<String>() {
                @Override
                public void onResponse(String response)
                {
                    try
                    {
                        Log.e("responsecity",response);
                        JSONArray jsonArray = new JSONArray(response);

                        JSONObject jsonObject = jsonArray.getJSONObject(0);

                        if (jsonObject.getString("Status").equals("True"))
                        {
                            progressBar.setVisibility(View.INVISIBLE);
                            JSONArray jsonArray1 = jsonObject.getJSONArray("City List Data");

                            for (int i=0;i<jsonArray1.length();i++)
                            {
                                JSONObject jsonObject1 = jsonArray1.getJSONObject(i);
                                city_list_model model = new city_list_model();

                                model.setCity_id(jsonObject1.getString("city_id"));
                                model.setCity_name(jsonObject1.getString("city_name"));

                                city.add(model);
                                citys.add(Html.fromHtml(jsonObject1.getString("city_name")).toString());
                                Log.e("city_name",Html.fromHtml(jsonObject1.getString("city_name")).toString());
                            }

                            city_list.setAdapter(new ArrayAdapter<String>(Back_order.this,R.layout.spiner,citys));

                            if ("ישראל".equals("ישראל"))
                            {
                                Log.e("compareresult","true");
                            }
                            else
                            {
                                Log.e("compareresult","false");
                            }
                            city_list.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
                                {
                                    for (int i = 0;i<citys.size();i++)
                                    {
                                        String sget = Html.fromHtml(city_list.getSelectedItem().toString()).toString();
                                        String scomp = Html.fromHtml(city.get(i).getCity_name()).toString();
                                        if (sget.equals(scomp))
                                        {
                                            city_name = Html.fromHtml(city.get(i).getCity_name()).toString();
                                            comp_id = city.get(i).getCity_id();
                                            Log.e("selectedcompanyidhere", Html.fromHtml(city_list.getSelectedItem().toString()) + " = " + Html.fromHtml(city.get(i).getCity_name()).toString());
                                        }

                                        Log.e("selectedcompanyid", Html.fromHtml(city_list.getSelectedItem().toString()) + " = " + Html.fromHtml(city.get(i).getCity_name()).toString());
                                    }

                                }
                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {

                                }
                            });
                        }
                        else
                        {
                            progressBar.setVisibility(View.INVISIBLE);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressBar.setVisibility(View.INVISIBLE);
                }
            });
            Mysingleton.getInstance(getApplicationContext()).addToRequestQueue(request);
        }
        else
        {
            Toast.makeText(getApplicationContext(),"בדוק את החיבור לאינטרנט",Toast.LENGTH_SHORT).show();
        }
    }

    public void setDate(int id)
    {
        showDialog(id);
        //Toast.makeText(getApplicationContext(), "ca", Toast.LENGTH_SHORT).show();
    }

    @SuppressLint("NewApi")
    @Override
    protected Dialog onCreateDialog(int id)
    {
        // TODO Auto-generated method stub
        if (id == 999)
        {
            if (!returndate.getText().toString().equals(""))
            {
                calendar2 = Calendar.getInstance();
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                Date date2 = new Date();
                String dates = returndate.getText().toString();
                try
                {
                    date2 = sdf.parse(dates);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                Log.d("timeshere", String.valueOf(calendar.getTimeInMillis()));
                calendar2.setTimeInMillis(date2.getTime());
                datePickerDialog = new DatePickerDialog(this, myDateListener, year, month, day);
                DatePicker picker = datePickerDialog.getDatePicker();
                picker.setMaxDate(calendar2.getTimeInMillis());
                picker.setMinDate(System.currentTimeMillis());
                return datePickerDialog;

            }else
            {
                datePickerDialog = new DatePickerDialog(this, myDateListener, year, month, day);
                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
                return datePickerDialog;
            }

        }else
        {
            calendar1 = Calendar.getInstance();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date date1 = new Date();
            String dates = date.getText().toString();
            try
            {
                date1 = sdf.parse(dates);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            Log.d("timeshere", String.valueOf(calendar.getTimeInMillis()));
            calendar1.setTimeInMillis(date1.getTime());
            // arg0.setMinDate(calendar.getTimeInMillis());
            Log.d("timeshere", String.valueOf(calendar1.getTimeInMillis()));
            Log.d("currtime", String.valueOf(System.currentTimeMillis()));
            datePickerDialog1 = new DatePickerDialog(this, myDateListener, year, month, day);
            datePickerDialog1.getDatePicker().setMinDate(calendar1.getTimeInMillis());
            return datePickerDialog1;
        }

    }
    private DatePickerDialog.OnDateSetListener myDateListener = new DatePickerDialog.OnDateSetListener()
    {
        @SuppressLint("ResourceAsColor")
        @Override
        public void onDateSet(DatePicker arg0, int arg1, int arg2, int arg3)
        {
            // TODO Auto-generated method stub
            // arg1 = year
            // arg2 = month
            // arg3 = day
            arg0.setBackgroundColor(R.color.blackblue);
            arg0.setMinDate(System.currentTimeMillis());
            if (id == 999)
            {
                showDate(date,arg1, arg2+1, arg3);
            }
            else
            {
                showDate(returndate,arg1, arg2+1, arg3);
            }
        }
    };

    private void showDate(EditText txt,int year, int month, int day)
    {
        txt.setText(Html.fromHtml(new StringBuilder().append(year).append("-").append(month).append("-").append(day).toString()));
    }


    private void categories_list()
    {
        String categoryurl = Staticurl.categorylist;

        if (Connectivity.isConnected(getApplicationContext()))
        {
            progressBar.setVisibility(View.VISIBLE);
            final StringRequest request = new StringRequest(Request.Method.GET, categoryurl, new Response.Listener<String>() {
                @Override
                public void onResponse(String response)
                {
                    Log.d("categoryresponce",response);
                    try
                    {
                        JSONArray jsonArray = new JSONArray(response);

                        JSONObject jsonObject = jsonArray.getJSONObject(0);

                        if (jsonObject.getString("Status").equals("True"))
                        {
                            JSONArray jsonArray1 = jsonObject.getJSONArray("Car Category List Data");

                            for (int i =0;i<jsonArray1.length();i++)
                            {
                                JSONObject jsonObject1 = jsonArray1.getJSONObject(i);
                                category_list model = new category_list();

                                model.setCar_category_id(jsonObject1.getString("car_category_id"));
                                model.setCar_category_name(jsonObject1.getString("car_category_name"));

                                categorylist.add(model);
                                categorys.add(Html.fromHtml(jsonObject1.getString("car_category_name")).toString());
                            }

                            category.setAdapter(new ArrayAdapter<String>(Back_order.this,R.layout.spiner,categorys));

                            category.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
                                {
                                    for (int i = 0;i<categorys.size();i++)
                                    {
                                        String sget = Html.fromHtml(category.getSelectedItem().toString()).toString();
                                        String scomp = Html.fromHtml(categorylist.get(i).getCar_category_name()).toString();
                                        if (sget.equals(scomp))
                                        {
                                            category_id = Html.fromHtml(categorylist.get(i).getCar_category_id()).toString();
                                            category_name = categorylist.get(i).getCar_category_name();

                                            Log.e("selectedcategoryidhere", Html.fromHtml(category.getSelectedItem().toString()) + " = " + Html.fromHtml(categorylist.get(i).getCar_category_name()).toString());
                                        }
                                        Log.e("selectedcategoryid", Html.fromHtml(category.getSelectedItem().toString()) + " = " + Html.fromHtml(categorylist.get(i).getCar_category_name()).toString());
                                    }
                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {

                                }
                            });

                            progressBar.setVisibility(View.INVISIBLE);
                        }else
                        {
                            progressBar.setVisibility(View.INVISIBLE);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressBar.setVisibility(View.INVISIBLE);
                }
            });
            Mysingleton.getInstance(getApplicationContext()).addToRequestQueue(request);
        }else
        {
            Toast.makeText(getApplicationContext(),"בדוק את החיבור לאינטרנט",Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}