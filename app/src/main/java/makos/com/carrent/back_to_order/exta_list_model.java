package makos.com.carrent.back_to_order;

/**
 * Created by pc2 on 03-12-2018.
 */

public class exta_list_model {

    private String optional_extra_name;
    private String extra_price;
    private boolean selected = false;

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public String getOptional_extra_name() {
        return optional_extra_name;
    }

    public void setOptional_extra_name(String optional_extra_name) {
        this.optional_extra_name = optional_extra_name;
    }

    public String getExtra_price() {
        return extra_price;
    }

    public void setExtra_price(String extra_price) {
        this.extra_price = extra_price;
    }




}
