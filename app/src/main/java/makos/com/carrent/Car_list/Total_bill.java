package makos.com.carrent.Car_list;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import makos.com.carrent.Mysingleton.Mysingleton;
import makos.com.carrent.NetworkInfo.Connectivity;
import makos.com.carrent.Payment_Details.Payment_details;
import makos.com.carrent.R;
import makos.com.carrent.invoice.Extras_Model;
import makos.com.carrent.upadte_payment_method.Update_payment_method;

public class Total_bill extends AppCompatActivity
{
    TextView orderdate, ordertime, returndate, city, commision_price, insurance_name, insurancedescr, insurance_price, total_bill, adv_price;

    RecyclerView car_recycler;
    RecyclerView.Adapter car_adapter;
    RecyclerView.LayoutManager car_Layout;

    RecyclerView extra_recycler;
    RecyclerView.Adapter extra_Adapter;
    RecyclerView.LayoutManager extra_Layout;

    LinearLayout in_name, in_dscr, in_price, in_msg, extra_msg;
    Button pay;

    String userid = "";
    String company_name = "";
    String category_name = "";
    String order_date = "";
    String order_time = "";
    String return_date = "";
    String insurance_status = "0";
    String selected_cars = "";

    String insurance_title = "";
    String insurance_descr = "";

    String extra_name = "";
    String extra_price = "";

    String selected_carnames = "";
    String selected_carimgs = "";
    String selected_carcomp = "";
    String selected_carprices = "";
    String selected_extra = "";

    double car_price = 0.0;
    double insuranceprice = 0.0;
    double insurance_extra_sum = 0.0;
    double sum_bill = 0.0;
    double original_car_price_sum = 0.0;
    double comission_price = 0.0;

    String[] car_name, car_comp, car_prices, car_imgs;
    String[] extra_names, extra_prices;

    ArrayList<Car_List_Model> car_list = new ArrayList<Car_List_Model>();
    ArrayList<Extras_Model> extra_list = new ArrayList<Extras_Model>();

    SharedPreferences preferences;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_total_bill);

        orderdate = findViewById(R.id.order_date);
        returndate = findViewById(R.id.return_date);
        ordertime = findViewById(R.id.return_time);
        city = findViewById(R.id.city);
        commision_price = findViewById(R.id.comssion_price);
        insurance_name = findViewById(R.id.insurance_name);
        insurancedescr = findViewById(R.id.description);
        insurance_price = findViewById(R.id.price);
        total_bill = findViewById(R.id.tot_bill);
        adv_price = findViewById(R.id.advance_bill);

        in_name = findViewById(R.id.insurance_name_layout);
        in_dscr = findViewById(R.id.insurance_desc_layout);
        in_price = findViewById(R.id.insurance_price_layout);
        in_msg = findViewById(R.id.noinsurancelayout);
        extra_msg = findViewById(R.id.noextralaypout);

        car_recycler = findViewById(R.id.car_details);
        extra_recycler = findViewById(R.id.extra_details);

        pay = findViewById(R.id.pay);

        preferences = getSharedPreferences("makos.com.carrent",MODE_PRIVATE);
        editor = preferences.edit();

        userid = preferences.getString("USER_ID","0");

        Intent intent = getIntent();

        company_name = intent.getStringExtra("company_name");
        category_name = intent.getStringExtra("category_name");
        order_date = intent.getStringExtra("order_date");
        order_time = intent.getStringExtra("order_time");
        return_date = intent.getStringExtra("return_date");

        car_price = intent.getDoubleExtra("car_price",0.0);
        selected_cars = intent.getStringExtra("selected_car_id");

        insurance_status = intent.getStringExtra("insurance_status");
        insurance_descr = intent.getStringExtra("insurance_descr");
        insuranceprice = intent.getDoubleExtra("insurance_price",0.0);
        insurance_title = intent.getStringExtra("insurance_name");

        selected_carnames = intent.getStringExtra("selected_carnames");
        selected_carcomp = intent.getStringExtra("selected_carcomp");
        selected_carimgs = intent.getStringExtra("selected_carimgs");
        selected_carprices = intent.getStringExtra("selected_carprices");

        selected_extra = intent.getStringExtra("selected_extras");
        extra_name = intent.getStringExtra("extras_name");
        extra_price = intent.getStringExtra("extra_price");

        comission_price = intent.getDoubleExtra("paid_amt",0.0);
        sum_bill = intent.getDoubleExtra("sum_price",0.0);
        original_car_price_sum = intent.getDoubleExtra("pending_amt",0.0);
        insurance_extra_sum = intent.getDoubleExtra("insurance_extra_sum",0.0);

        Log.e("extra_name_price",extra_name + "\n" + extra_price + "\n" + selected_carnames);

        orderdate.setText(order_date);
        ordertime.setText(order_time);
        returndate.setText(return_date);
        city.setText(Html.fromHtml(company_name));

        if (insurance_title.equals(""))
        {
            in_name.setVisibility(View.GONE);
            in_dscr.setVisibility(View.GONE);
            in_price.setVisibility(View.GONE);

            in_msg.setVisibility(View.VISIBLE);
        }else
        {
            in_name.setVisibility(View.VISIBLE);
            in_dscr.setVisibility(View.VISIBLE);
            in_price.setVisibility(View.VISIBLE);

            in_msg.setVisibility(View.GONE);
            insurance_name.setText(Html.fromHtml(insurance_title));
            insurancedescr.setText(Html.fromHtml(insurance_descr));
            insurance_price.setText(String.valueOf(insuranceprice) + " ₪");
        }

        commision_price.setText("  " + String.format("%.2f",comission_price - insurance_extra_sum) + " ₪");
        total_bill.setText(String.format("%.2f",sum_bill) + " ₪");
        adv_price.setText(String.format("%.2f",comission_price) + " ₪");

        car_adapter = new New_Car_List_Adapter(Total_bill.this,car_list);
        car_Layout = new LinearLayoutManager(getApplicationContext());
        car_recycler.setAdapter(car_adapter);
        car_recycler.setLayoutManager(car_Layout);

        car_name = selected_carnames.split(",");
        car_comp = selected_carcomp.split(",");
        car_imgs = selected_carimgs.split(",");
        car_prices = selected_carprices.split(",");

        for (int i =0;i<car_name.length;i++)
        {
            Car_List_Model model = new Car_List_Model();
            //Log.e("car_name_here",car_name[i].toString());
            model.setCar_name(car_name[i].toString());
            model.setCompany_name(car_comp[i].toString());
            model.setPrice(car_prices[i].toString());
            model.setImg(car_imgs[i].toString());
            model.setCar_category(category_name);

            car_list.add(model);
        }
        car_adapter.notifyDataSetChanged();

        extra_Adapter = new Extra_list_Adapter(extra_list,Total_bill.this);
        extra_Layout = new LinearLayoutManager(getApplicationContext());
        extra_recycler.setAdapter(extra_Adapter);
        extra_recycler.setLayoutManager(extra_Layout);

        if (!extra_name.equals(""))
        {
            extra_recycler.setVisibility(View.VISIBLE);
            extra_msg.setVisibility(View.GONE);

            extra_names = extra_name.split(",");
            extra_prices = extra_price.split(",");

            for (int i = 0;i<extra_names.length;i++)
            {
                Extras_Model model = new Extras_Model();

                model.setExtra_name(extra_names[i].toString());

                model.setPrice(extra_prices[i].toString());

                extra_list.add(model);
            }
            extra_Adapter.notifyDataSetChanged();
        }else
        {
            extra_recycler.setVisibility(View.GONE);
            extra_msg.setVisibility(View.VISIBLE);
        }

        pay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                card_list_api();
                int card_count = preferences.getInt("CARD_COUNT",0);
                Log.e("card_count", String.valueOf(card_count));

               /* if (card_count == 0)
                {
                    popup();
                }else
                {*/
                    Intent intent1 = new Intent(Total_bill.this, Payment_details.class);
                    intent1.setFlags(intent1.getFlags() | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    intent1.putExtra("status","3");
                    intent1.putExtra("company_name",company_name);
                    intent1.putExtra("order_date",order_date);
                    intent1.putExtra("order_time",order_time);
                    intent1.putExtra("return_date",return_date);
                    intent1.putExtra("car_price",car_price);
                    intent1.putExtra("selected_car_id",selected_cars);
                    intent1.putExtra("insurance_status",insurance_status);
                    intent1.putExtra("insurance_price",insuranceprice);
                    intent1.putExtra("sum_price",sum_bill);
                    intent1.putExtra("paid_amt",comission_price);
                    intent1.putExtra("selected_extras",selected_extra);
                    intent1.putExtra("pending_amt",original_car_price_sum);
                    startActivity(intent1);
               // }
            }
        });

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    private void card_list_api()
    {
        if(Connectivity.isConnected(getApplicationContext())){
            String card_list_url="http://rentapp-env.av3qgyvtfy.ap-south-1.elasticbeanstalk.com/rentapp/view_card_details"+"?user_id="+userid;
            Log.e("card_list_url",card_list_url);

            StringRequest stringRequest=new StringRequest(Request.Method.GET, card_list_url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.e("response",response);

                    try {
                        JSONArray jsonArray=new JSONArray(response);

                        JSONObject jsonObject=jsonArray.getJSONObject(0);

                        //JSONArray jsonArray1 = null;
                        if(jsonObject.getString("Status").equals("True"))
                        {
                            JSONArray jsonArray1 = jsonObject.getJSONArray("Card Data");
                            Log.e("card_count_here", String.valueOf(jsonArray1.length()));
                            editor.remove("CARD_COUNT");
                            editor.apply();
                            editor.putInt("CARD_COUNT",jsonArray1.length());
                            editor.apply();
                        }else
                        {
                           /* Log.e("card_count_here", String.valueOf(jsonArray1.length()));
                            editor.remove("CARD_COUNT");
                            editor.apply();
                            editor.putInt("CARD_COUNT",0);
                            editor.apply();*/
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });
            Mysingleton.getInstance(Total_bill.this).addToRequestQueue(stringRequest);
        }else {
            Toast.makeText(getApplicationContext(),"בדוק את חיבור האינטרנט",Toast.LENGTH_SHORT).show();
        }

    }

    private void popup()
    {
        final Dialog dialog = new Dialog(Total_bill.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.logout_popup);

        TextView txt = dialog.findViewById(R.id.txt);
        Button ok = dialog.findViewById(R.id.ok);
        Button cancel = dialog.findViewById(R.id.cancel);

        txt.setText("עליך להוסיף כרטיס לפני שתעבור לתשלום");

        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                dialog.dismiss();
                Intent intent = new Intent(Total_bill.this, Update_payment_method.class);
                startActivity(intent);
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                dialog.dismiss();
            }
        });

        dialog.show();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
