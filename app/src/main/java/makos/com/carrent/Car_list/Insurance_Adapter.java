package makos.com.carrent.Car_list;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import makos.com.carrent.R;
import makos.com.carrent.back_to_order.Insurance_model;

/**
 * Created by PC1 on 17-12-2018.
 */

public class Insurance_Adapter extends RecyclerView.Adapter<Insurance_Adapter.Insurance_Viewholder>
{
    ArrayList<Insurance_model> models = new ArrayList<Insurance_model>();
    Activity activity;

    public Insurance_Adapter(ArrayList<Insurance_model> models, Activity activity)
    {
        this.models = models;
        this.activity = activity;
    }

    @NonNull
    @Override
    public Insurance_Viewholder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i)
    {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.custom_insurance_list,viewGroup,false);
        Insurance_Viewholder holder = new Insurance_Viewholder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull Insurance_Viewholder holder, int position)
    {
        TextView title = holder.title;
        TextView explaination = holder.explaination;
        TextView price = holder.price;
        RelativeLayout layout = holder.layout;

        layout.setBackgroundResource(models.get(position).getColor());
        title.setText(Html.fromHtml(models.get(position).getInsurance_title()).toString());
        explaination.setText(Html.fromHtml(models.get(position).getInsurance_details()).toString());
        price.setText(models.get(position).getInsurance_amt() + "₪");
    }

    @Override
    public int getItemCount()
    {
        return models.size();
    }

    public static class Insurance_Viewholder extends RecyclerView.ViewHolder
    {
        TextView title, explaination, price;
        RelativeLayout layout;
        public Insurance_Viewholder(@NonNull View itemView)
        {
            super(itemView);

            layout = itemView.findViewById(R.id.layout);
            title = itemView.findViewById(R.id.title);
            explaination = itemView.findViewById(R.id.explaination);
            price = itemView.findViewById(R.id.price);
        }
    }
}
