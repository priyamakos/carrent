package makos.com.carrent.Car_list;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.StringTokenizer;

import io.apptik.widget.multiselectspinner.MultiSelectSpinner;
import makos.com.carrent.Mysingleton.Mysingleton;
import makos.com.carrent.NetworkInfo.Connectivity;
import makos.com.carrent.Payment_Details.Payment_details;
import makos.com.carrent.R;
import makos.com.carrent.invoice.Extras_Model;
import makos.com.carrent.statusurl.Staticurl;
import makos.com.carrent.upadte_payment_method.Update_payment_method;

public class Insurance_Extra_list extends AppCompatActivity
{
    Spinner insurancespin;
    MultiSelectSpinner extraspin;

    ProgressBar progressBar;

    TextView msg;
    RecyclerView recyclerView;
    RecyclerView.Adapter adapter;
    RecyclerView.LayoutManager layoutManager;

    Button back,pay;
    ArrayList<String> extralists = new ArrayList<String>();
    ArrayList<Extras_Model> extralist = new ArrayList<Extras_Model>();

    ArrayList<String> extras = new ArrayList<String>();
    ArrayList<String> extras_name = new ArrayList<String>();

    String selected_extra = "";
    String userid = "";
    String company_name = "";
    String category_name = "";
    String order_date = "";
    String order_time = "";
    String return_date = "";
    String insurance_status = "0";
    double car_price = 0;
    String category_id = "";
    String selected_cars = "";
    double insuranceprice = 0;
    double extrasumprice = 0;
    double sum_bill = 0;
    double paid_amt = 0;
    double pending_amt = 0 ;
    double original_car_price_sum = 0.0;
    double comission_price = 0.0;
    double price = 0.0;
    //String pending_amt = "";

    String insurance_title = "";
    String insurance_descr = "";

    String extra_name = "";
    String extra_price = "";

    String selected_carnames = "";
    String selected_carimgs = "";
    String selected_carcomp = "";
    String selected_carprices = "";

    String bill = "";
    String paid = "";
    String pending = "";

    int billint = 0;
    int paidint = 0;
    int pendingint = 0;
    double insurance_extra_sum = 0.0;

    double amt = 0.0;
    int stat = 0;

    SharedPreferences preferences;
    SharedPreferences.Editor editor;

    StringTokenizer tokenizer;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insurance__extra_list);

        msg = findViewById(R.id.msg);

        /*insurancespin = findViewById(R.id.insurance_list);
        extraspin = findViewById(R.id.extra_list);*/

        progressBar=findViewById(R.id.progressbar);
        recyclerView = findViewById(R.id.extrarecycler);
        adapter = new Extra_list_Adapter(extralist,Insurance_Extra_list.this);
        layoutManager = new LinearLayoutManager(getApplicationContext());

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

        preferences = getSharedPreferences("makos.com.carrent",MODE_PRIVATE);
        editor = preferences.edit();

        userid = preferences.getString("USER_ID","0");

        extralist();

        back = findViewById(R.id.back);
        pay = findViewById(R.id.pay);

        Intent intent = getIntent();
        company_name = intent.getStringExtra("company_name");//city name
        category_name = intent.getStringExtra("category_name");
        order_date = intent.getStringExtra("order_date");
        order_time = intent.getStringExtra("order_time");
        return_date = intent.getStringExtra("return_date");
        car_price = intent.getDoubleExtra("car_price",0);
        selected_cars = intent.getStringExtra("selected_car_id");

        insurance_status = intent.getStringExtra("insurance_status");
        insuranceprice = intent.getDoubleExtra("insurance_price",0);
        insurance_title = intent.getStringExtra("insurance_title");
        insurance_descr = intent.getStringExtra("insurance_descr");

        selected_carnames = intent.getStringExtra("selected_carnames");
        selected_carcomp = intent.getStringExtra("selected_carcomp");
        selected_carimgs = intent.getStringExtra("selected_carimgs");
        selected_carprices = intent.getStringExtra("selected_carprices");

        Log.e("carprice", String.valueOf(car_price));

        recyclerView.addOnItemTouchListener(new Insurance_Extra_list.RecyclerTouchListener(getApplicationContext(), recyclerView,
                new Insurance_Extra_list.RecyclerTouchListener.ClickListener() {
                    @SuppressLint("ResourceAsColor")
                    @Override
                    public void onClick(View view, int position)
                    {
                        Log.e("herestart", String.valueOf(extralist.get(position).isSelected()));
                        if (extralist.get(position).isSelected() == false)
                        {
                            extralist.get(position).setSelected(true);
                            //notifyDataSetChanged();
                            view.setBackgroundResource(R.color.greenlight);
                        }else
                        {
                            extralist.get(position).setSelected(false);
                            //notifyDataSetChanged();
                            view.setBackgroundResource(R.color.transparent);
                        }

                        Log.e("hereend", String.valueOf(extralist.get(position).isSelected()));

                        adapter.notifyDataSetChanged();
                        Log.e("herestatus", String.valueOf(extralist.get(position).isSelected()) + " " + position);

                        if (extralist.get(position).isSelected())
                        {
                            extralists.add(extralist.get(position).getPrice());
                            extras.add(extralist.get(position).getExtra_id());
                            extras_name.add(extralist.get(position).getExtra_name());
                        }else
                        {
                            extralists.remove(extralist.get(position).getPrice());
                            extras.remove(extralist.get(position).getExtra_id());
                            extras_name.remove(extralist.get(position).getExtra_name());
                        }

                        Log.e("heresize", String.valueOf(extralists.size()) + " " + position);
                    }
                    @Override
                    public void onLongClick(View view, int position)
                    {

                    }
                }));

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {

                    /*Log.e("heresize", String.valueOf(extralists.size()));
                    for (int i = 0; i < extralists.size(); i++)
                    {
                        if (selected_extra.equals(""))
                        {
                            selected_extra = extras.get(i).toString();
                        }else
                        {
                            selected_extra = selected_extra + "," + extras.get(i).toString();
                        }

                        extrasumprice = extrasumprice + Double.parseDouble(extralists.get(i));
                    }
                Log.e("selectedextras",selected_extra);
                    Log.e("insurance_sum", String.valueOf(extrasumprice));*/
                    //original_car_price();

                Intent intents = new Intent(Insurance_Extra_list.this,Insurance_list.class);
                intents.putExtra("status","3");
                startActivity(intents);
            }
        });


        pay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Log.e("heresize", String.valueOf(extralists.size()));
                for (int i = 0; i < extralists.size(); i++)
                {
                    if (selected_extra.equals(""))
                    {
                        extra_name = extras_name.get(i).toString();
                        extra_price = extralists.get(i).toString();
                        selected_extra = extras.get(i).toString();
                    }else
                    {
                        selected_extra = selected_extra + "," + extras.get(i).toString();
                        extra_name = extra_name + "," + extras_name.get(i);
                        extra_price = extra_price + "," + extralists.get(i).toString();
                    }

                    extrasumprice = extrasumprice + Double.parseDouble(extralists.get(i));
                }
                Log.e("selectedextras",extra_name);
                Log.e("insurance_sum", String.valueOf(extrasumprice));
                double amt = original_car_price();

                Log.e("sum_bill_calculated", String.valueOf(amt));
                Log.e("commission_calculated", String.valueOf(comission_price));
            }
        });

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

    }

    private void extralist()
    {
     String extraurl = Staticurl.extralist;

     progressBar.setVisibility(View.VISIBLE);
     extralist.clear();
     if (Connectivity.isConnected(getApplicationContext()))
     {
         StringRequest request = new StringRequest(Request.Method.GET, extraurl, new Response.Listener<String>() {
             @Override
             public void onResponse(String response)
             {
                 try
                 {
                     JSONArray jsonArray = new JSONArray(response);

                     JSONObject jsonObject = jsonArray.getJSONObject(0);

                     if (jsonObject.getString("Status").equals("True"))
                     {
                         JSONArray jsonArray1 = jsonObject.getJSONArray("Extra List Data");

                         for (int i=0;i<jsonArray1.length();i++)
                         {
                             JSONObject jsonObject1 = jsonArray1.getJSONObject(i);
                             Extras_Model model = new Extras_Model();

                             model.setExtra_id(jsonObject1.getString("extra_id"));
                             model.setExtra_name(jsonObject1.getString("optional_extra_name"));
                             model.setPrice(jsonObject1.getString("extra_price"));

                             extralist.add(model);
                            // extralists.add(jsonObject1.getString("optional_extra_name"));
                         }
                         progressBar.setVisibility(View.INVISIBLE);

                         if (extralist.size() == 0)
                         {
                             msg.setVisibility(View.VISIBLE);
                         }else
                         {
                             msg.setVisibility(View.INVISIBLE);
                         }
                         adapter.notifyDataSetChanged();

                         //ArrayAdapter adapter = new ArrayAdapter<String>(Insurance_Extra_list.this,R.layout.spiner,extralists);
                         /*extraspin.setItems(extralists).setAllUncheckedText(extralists.get(0))
                                                       .setAllCheckedText("כולם נבחרו")
                                                       .setSelectAll(false)
                                                       .setListener(new BaseMultiSelectSpinner.MultiSpinnerListener() {
                                                           @Override
                                                           public void onItemsSelected(boolean[] selected)
                                                           {
                                                               extrasumprice = 0;
                                                                for (int i= 0;i<selected.length;i++)
                                                                {
                                                                    if (selected[i])
                                                                    {
                                                                        Log.e("selectedextras",extralists.get(i));
                                                                        if (selected_extra.equals(""))
                                                                        {
                                                                            selected_extra = String.valueOf(i+1);
                                                                        }else if (!selected_extra.contains(String.valueOf(i)))
                                                                        {
                                                                            selected_extra = selected_extra + "," + (i+1);
                                                                        }
                                                                        Log.e("selectedextras",selected_extra);
                                                                        extrasumprice = extrasumprice + Double.parseDouble(extralist.get(i).getPrice());
                                                                    }
                                                                }
                                                               Log.e("extrasum", String.valueOf(extrasumprice));
                                                           }
                                                       });*/
                     }
                     else
                     {
                         progressBar.setVisibility(View.INVISIBLE);
                         msg.setVisibility(View.VISIBLE);
                         Toast.makeText(getApplicationContext(),"אין תוספות זמינות",Toast.LENGTH_SHORT).show();
                     }
                 } catch (JSONException e) {
                     e.printStackTrace();
                 }
             }
         }, new Response.ErrorListener() {
             @Override
             public void onErrorResponse(VolleyError error) {
                 progressBar.setVisibility(View.INVISIBLE);
             }
         });
         Mysingleton.getInstance(getApplicationContext()).addToRequestQueue(request);
     }
     else
     {
         Toast.makeText(getApplicationContext(),"בדוק את חיבור האינטרנט",Toast.LENGTH_SHORT).show();
     }
 }

 private double original_car_price()
 {
     final double[] prices = {0.0};
    String original_car_price_url = Staticurl.original_car_price;

    Log.e("original_car_price_url" , original_car_price_url);

    tokenizer = new StringTokenizer(selected_cars,",");
    String[] cars = new String[selected_cars.length()];

    cars = selected_cars.split(",");

    if (Connectivity.isConnected(getApplicationContext()))
    {
        final String[] finalCars = cars;
        StringRequest request = new StringRequest(Request.Method.GET, original_car_price_url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response)
            {
                progressBar.setVisibility(View.VISIBLE);

                try
                {
                    JSONArray jsonArray = new JSONArray(response);
                    JSONObject jsonObject = jsonArray.getJSONObject(0);

                    if (jsonObject.getString("Status").equals("True"))
                    {
                        progressBar.setVisibility(View.INVISIBLE);
                        JSONArray jsonArray1 = jsonObject.getJSONArray("Car_List");

                        Log.e("selected_cars_here", selected_cars);
                        for (int j = 0; j < finalCars.length ; j++)
                        {
                            for (int i=0;i<jsonArray1.length();i++)
                            {
                                JSONObject jsonObject1 = jsonArray1.getJSONObject(i);
                                Log.e("original_car_token", finalCars[j]);
                                if (finalCars[j].equals(jsonObject1.getString("car_id")))
                                {
                                    original_car_price_sum = original_car_price_sum + Double.parseDouble(jsonObject1.getString("price"));
                                    Log.e("original_car_price", String.valueOf(Double.parseDouble(jsonObject1.getString("price"))));
                                }
                            }
                            Log.e("original_car_price_sum", String.valueOf(original_car_price_sum));
                            sum_bill = sum_bill + car_price + insuranceprice + extrasumprice;
                            Log.e("sum_bill", String.valueOf(sum_bill));

                            insurance_extra_sum = insuranceprice + extrasumprice;
                            comission_price = sum_bill - original_car_price_sum;
                            make_payment(0.0,original_car_price_sum);
                            //Log.e("amount", String.valueOf(amt));
                        }
                    }
                    else
                    {
                        progressBar.setVisibility(View.INVISIBLE);
                        Toast.makeText(Insurance_Extra_list.this,jsonObject.getString("Message"),Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        });
        Mysingleton.getInstance(getApplicationContext()).addToRequestQueue(request);
    }
    else
    {
        progressBar.setVisibility(View.INVISIBLE);
        Toast.makeText(getApplicationContext(),"בדוק את חיבור האינטרנט",Toast.LENGTH_SHORT).show();
    }
     Log.e("amount", String.valueOf(amt));
    return amt;
 }

    private void make_payment(double stat, double com_price)
    {
        //?user_id=1&city=solapur&order_date=2018-10-11&order_time=10:30&return_date=2018-10-11&selected_car_ids=1,2,3,4&authority=1&extra_list_ids=1,2,3,4&insurance_status=0&bill_amt=1000&paid_amt=200&pending_amt=800&bill_status=0

        /*sum_bill = sum_bill + car_price + insuranceprice + extrasumprice;
        Log.e("sum_bill", String.valueOf(sum_bill));
        comission_price = sum_bill - com_price;
        Log.e("comission_price", String.valueOf(com_price));

        if (selected_extra.equals(""))
        {
            selected_extra = "0";
        }
        if (insurance_status.equals("0"))
        {
            insurance_status = "0";
        }*/

        card_list_api();
        int card_count = preferences.getInt("CARD_COUNT",0);
        Log.e("card_count", String.valueOf(card_count));

       /* if (card_count == 0)
        {
            popup();
        }else
        {*/

            Intent intent1 = new Intent(Insurance_Extra_list.this,Total_bill.class);
            intent1.setFlags(intent1.getFlags() | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            intent1.putExtra("status","3");
            intent1.putExtra("company_name",company_name);
            intent1.putExtra("category_name",category_name);
            intent1.putExtra("order_date",order_date);
            intent1.putExtra("order_time",order_time);
            intent1.putExtra("return_date",return_date);
            intent1.putExtra("car_price",car_price);
            intent1.putExtra("selected_car_id",selected_cars);

            intent1.putExtra("insurance_status",insurance_status);
            intent1.putExtra("insurance_price",insuranceprice);
            intent1.putExtra("insurance_name",insurance_title);
            intent1.putExtra("insurance_descr",insurance_descr);

            intent1.putExtra("selected_carnames",selected_carnames);
            intent1.putExtra("selected_carcomp",selected_carcomp);
            intent1.putExtra("selected_carimgs",selected_carimgs);
            intent1.putExtra("selected_carprices",selected_carprices);

            intent1.putExtra("selected_extras",selected_extra);
            intent1.putExtra("extras_name",extra_name);
            intent1.putExtra("extra_price",extra_price);

            intent1.putExtra("paid_amt",comission_price);
            intent1.putExtra("sum_price",sum_bill);
            intent1.putExtra("pending_amt",original_car_price_sum);
            intent1.putExtra("insurance_extra_sum",insurance_extra_sum);
           // intent1.setFlags(intent1.getFlags() | Intent.FLAG_ACTIVITY_NO_HISTORY);// | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP
            startActivity(intent1);
     //   }

        /*String orderurl = Staticurl.make_an_order
                          + "?user_id=" + userid
                          + "&city=" + company_name//city name
                          + "&order_date=" + order_date
                          + "&order_time=" + order_time
                          + "&return_date=" + return_date
                          + "&selected_car_ids=" + selected_cars
                          + "&authority=0"
                          + "&extra_list_ids=" + selected_extra
                          + "&insurance_status=" + insurance_status
                          + "&bill_amt=" + sum_bill
                          + "&paid_amt=" + comission_price
                          + "&pending_amt=" + original_car_price_sum
                          + "&bill_status=" + "0";

        Log.e("orderurl",orderurl);

        progressBar.setVisibility(View.VISIBLE);
        if (Connectivity.isConnected(getApplicationContext()))
        {
            StringRequest request = new StringRequest(Request.Method.GET, orderurl, new Response.Listener<String>() {
                @Override
                public void onResponse(String response)
                {
                    Log.e("orderresponce",response);
                    try
                    {
                        JSONArray jsonArray = new JSONArray(response);
                        JSONObject jsonObject = jsonArray.getJSONObject(0);

                        if (jsonObject.getString("Status").equals("True"))
                        {
                            editor.remove("ORDER_ID");
                            editor.apply();
                            editor.putString("ORDER_ID",jsonObject.getString("Order Id"));
                            editor.commit();
                            editor.apply();

                            Toast.makeText(getApplicationContext(),"ההזמנה נוספה בהצלחה",Toast.LENGTH_SHORT).show();
                            progressBar.setVisibility(View.INVISIBLE);*/



                            /*Intent intent1 = new Intent(Insurance_Extra_list.this,Previous_orders.class);
                            intent1.putExtra("stat",1);
                            intent1.putExtra("user_id",userid);
                            //intent1.putExtra("order_id",order_id);
                            intent1.putExtra("status","3");
                            intent1.putExtra("bill_amt",sum_bill);
                            intent1.putExtra("pending_bill",pending_amt);
                            intent1.putExtra("paid_bill",paid_amt);
                            startActivity(intent1);*/
                        /*}else
                        {
                            progressBar.setVisibility(View.INVISIBLE);
                            Toast.makeText(getApplicationContext(),"הוספת ההזמנות נכשלה",Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressBar.setVisibility(View.INVISIBLE);
                }
            });
            Mysingleton.getInstance(getApplicationContext()).addToRequestQueue(request);
        }
        Log.e("sum_bill", String.valueOf(comission_price));
        return comission_price;*/
    }

    private void card_list_api()
    {
        if(Connectivity.isConnected(getApplicationContext())){
            String card_list_url="http://rentapp-env.av3qgyvtfy.ap-south-1.elasticbeanstalk.com/rentapp/view_card_details"+"?user_id="+userid;
            Log.e("card_list_url",card_list_url);

            StringRequest stringRequest=new StringRequest(Request.Method.GET, card_list_url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.e("response",response);

                    try {
                        JSONArray jsonArray=new JSONArray(response);

                        JSONObject jsonObject=jsonArray.getJSONObject(0);

                        JSONArray jsonArray1 = jsonObject.getJSONArray("Card Data");
                        if(jsonObject.getString("Status").equals("True"))
                        { Log.e("card_count_here", String.valueOf(jsonArray1.length()));
                            editor.remove("CARD_COUNT");
                            editor.apply();
                            editor.putInt("CARD_COUNT",jsonArray1.length());
                            editor.apply();
                        }else
                        {
                            Log.e("card_count_here", String.valueOf(jsonArray1.length()));
                            editor.remove("CARD_COUNT");
                            editor.apply();
                            editor.putInt("CARD_COUNT",0);
                            editor.apply();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });
            Mysingleton.getInstance(Insurance_Extra_list.this).addToRequestQueue(stringRequest);
        }else {
            Toast.makeText(getApplicationContext(),"בדוק את חיבור האינטרנט",Toast.LENGTH_SHORT).show();
        }

    }

    private void popup()
    {
        final Dialog dialog = new Dialog(Insurance_Extra_list.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.logout_popup);

        TextView txt = dialog.findViewById(R.id.txt);
        Button ok = dialog.findViewById(R.id.ok);
        Button cancel = dialog.findViewById(R.id.cancel);

        txt.setText("עליך להוסיף כרטיס לפני שתעבור לתשלום");

        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                dialog.dismiss();
                Intent intent = new Intent(Insurance_Extra_list.this, Update_payment_method.class);
                startActivity(intent);
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private Insurance_Extra_list.RecyclerTouchListener.ClickListener clickListener;

        public RecyclerTouchListener(Context applicationContext, final RecyclerView recyclerView, final Insurance_Extra_list.RecyclerTouchListener.ClickListener clickListener)
        {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(applicationContext, new GestureDetector.SimpleOnGestureListener()
            {
                @Override
                public boolean onSingleTapUp(MotionEvent e)
                {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e)
                {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null)
                    {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }
        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e)
        {
            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e))
            {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }
        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e)
        {
        }
        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept)
        {
        }
        public interface ClickListener
        {
            void onClick(View view, int position);
            void onLongClick(View view, int position);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
