package makos.com.carrent.Car_list;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import makos.com.carrent.MainActivity;
import makos.com.carrent.Mysingleton.Mysingleton;
import makos.com.carrent.NetworkInfo.Connectivity;
import makos.com.carrent.Payment_Details.Payment_details;
import makos.com.carrent.R;
import makos.com.carrent.invoice.Detailed_Activity;
import makos.com.carrent.invoice.Detailed_Car_Adapter;
import makos.com.carrent.invoice.Detailed_Extras_Adapter;
import makos.com.carrent.invoice.Extras_Model;
import makos.com.carrent.invoice.Invice_Model;
import makos.com.carrent.statusurl.Staticurl;

public class View_order extends AppCompatActivity
{
    TextView invoice_no,return_time,insurance,order_no,bill_amt,order_date,return_date,order_time,insurance_title,insurance_price;
    Button return_order, payment, cancel_order;

    RecyclerView carrecycler;
    RecyclerView extrarecycler;
    RecyclerView.Adapter caradapter;
    RecyclerView.Adapter extraadapter;
    RecyclerView.LayoutManager carlayout;
    RecyclerView.LayoutManager extralayout;
    ProgressBar progressBar;

    ArrayList<Car_List_Model> carlist = new ArrayList<Car_List_Model>();
    ArrayList<Extras_Model> extralist = new ArrayList<Extras_Model>();

    String userid = "";
    double bill = 0;
    double pending = 0;
    double paid = 0;
    String orderid = "";
    String returntime = "";

    String pay_stat = "0";
    String return_stat = "0";
    String user_id = "";

    int stat = 0;

    SharedPreferences preferences;
    SharedPreferences.Editor editor;

    Calendar calendar;
    Date date;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_order);

        progressBar=findViewById(R.id.progressbar);
        invoice_no = findViewById(R.id.invoice_no);
        //return_time = findViewById(R.id.return_time);
        insurance = findViewById(R.id.insurance);
        order_no = findViewById(R.id.orderno);
        bill_amt = findViewById(R.id.billamt);
        order_date = findViewById(R.id.order_date);
        return_date = findViewById(R.id.return_date);
        order_time = findViewById(R.id.order_time);
        insurance_title = findViewById(R.id.insurance_title);
        insurance_price = findViewById(R.id.insurance_price);

        return_order = findViewById(R.id.return_order);
       // payment = findViewById(R.id.payment);
      //  cancel_order = findViewById(R.id.cancel_order);

        preferences = getSharedPreferences("makos.com.carrent",MODE_PRIVATE);
        editor = preferences.edit();

        user_id = preferences.getString("USER_ID","0");

        Intent intent = getIntent();

        stat = intent.getIntExtra("stat",0);

        if (stat == 3)
        {
            orderid = intent.getStringExtra("order_id");
            pending = intent.getDoubleExtra("pending_amt",0.0);
            paid = intent.getDoubleExtra("paid_amt",0.0);
            bill = intent.getDoubleExtra("bill_amt",0.0);
        }
        else if (stat == 2)
        {
            orderid = intent.getStringExtra("order_id");
            pending = intent.getDoubleExtra("pending_amt",0.0);
            paid = intent.getDoubleExtra("paid_amt",0.0);
            pay_stat = intent.getStringExtra("pay_stat");
        }

        return_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                calendar = Calendar.getInstance();
                returntime = calendar.get(Calendar.HOUR_OF_DAY) + ":" + calendar.get(Calendar.MINUTE);

                if (pay_stat.equals("0"))
                {
                    popup("עליך לשלם את הסכום שנותר לפני החזרת ההזמנה","0");
                }else
                {
                    if (return_stat.equals("0"))
                    {
                        popup("האם אתה בטוח שברצונך להחזיר הזמנה זו?","1");
                    }else
                    {
                        popup("כבר החזרת את ההזמנה","1");//you have already returned the order
                    }

                }
            }
        });

       /* payment.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
               // make_payment();
                Intent intent1 = new Intent(View_order.this, Payment_details.class);
                intent1.putExtra("status","2");
                intent1.putExtra("bill_amt",bill);
                intent1.putExtra("paid_amt",paid);
                intent1.putExtra("pending_amt",pending);
                intent1.putExtra("orderid",orderid);
                startActivity(intent1);

            }
        });*/

        /*cancel_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {

            }
        });*/

        carrecycler = findViewById(R.id.carrecycler);
        caradapter = new Detailed_Car_Adapter(View_order.this,carlist);
        carlayout = new LinearLayoutManager(getApplicationContext());
        carrecycler.setLayoutManager(carlayout);
        carrecycler.setAdapter(caradapter);

        extrarecycler = findViewById(R.id.extrarecycler);
        extraadapter = new Detailed_Extras_Adapter(View_order.this,extralist);
        extralayout = new LinearLayoutManager(getApplicationContext());
        extrarecycler.setLayoutManager(extralayout);
        extrarecycler.setAdapter(extraadapter);

        invoice_list();

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    private void popup(String msg , final String stat)
    {
        final Dialog dialog = new Dialog(View_order.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.logout_popup);

        TextView text = dialog.findViewById(R.id.txt);

        text.setText(msg);

        Button ok = dialog.findViewById(R.id.ok);
        Button cancel = dialog.findViewById(R.id.cancel);

        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (stat.equals("0"))
                {
                   // make_payment();
                    dialog.dismiss();
                    Intent intent1 = new Intent(View_order.this, Payment_details.class);
                    intent1.putExtra("status","2");
                    intent1.putExtra("bill_amt",bill);
                    intent1.putExtra("paid_amt",paid);
                    intent1.putExtra("pending_amt",pending);
                    intent1.putExtra("orderid",orderid);
                    startActivity(intent1);
                }else if (stat.equals("1"))
                {
                    if (return_stat.equals("0"))
                    {
                        return_order();
                    }
                    dialog.dismiss();
                }

            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void invoice_list()
    {
        String invoiceurl = Staticurl.orderwise_invoice + "?order_id=" + orderid;

        Log.e("Invoiceurl",invoiceurl);
        progressBar.setVisibility(View.VISIBLE);
        if (Connectivity.isConnected(getApplicationContext()))
        {
            StringRequest request = new StringRequest(Request.Method.GET, invoiceurl, new Response.Listener<String>() {
                @Override
                public void onResponse(String response)
                {
                    Log.d("invoiceresponcce",response);
                    try
                    {
                        JSONArray jsonArray = new JSONArray(response);

                        JSONObject jsonObject = jsonArray.getJSONObject(0);
                        if (jsonObject.getString("Status").equals("True"))
                        {
                            JSONArray jsonArray1 = jsonObject.getJSONArray("Data");
                            for (int i = 0; i<jsonArray1.length();i++)
                            {
                                JSONObject jsonObject1 = jsonArray1.getJSONObject(i);
                                Invice_Model model = new Invice_Model();

                                Log.d("Datalist",jsonObject1.getString("Invoice No"));
                                invoice_no.setText(Html.fromHtml(jsonObject1.getString("Invoice No")).toString());
                                //return_time.setText(jsonObject1.getString("Return Time"));
                                insurance.setText(Html.fromHtml(jsonObject1.getString("Insurance")).toString());
                                order_no.setText(Html.fromHtml(jsonObject1.getString("Order No")).toString());
                                bill_amt.setText(String.format("%.2f",Double.parseDouble(Html.fromHtml(jsonObject1.getString("Bill Amount")).toString())) + "₪");
                                order_date.setText(Html.fromHtml(jsonObject1.getString("Order Date")).toString());
                                return_date.setText(Html.fromHtml(jsonObject1.getString("Return Date")).toString());
                                order_time.setText(Html.fromHtml(jsonObject1.getString("Order Time")).toString());
                                if (!jsonObject1.getString("Insurance").equals("No"))
                                {
                                    insurance_title.setText(Html.fromHtml(jsonObject1.getString("Insurance Title")).toString());
                                    insurance_price.setText(String.format("%.2f",Double.parseDouble(jsonObject1.getString("Insurance Amount"))));
                                }

                                JSONArray jsonArray2 = jsonObject1.getJSONArray("Car List");
                                model.setCar(String.valueOf(jsonArray2));

                                for (int j = 0; j< jsonArray2.length();j++)
                                {
                                    JSONObject jsonObject2 = jsonArray2.getJSONObject(j);

                                    Log.e("carlist",jsonObject2.getString("car_name"));
                                    Car_List_Model invoice = new Car_List_Model();
                                    invoice.setCar_name(jsonObject2.getString("car_name"));
                                    invoice.setCompany_name(jsonObject2.getString("car_company"));
                                    invoice.setCar_category(jsonObject2.getString("car_category"));
                                    invoice.setPrice(jsonObject2.getString("list_price"));
                                    invoice.setImg(jsonObject2.getString("image_path"));

                                    carlist.add(invoice);
                                }
                                caradapter.notifyDataSetChanged();

                                JSONArray jsonArray3 = jsonObject1.getJSONArray("Extra List");
                                model.setExtra(String.valueOf(jsonArray3));
                                Log.e("carshow", String.valueOf(jsonArray2));

                                for (int k = 0; k < jsonArray3.length(); k++)
                                {
                                    JSONObject jsonObject2 = jsonArray3.getJSONObject(k);
                                    Extras_Model extra = new Extras_Model();

                                    Log.d("extralist",jsonObject2.getString("extra_name"));

                                    extra.setExtra_name(jsonObject2.getString("extra_name"));
                                    extra.setPrice(jsonObject2.getString("price"));

                                    extralist.add(extra);
                                }
                                progressBar.setVisibility(View.INVISIBLE);
                                extraadapter.notifyDataSetChanged();
                                //inviceModel.add(model);
                            }
                            // Log.e("inviocelistcount", String.valueOf(inviceModel.size()));
                        }
                        else
                        {
                            progressBar.setVisibility(View.INVISIBLE);
                            Toast.makeText(getApplicationContext(),"ללא חשבונית",Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressBar.setVisibility(View.INVISIBLE);
                }
            })
            {
                /**
                 * Returns which encoding should be used when converting POST or PUT parameters returned by
                 * {@link #getParams()} into a raw POST or PUT body.
                 * <p>
                 * <p>This controls both encodings:
                 * <ol>
                 * <li>The string encoding used when converting parameter names and values into bytes prior
                 * to URL encoding them.</li>
                 * <li>The string encoding used when converting the URL encoded parameters into a raw
                 * byte array.</li>
                 * </ol>
                 */
                @Override
                protected String getParamsEncoding()
                {
                    return super.getParamsEncoding();
                }
            };
            Mysingleton.getInstance(getApplicationContext()).addToRequestQueue(request);
        }
        else
        {
            Toast.makeText(getApplicationContext(),"בדוק את חיבור האינטרנט",Toast.LENGTH_SHORT).show();
        }
    }

    private void return_order()
    {
        //?order_id=1&return_time=10:30&paid_amt=1000&pending_amt=0&bill_status=1
        String return_order_url = Staticurl.return_order
                                  + "?cid=" + user_id
                                  + "&return_time=" + returntime
                                  + "&order_id=" + orderid
                                  + "&paid_amt=" + paid
                                  + "&pending_amt=" + pending
                                  + "&bill_status=" + "1";

        Log.e("return_order_url",return_order_url);

        progressBar.setVisibility(View.VISIBLE);
        if (Connectivity.isConnected(getApplicationContext()))
        {
            StringRequest request = new StringRequest(Request.Method.GET, return_order_url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response)
                {
                    Log.e("return_order_responce",response);

                    try
                    {
                        JSONArray jsonArray = new JSONArray(response);
                        JSONObject jsonObject = jsonArray.getJSONObject(0);

                        if (jsonObject.getString("Status").equals("True"))
                        {
                            progressBar.setVisibility(View.INVISIBLE);
                            return_stat = "1";
                            Toast.makeText(getApplicationContext(),"ההזמנה הוחזרה בהצלחה",Toast.LENGTH_SHORT).show();
                        }else
                        {
                            progressBar.setVisibility(View.INVISIBLE);
                            Toast.makeText(getApplicationContext(),"החזרת ההזמנה נכשלה",Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressBar.setVisibility(View.INVISIBLE);
                }
            });
            Mysingleton.getInstance(getApplicationContext()).addToRequestQueue(request);
        }else
        {
            Toast.makeText(getApplicationContext(),"בדוק את חיבור האינטרנט",Toast.LENGTH_SHORT).show();
        }
    }

    private void make_payment()
    {
        //?order_id=1&bill_amt=1000&paid_amt=1000&pending_amt=0&bill_status=1
        paid = pending;
        pending = 0.0;
        String makepaymenturl = Staticurl.make_payment
                + "?order_id=" + orderid
                + "&bill_amt=" + bill
                + "&paid_amt=" + paid
                + "&pending_amt=" + pending
                + "&bill_status=" + "1";

        Log.e("makepaymenturl",makepaymenturl);

        progressBar.setVisibility(View.VISIBLE);
        if (Connectivity.isConnected(getApplicationContext()))
        {
            final StringRequest request = new StringRequest(Request.Method.GET, makepaymenturl, new Response.Listener<String>() {
                @Override
                public void onResponse(String response)
                {
                    Log.e("makepaymentrespnce",response);
                    try
                    {
                        JSONArray jsonArray = new JSONArray(response);
                        JSONObject jsonObject = jsonArray.getJSONObject(0);

                        if (jsonObject.getString("Status").equals("True"))
                        {
                            Toast.makeText(getApplicationContext(),"ההזמנה נוספה בהצלחה",Toast.LENGTH_SHORT).show();
                            pay_stat = "1";
                            progressBar.setVisibility(View.INVISIBLE);
                        }
                        else
                        {
                            progressBar.setVisibility(View.INVISIBLE);
                            Toast.makeText(getApplicationContext(),"הוספת ההזמנות נכשלה",Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });
            Mysingleton.getInstance(getApplicationContext()).addToRequestQueue(request);
        }
        else
        {
            Toast.makeText(getApplicationContext(),"בדוק את חיבור האינטרנט",Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
