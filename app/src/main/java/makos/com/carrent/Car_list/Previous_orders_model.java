package makos.com.carrent.Car_list;

/**
 * Created by PC1 on 15-12-2018.
 */

public class Previous_orders_model
{
    private String order_no;
    private String invoice_no;
    private String bill_amount;
    private String pending_amount;
    private String paymnet_status;

    public String getPending_amount() {
        return pending_amount;
    }

    public void setPending_amount(String pending_amount) {
        this.pending_amount = pending_amount;
    }

    public String getOrder_no() {
        return order_no;
    }

    public void setOrder_no(String order_no) {
        this.order_no = order_no;
    }

    public String getInvoice_no() {
        return invoice_no;
    }

    public void setInvoice_no(String invoice_no) {
        this.invoice_no = invoice_no;
    }

    public String getBill_amount() {
        return bill_amount;
    }

    public void setBill_amount(String bill_amount) {
        this.bill_amount = bill_amount;
    }

    public String getPaymnet_status() {
        return paymnet_status;
    }

    public void setPaymnet_status(String paymnet_status) {
        this.paymnet_status = paymnet_status;
    }
}
