package makos.com.carrent.Car_list;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import makos.com.carrent.R;

/**
 * Created by PC1 on 15-01-2019.
 */

public class New_Car_List_Adapter extends RecyclerView.Adapter<New_Car_List_Adapter.Car_List_View>
{
    Activity activity;
    ArrayList<Car_List_Model> model = new ArrayList<Car_List_Model>();
    String selected_car = "";
    boolean status = false,stat = false;
    String car = "";
    int pos=0;

    ArrayList<String> selected_cars = new ArrayList<String>();

    public New_Car_List_Adapter()
    {
    }

    public New_Car_List_Adapter(Activity activity, ArrayList<Car_List_Model> model)
    {
        this.activity = activity;
        this.model = model;
    }

    @NonNull
    @Override
    public Car_List_View onCreateViewHolder(@NonNull ViewGroup viewGroup, int i)
    {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.car_details,viewGroup,false);//.inflate(R.layout.custom_car_list,viewGroup,null);
        Car_List_View carListView = new Car_List_View(view);
        return carListView;
    }

    @Override
    public void onBindViewHolder(@NonNull Car_List_View holder, int position)
    {
        final RelativeLayout layout = holder.layout;
        TextView carid = holder.carid;
        TextView carname = holder.carname;
        TextView carcategory = holder.carcategory;
        TextView carprice = holder.carprice;
        ImageView img = holder.carimg;
        final FrameLayout flayout = holder.flayout;

        // carid.setText(model.get(position).getCar_id());
        carname.setText(Html.fromHtml(model.get(position).getCar_name()).toString());
        carcategory.setText(Html.fromHtml(model.get(position).getCar_category()));
        carprice.setText(String.format("%.2f",Double.parseDouble(model.get(position).getPrice())) + "₪");
        //Picasso.with(activity).load(model.get(position).getImg()).error(R.drawable.carlogo).placeholder(R.drawable.carlogo).into(img);
        Picasso.with(activity).load(model.get(position).getImg()).placeholder(R.drawable.lesss_for_u).error(R.drawable.lesss_for_u).into(img);
    }

    @Override
    public int getItemCount()
    {
        return model.size();
    }

    public static class Car_List_View extends RecyclerView.ViewHolder
    {
        RelativeLayout layout;
        TextView carid, carname, carcategory, carprice;
        ImageView carimg;
        FrameLayout flayout;
        public Car_List_View(@NonNull View itemView)
        {
            super(itemView);
            layout = itemView.findViewById(R.id.layout);
            //carid = itemView.findViewById(R.id.carid);
            carname = itemView.findViewById(R.id.carname);
            carcategory = itemView.findViewById(R.id.category);
            carprice = itemView.findViewById(R.id.price);
            carimg=itemView.findViewById(R.id.img);
            flayout = itemView.findViewById(R.id.flContent);
        }
    }
}
