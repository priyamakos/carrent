package makos.com.carrent.Car_list;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import makos.com.carrent.MainActivity;
import makos.com.carrent.Mysingleton.Mysingleton;
import makos.com.carrent.NetworkInfo.Connectivity;
import makos.com.carrent.R;
import makos.com.carrent.invoice.Detailed_Activity;
import makos.com.carrent.lobby.Lobby;
import makos.com.carrent.statusurl.Staticurl;

public class Previous_orders extends AppCompatActivity
{
    FrameLayout flayout;
    TextView status, msg;
    RecyclerView recyclerView;
    RecyclerView.Adapter adapter;
    RecyclerView.LayoutManager layoutManager;
    ProgressBar progressBar;

    ArrayList<Previous_orders_model> model = new ArrayList<Previous_orders_model>();

    String userid = "";
    double bill = 0.0;
    double pending = 0;
    double paid = 0.0;
    double paid_amt = 0.0;
    String orderid = "";
    String s_status = "", stat_back = "";

    SharedPreferences preferences;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_previous_orders);

        progressBar=findViewById(R.id.progressbar);
        flayout = findViewById(R.id.flContent);

        msg = findViewById(R.id.txt);

        final Intent intent = getIntent();
        s_status = intent.getStringExtra("status");
        stat_back = intent.getStringExtra("stat_back");

        preferences = getSharedPreferences("makos.com.carrent",MODE_PRIVATE);
        editor = preferences.edit();

        userid = preferences.getString("USER_ID","0");

        recyclerView = findViewById(R.id.recycler);

        adapter = new Previous_order_Adpater(Previous_orders.this,model);
        layoutManager = new LinearLayoutManager(getApplicationContext());

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

        previous_orders();

        recyclerView.addOnItemTouchListener(new Previous_orders.RecyclerTouchListener(getApplicationContext(), recyclerView,
                new Previous_orders.RecyclerTouchListener.ClickListener() {
                    @SuppressLint("ResourceAsColor")
                    @Override
                    public void onClick(View view, final int position)
                    {
                        status = view.findViewById(R.id.payment_status);

                        // status.setFocusable(false);
                        status.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v)
                            {
                                orderid = model.get(position).getOrder_no();

                                if (s_status.equals("2") || s_status.equals("3"))
                                {
                                    status.setBackgroundResource(R.color.yellow);
                                    bill = Double.parseDouble(model.get(position).getBill_amount());

                                    if (model.get(position).getPaymnet_status().equals("Pending"))
                                    {
                                        pending = Double.parseDouble(model.get(position).getPending_amount());
                                        paid = 0;
                                    }

                                    Intent intent1 = new Intent(Previous_orders.this,View_order.class);
                                    intent1.putExtra("stat",3);
                                    intent1.putExtra("paid_amt",paid_amt);
                                    intent1.putExtra("bill_amt",bill);
                                    intent1.putExtra("pending_amt",pending);
                                    intent1.putExtra("order_id",orderid);
                                    startActivity(intent1);
                                    //make_payment();
                                }else
                                {
                                    status.setBackgroundResource(R.color.lightgreen);
                                    //status.setText("Invoice");
                                    Intent intent1 = new Intent(Previous_orders.this, Detailed_Activity.class);
                                    intent1.putExtra("status","0");
                                    intent1.putExtra("order_id",orderid);
                                    intent1.putExtra("user_id",userid);
                                    startActivity(intent1);
                                }
                            }
                        });
                    }
                    @Override
                    public void onLongClick(View view, int position)
                    {

                    }
                }));

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        if (s_status.equals("1"))
        {
            getSupportActionBar().setTitle("סדר תיקייה");
        }else if (s_status.equals("2") || s_status.equals("3"))
        {
            getSupportActionBar().setTitle("מצב הזמנה");
        }
    }

    @Override
    protected void onResume()
    {
        adapter.notifyDataSetChanged();
        super.onResume();
    }

    @Override
    protected void onStart()
    {
        adapter.notifyDataSetChanged();
        super.onStart();
    }

    private void previous_orders()
    {
        String previousurl = Staticurl.previous_orders + "?user_id=" + userid;

        Log.d("previousurl",previousurl);

        progressBar.setVisibility(View.VISIBLE);

        model.clear();
        if (Connectivity.isConnected(getApplicationContext()))
        {
            StringRequest request = new StringRequest(Request.Method.GET, previousurl, new Response.Listener<String>() {
                @Override
                public void onResponse(String response)
                {
                    try
                    {
                        JSONArray jsonArray = new JSONArray(response);
                        JSONObject jsonObject = jsonArray.getJSONObject(0);

                        if (jsonObject.getString("Status").equals("True"))
                        {
                            JSONArray jsonArray1 = jsonObject.getJSONArray("Data");

                            for (int i=0;i<jsonArray1.length();i++)
                            {
                                JSONObject jsonObject1 = jsonArray1.getJSONObject(i);
                                Previous_orders_model models = new Previous_orders_model();

                                if (s_status.equals("1") && jsonObject1.getString("paymnet_status").equals("paid"))
                                {
                                    models.setOrder_no(jsonObject1.getString("order_no"));
                                    models.setInvoice_no(jsonObject1.getString("invoice_no"));
                                    models.setBill_amount(jsonObject1.getString("bill_amount"));
                                    models.setPaymnet_status(jsonObject1.getString("paymnet_status"));
                                    paid_amt = Double.parseDouble(jsonObject1.getString("bill_amount")) - Double.parseDouble(jsonObject1.getString("pending_amount"));

                                    model.add(models);
                                }else if ((s_status.equals("2") && jsonObject1.getString("paymnet_status").equals("Pending")) || (s_status.equals("3") && jsonObject1.getString("paymnet_status").equals("Pending")))
                                {
                                    models.setOrder_no(jsonObject1.getString("order_no"));
                                    models.setInvoice_no(jsonObject1.getString("invoice_no"));
                                    models.setBill_amount(jsonObject1.getString("bill_amount"));
                                    models.setPaymnet_status(jsonObject1.getString("paymnet_status"));
                                    models.setPending_amount(jsonObject1.getString("pending_amount"));
                                    paid_amt = Double.parseDouble(jsonObject1.getString("bill_amount")) - Double.parseDouble(jsonObject1.getString("pending_amount"));

                                    model.add(models);
                                }
                            }
                            progressBar.setVisibility(View.INVISIBLE);

                            if (model.size() == 0)
                            {
                                msg.setVisibility(View.VISIBLE);
                            }
                            else
                            {
                                msg.setVisibility(View.INVISIBLE);
                            }
                            adapter.notifyDataSetChanged();
                        }
                        else
                        {
                            progressBar.setVisibility(View.INVISIBLE);
                            Toast.makeText(getApplicationContext(),"אין הזמנות קודמות",Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressBar.setVisibility(View.INVISIBLE);
                }
            });
            Mysingleton.getInstance(getApplicationContext()).addToRequestQueue(request);
        }
        else
        {
            Toast.makeText(getApplicationContext(),"בדוק את החיבור לאינטרנט",Toast.LENGTH_SHORT).show();
        }
    }

    private void make_payment()
    {
        //?order_id=1&bill_amt=1000&paid_amt=1000&pending_amt=0&bill_status=1
        String makepaymenturl = Staticurl.make_payment
                + "?order_id=" + orderid
                + "&bill_amt=" + bill
                + "&paid_amt=" + paid
                + "&pending_amt=" + pending
                + "&bill_status=" + "1";

        Log.e("makepaymenturl",makepaymenturl);

        progressBar.setVisibility(View.VISIBLE);
        if (Connectivity.isConnected(getApplicationContext()))
        {
            final StringRequest request = new StringRequest(Request.Method.GET, makepaymenturl, new Response.Listener<String>() {
                @Override
                public void onResponse(String response)
                {
                    Log.e("makepaymentrespnce",response);
                    try
                    {
                        JSONArray jsonArray = new JSONArray(response);
                        JSONObject jsonObject = jsonArray.getJSONObject(0);

                        if (jsonObject.getString("Status").equals("True"))
                        {
                            Toast.makeText(getApplicationContext(),"ההזמנה נוספה בהצלחה",Toast.LENGTH_SHORT).show();
                            progressBar.setVisibility(View.INVISIBLE);
                            status.setText("Invoice");
                            status.setBackgroundResource(R.color.lightgreen);
                        }
                        else
                        {
                            progressBar.setVisibility(View.INVISIBLE);
                            Toast.makeText(getApplicationContext(),"הוספת ההזמנות נכשלה",Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressBar.setVisibility(View.INVISIBLE);
                }
            });
            Mysingleton.getInstance(getApplicationContext()).addToRequestQueue(request);
        }
        else
        {
            Toast.makeText(getApplicationContext(),"בדוק את החיבור לאינטרנט",Toast.LENGTH_SHORT).show();
        }
    }


    private static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener
    {
        private GestureDetector gestureDetector;
        private Previous_orders.RecyclerTouchListener.ClickListener clickListener;

        public RecyclerTouchListener(Context applicationContext, final RecyclerView recyclerView, final Previous_orders.RecyclerTouchListener.ClickListener clickListener)
        {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(applicationContext, new GestureDetector.SimpleOnGestureListener()
            {
                @Override
                public boolean onSingleTapUp(MotionEvent e)
                {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e)
                {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null)
                    {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e)
        {
            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e))
            {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }
        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e)
        {
        }
        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept)
        {
        }
        public interface ClickListener
        {
            void onClick(View view, int position);
            void onLongClick(View view, int position);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case android.R.id.home:
                if (stat_back.equals("2"))
                {
                    Intent intent = new Intent(Previous_orders.this, MainActivity.class);
                    intent.setFlags(intent.getFlags() | Intent.FLAG_ACTIVITY_SINGLE_TOP); // | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK
                    intent.putExtra("UserId",userid);
                    startActivity(intent);
                }else
                {
                    finish();
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed()
    {
        if (stat_back.equals("2"))
        {
            Intent intent = new Intent(Previous_orders.this, MainActivity.class);
            intent.setFlags(intent.getFlags() | Intent.FLAG_ACTIVITY_SINGLE_TOP); // | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK
            intent.putExtra("UserId",userid);
            startActivity(intent);
        }else
        {
            super.onBackPressed();
        }
    }
}