package makos.com.carrent.Car_list;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import makos.com.carrent.MainActivity;
import makos.com.carrent.R;
import makos.com.carrent.back_to_order.Back_order;

/**
 * Created by PC1 on 01-12-2018.
 */

public class Car_List_Adapter extends RecyclerView.Adapter<Car_List_Adapter.Car_List_View>
{
    Activity activity;
    ArrayList<Car_List_Model> model = new ArrayList<Car_List_Model>();
    String selected_car = "";
    boolean status = false,stat = false;
    String car = "";
    int pos=0;

    ArrayList<String> selected_cars = new ArrayList<String>();

    public Car_List_Adapter()
    {
    }

    public Car_List_Adapter(Activity activity, ArrayList<Car_List_Model> model)
    {
        this.activity = activity;
        this.model = model;
    }

    @NonNull
    @Override
    public Car_List_View onCreateViewHolder(@NonNull ViewGroup viewGroup, int i)
    {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.custom_car_list,viewGroup,false);//.inflate(R.layout.custom_car_list,viewGroup,null);
        Car_List_View carListView = new Car_List_View(view);
        return carListView;
    }

    @Override
    public void onBindViewHolder(@NonNull final Car_List_View holder, final int position) {
        final RelativeLayout layout = holder.layout;
        TextView carid = holder.carid;
        TextView carname = holder.carname;
        TextView carcategory = holder.carcategory;
        TextView carprice = holder.carprice;
        ImageView img = holder.carimg;
        final FrameLayout flayout = holder.flayout;

        // carid.setText(model.get(position).getCar_id());
        carname.setText(Html.fromHtml(model.get(position).getCar_name()).toString());
        carcategory.setText(Html.fromHtml(model.get(position).getCar_category()));
        carprice.setText(String.format("%.2f",Double.parseDouble(model.get(position).getPrice())) + "₪");
        //Picasso.with(activity).load(model.get(position).getImg()).error(R.drawable.carlogo).placeholder(R.drawable.carlogo).into(img);
        Picasso.with(activity).load(model.get(position).getImg()).placeholder(R.drawable.lesss_for_u).error(R.drawable.lesss_for_u).fit().into(img);
       // model.get(position).setDefault();
       // pos = position;

       // layout.setFocusable(false);
        layout.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
               //model.get(position).setSelected(!model.get(position).isSelected());
               /* Log.e("herestart", String.valueOf(model.get(position).isSelected()));
                if (model.get(position).isSelected() == false)
                {
                    model.get(position).setSelected(true);
                    //notifyDataSetChanged();
                    layout.setBackgroundResource(R.color.greenlight);
                }else
                {
                    model.get(position).setSelected(false);
                    //notifyDataSetChanged();
                    layout.setBackgroundResource(R.color.transparent);
                }

                Log.e("hereend", String.valueOf(model.get(position).isSelected()));

                notifyDataSetChanged();
                Log.e("herestatus", String.valueOf(model.get(position).isSelected()) + " " + position);

                if (model.get(position).isSelected())
                {
                    selected_cars.add(model.get(position).getCar_id());
                }else
                {
                    selected_cars.remove(model.get(position).getCar_id());
                }

                Log.e("heresize", String.valueOf(selected_cars.size()) + " " + position);*/

            }
        });
      //  Log.e("heresizeout", String.valueOf(selected_cars) + " " + position);
        //Log.e("heresizeout", String.valueOf(selected_cars) + " " + position);

      // for (int i = 0;i<selected_cars.size() ;i++)
       // {

      /*  if (position < selected_cars.size())
        {
            if (model.get(position).isSelected())
            {
                if (selected_car.equals(""))
                {
                    selected_car = selected_cars.get(position);
                }else if (!selected_car.contains(selected_cars.get(position)))
                {
                    selected_car = selected_car + "," + selected_cars.get(position);
                }
            }else
            {
                selected_car.replace(selected_cars.get(position),"");
            }
        }*/

        //}
       // Log.e("selectedcarstring",selected_car);

    }

    public void updateData(ArrayList<Car_List_Model> viewModels) {
       // model.clear();
       // model.addAll(viewModels);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount()
    {
        return model.size();
    }

    public static class Car_List_View extends RecyclerView.ViewHolder
    {
        RelativeLayout layout;
        TextView carid, carname, carcategory, carprice;
        ImageView carimg;
        FrameLayout flayout;
        public Car_List_View(@NonNull View itemView)
        {
            super(itemView);
            layout = itemView.findViewById(R.id.layout);
            //carid = itemView.findViewById(R.id.carid);
            carname = itemView.findViewById(R.id.carname);
            carcategory = itemView.findViewById(R.id.category);
            carprice = itemView.findViewById(R.id.price);
            carimg=itemView.findViewById(R.id.img);
            flayout = itemView.findViewById(R.id.flContent);
        }
    }

    public ArrayList<String> getArraylist()
    {
        return selected_cars;
    }
}
