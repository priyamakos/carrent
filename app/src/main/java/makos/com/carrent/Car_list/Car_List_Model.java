package makos.com.carrent.Car_list;

import android.util.Log;

/**
 * Created by PC1 on 01-12-2018.
 */

public class Car_List_Model
{
    private String car_id;
    private String company_name;
    private String car_name;
    private String car_category;
    private String price;
    private String original_car_price;
    private String advance_price;
    private String img;
    private boolean selected = false;


    public String getAdvance_price() {
        return advance_price;
    }

    public void setAdvance_price(String advance_price) {
        this.advance_price = advance_price;
    }

    public String getOriginal_car_price() {
        return original_car_price;
    }

    public void setOriginal_car_price(String original_car_price) {
        this.original_car_price = original_car_price;
    }

    public boolean isSelected()
    {
        return selected;
    }

    public void setSelected(boolean selected) {
        Log.e("setselected", String.valueOf(this.selected));
        this.selected = selected;
    }

    public void setDefault()
    {
        this.selected = false;
    }

    public boolean getdefault()
    {
        return this.selected;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }



    public String getCar_id() {
        return car_id;
    }

    public void setCar_id(String car_id) {
        this.car_id = car_id;
    }

    public String getCompany_name() {
        return company_name;
    }

    public void setCompany_name(String company_name) {
        this.company_name = company_name;
    }

    public String getCar_name() {
        return car_name;
    }

    public void setCar_name(String car_name) {
        this.car_name = car_name;
    }

    public String getCar_category() {
        return car_category;
    }

    public void setCar_category(String car_category) {
        this.car_category = car_category;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}
