package makos.com.carrent.Car_list;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import makos.com.carrent.R;
import makos.com.carrent.invoice.Extras_Model;

/**
 * Created by PC1 on 17-12-2018.
 */

public class Extra_list_Adapter extends RecyclerView.Adapter<Extra_list_Adapter.Extra_list_viewholder>
{
    ArrayList<Extras_Model> models = new ArrayList<Extras_Model>();
    Activity activity;

    public Extra_list_Adapter(ArrayList<Extras_Model> models, Activity activity)
    {
        this.models = models;
        this.activity = activity;
    }

    @NonNull
    @Override
    public Extra_list_viewholder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i)
    {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.custom_extra_list,viewGroup,false);
        Extra_list_viewholder holder = new Extra_list_viewholder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull Extra_list_viewholder holder, int position)
    {
        TextView name = holder.name;
        TextView price = holder.price;

        Log.e("statushereextra", String.valueOf(models.get(position).isSelected()));
        name.setText(Html.fromHtml(models.get(position).getExtra_name().toString()));
        price.setText(models.get(position).getPrice() + "₪");
    }

    @Override
    public int getItemCount()
    {
        return models.size();
    }

    public static class Extra_list_viewholder extends RecyclerView.ViewHolder
    {
        TextView name,price;
        public Extra_list_viewholder(@NonNull View itemView)
        {
            super(itemView);

            name = itemView.findViewById(R.id.extraname);
            price = itemView.findViewById(R.id.extraprice);
        }
    }
}
