package makos.com.carrent.Car_list;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;

import makos.com.carrent.Mysingleton.Mysingleton;
import makos.com.carrent.NetworkInfo.Connectivity;
import makos.com.carrent.R;
import makos.com.carrent.back_to_order.Back_order;
import makos.com.carrent.statusurl.Staticurl;

public class Car_list extends AppCompatActivity
{
    RelativeLayout layout;
    TextView msg,terms;
    RecyclerView recyclerView;
    RecyclerView.Adapter adapter;
    RecyclerView.LayoutManager layoutManager;
    Button submit;

    String comapny_id = "";
    String comp_id = "";
    String category_id = "";

    String selected_car = "";
    String selected_carnames = "";
    String selected_carimgs = "";
    String selected_carcomp = "";
    String selected_carprices = "";

    String user_id = "";
    String company_name = "";
    String category_name = "";
    String order_date = "";
    String order_time = "";
    String return_date = "";
    //String category_name = "";
    double car_price = 0.0;

    ProgressBar progressBar;
    boolean status = false;

    SharedPreferences preferences;
    SharedPreferences.Editor editor;

    Car_List_Adapter car = new Car_List_Adapter();
    ArrayList<String> sel_cars = new ArrayList<String>();
    ArrayList<String> selected_cars = new ArrayList<String>();
    ArrayList<String> selcar_names = new ArrayList<String>();
    ArrayList<String> selcar_imgs = new ArrayList<String>();
    ArrayList<String> selcar_compnames = new ArrayList<String>();

    public ArrayList<Car_List_Model> models;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_car_list);

        progressBar=findViewById(R.id.progressbar);

        preferences = getSharedPreferences("makos.com.carrent",MODE_PRIVATE);
        editor = preferences.edit();

        user_id = preferences.getString("USER_ID","0");

        Intent in = getIntent();
        comapny_id = in.getStringExtra("company_id");
        comp_id = in.getStringExtra("comp_id");
        category_id = in.getStringExtra("category_id");
        company_name = in.getStringExtra("company_name");//city name
        category_name = in.getStringExtra("category_name");
        //user_id = in.getStringExtra("user_id");
        order_date = in.getStringExtra("order_date");
        order_time = in.getStringExtra("order_time");
        return_date = in.getStringExtra("return_date");

        submit = findViewById(R.id.submit);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {

                if (selected_cars.size() == 0)
                {
                    Toast.makeText(getApplicationContext(),"לא ניתן להמשיך בהזמנה לפני בחירת הרכב",Toast.LENGTH_SHORT).show();
                }else
                {
                    Log.e("heresize", String.valueOf(selected_cars.size()));
                    for (int i = 0;i<selected_cars.size();i++)
                    {
                        if (selected_car.equals(""))
                        {
                            selected_car = selected_cars.get(i).toString();
                            selected_carnames = selcar_names.get(i).toString();
                            selected_carcomp = selcar_compnames.get(i).toString();
                            selected_carimgs = selcar_imgs.get(i).toString();
                            selected_carprices = sel_cars.get(i).toString();

                        }else if (!selected_car.contains(selected_cars.get(i).toString()))
                        {
                            selected_car = selected_car + "," + selected_cars.get(i).toString();
                            selected_carnames = selected_carnames + "," + selcar_names.get(i).toString();
                            selected_carcomp = selected_carcomp + "," + selcar_compnames.get(i).toString();
                            selected_carimgs = selected_carimgs + "," + selcar_imgs.get(i).toString();
                            selected_carprices = selected_carprices + "," + sel_cars.get(i).toString();
                        }/*else if (i == (selected_cars.size()-1))
                        {
                            selected_car = selected_car + selected_cars.get(i).toString();
                        }*/
                        car_price = car_price + Double.parseDouble(sel_cars.get(i));
                    }

                    Log.e("selectedcarsprice",selected_car);
                    Log.e("selectedcarsprice", String.valueOf(car_price));

                    Intent back = new Intent(getApplicationContext(),Insurance_list.class);
                    back.setFlags(back.getFlags() | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    back.putExtra("company_id",comapny_id);
                    back.putExtra("user_id",user_id);
                    back.putExtra("category_id",category_id);
                    back.putExtra("company_name",company_name);//city name
                    back.putExtra("category_name",category_name);
                    back.putExtra("order_date",order_date);
                    back.putExtra("order_time",order_time);
                    back.putExtra("return_date",return_date);
                    back.putExtra("selected_car_id",selected_car);
                    back.putExtra("car_price",car_price);

                    back.putExtra("selected_carnames",selected_carnames);
                    back.putExtra("selected_carcomp",selected_carcomp);
                    back.putExtra("selected_carimgs",selected_carimgs);
                    back.putExtra("selected_carprices",selected_carprices);

                    startActivity(back);
                }
            }
        });

        msg = findViewById(R.id.nocars);
        models = new ArrayList<Car_List_Model>();
        recyclerView = findViewById(R.id.recycler);

        adapter = new Car_List_Adapter(Car_list.this,models);
        layoutManager = new LinearLayoutManager(getApplicationContext());

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

        comparecars();

        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), recyclerView,
                new RecyclerTouchListener.ClickListener() {
                    @SuppressLint("ResourceAsColor")
                    @Override
                    public void onClick(final View view, final int position)
                    {
                        terms = view.findViewById(R.id.terms);
                        layout = view.findViewById(R.id.layouts);

                        terms.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v)
                            {
                                getTerms();
                            }
                        });

                        layout.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v)
                            {
                                Log.e("herestart", String.valueOf(models.get(position).isSelected()));
                                if (models.get(position).isSelected() == false)
                                {
                                    models.get(position).setSelected(true);
                                    //notifyDataSetChanged();
                                    view.setBackgroundResource(R.color.greenlight);
                                }else
                                {
                                    models.get(position).setSelected(false);
                                    //notifyDataSetChanged();
                                    view.setBackgroundResource(R.color.transparent);
                                }

                                Log.e("hereend", String.valueOf(models.get(position).isSelected()));

                                adapter.notifyDataSetChanged();
                                Log.e("herestatus", String.valueOf(models.get(position).isSelected()) + " " + position);

                                if (models.get(position).isSelected())
                                {
                                    selected_cars.add(models.get(position).getCar_id());
                                    selcar_names.add(models.get(position).getCar_name());
                                    selcar_imgs.add(models.get(position).getImg());
                                    selcar_compnames.add(models.get(position).getCompany_name());
                                    sel_cars.add(models.get(position).getPrice());

                                    Log.e("carids",models.get(position).getCar_id());
                                }else
                                {
                                    selected_cars.remove(models.get(position).getCar_id());
                                    sel_cars.remove(models.get(position).getPrice());
                                    selcar_names.remove(models.get(position).getCar_name());
                                    selcar_imgs.remove(models.get(position).getImg());
                                    selcar_compnames.remove(models.get(position).getCompany_name());
                                }

                                Log.e("heresize", String.valueOf(selected_cars.size()) + " " + position);
                            }
                        });

                    }
                    @Override
                    public void onLongClick(View view, int position)
                    {

                    }
                }));

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    private static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private Car_list.RecyclerTouchListener.ClickListener clickListener;

        public RecyclerTouchListener(Context applicationContext, final RecyclerView recyclerView, final Car_list.RecyclerTouchListener.ClickListener clickListener)
        {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(applicationContext, new GestureDetector.SimpleOnGestureListener()
            {
                @Override
                public boolean onSingleTapUp(MotionEvent e)
                {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e)
                {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null)
                    {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }
        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e)
        {
            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e))
            {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }
        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e)
        {
        }
        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept)
        {
        }
        public interface ClickListener
        {
            void onClick(View view, int position);
            void onLongClick(View view, int position);
        }
    }

    private void getTerms()
    {
        String gettermsurl = Staticurl.getterms;
        progressBar.setVisibility(View.VISIBLE);

        if (Connectivity.isConnected(getApplicationContext()))
        {
            final StringRequest request = new StringRequest(Request.Method.GET, gettermsurl, new Response.Listener<String>() {
                @Override
                public void onResponse(String response)
                {
                    try
                    {
                        JSONArray jsonArray = new JSONArray(response);

                        JSONObject jsonObject = jsonArray.getJSONObject(0);

                        if (jsonObject.getString("Status").equals("True"))
                        {
                            progressBar.setVisibility(View.INVISIBLE);
                            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(jsonObject.getString("Terms")));//,Uri.parse(jsonObject.getString("Terms"))
                            //intent.setDataAndType(Uri.parse(jsonObject.getString("Terms")), "text/html");
                            getApplicationContext().startActivity(intent);
                        }
                        else {
                            progressBar.setVisibility(View.INVISIBLE);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressBar.setVisibility(View.INVISIBLE);
                }
            });
            Mysingleton.getInstance(getApplicationContext()).addToRequestQueue(request);
        }
        else
        {
            Toast.makeText(getApplicationContext(),"בדוק את החיבור לאינטרנט",Toast.LENGTH_SHORT).show();
        }
    }

    private void comparecars()
    {
        models.clear();
        String compareurl = null;

        compareurl = Staticurl.comparecar + "?branch="+ comp_id//city name
                + "&category_id="+ category_id;

        Log.e("compareurl",compareurl);

        progressBar.setVisibility(View.VISIBLE);

        if (Connectivity.isConnected(getApplicationContext()))
        {
            final StringRequest request = new StringRequest(Request.Method.GET, compareurl, new Response.Listener<String>() {
                @Override
                public void onResponse(String response)
                {

                    Log.e("compareresponce",response);
                    try
                    {
                        JSONArray jsonArray = new JSONArray(response);
                        JSONObject jsonObject = jsonArray.getJSONObject(0);

                        if (jsonObject.getString("Status").equals("True"))
                        {
                            JSONArray jsonArray1 = jsonObject.getJSONArray("Car_List");

                            for (int i=0;i<jsonArray1.length();i++)
                            {
                                JSONObject jsonObject1 = jsonArray1.getJSONObject(i);
                                Car_List_Model model = new Car_List_Model();

                                model.setCar_id(jsonObject1.getString("car_id"));
                                model.setCar_name(jsonObject1.getString("car_name"));
                                model.setCompany_name(jsonObject1.getString("company_name"));
                                model.setCar_category(jsonObject1.getString("car_category"));
                                model.setPrice(jsonObject1.getString("price"));
                                model.setImg(jsonObject1.getString("image_path"));

                                models.add(model);
                            }
                            progressBar.setVisibility(View.INVISIBLE);

                            if (models.size() == 0)
                            {
                                msg.setVisibility(View.VISIBLE);
                            }else
                            {
                                msg.setVisibility(View.INVISIBLE);
                            }
                            adapter.notifyDataSetChanged();
                        }else
                        {
                            progressBar.setVisibility(View.INVISIBLE);
                            msg.setVisibility(View.VISIBLE);
                            Toast.makeText(getApplicationContext(),"אין מכוניות זמינות",Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressBar.setVisibility(View.INVISIBLE);
                }
            });

            Mysingleton.getInstance(getApplicationContext()).addToRequestQueue(request);
        }
        else
        {
            Toast.makeText(getApplicationContext(),"בדוק את החיבור לאינטרנט",Toast.LENGTH_SHORT).show();
        }
    }


    /*private void car_list()
    {
        String carlisturl = Staticurl.carlist + "?company_id=" + comapny_id;

        Log.e("carlisturl",carlisturl);

        if (Connectivity.isConnected(getApplicationContext()))
        {
            StringRequest request = new StringRequest(Request.Method.GET, carlisturl, new Response.Listener<String>() {
                @Override
                public void onResponse(String response)
                {
                    Log.e("response",response);
                    try
                    {
                        JSONArray jsonArray = new JSONArray(response);

                        JSONObject jsonObject = jsonArray.getJSONObject(0);

                        if (jsonObject.getString("Status").equals("True"))
                        {
                            JSONArray jsonArray1 = jsonObject.getJSONArray("Car_List");

                            for (int i=0; i<jsonArray1.length(); i++)
                            {
                                JSONObject jsonObject1 = jsonArray1.getJSONObject(i);

                                Car_List_Model model = new Car_List_Model();
                                model.setCar_id(jsonObject1.getString("car_id"));
                                model.setCar_name(jsonObject1.getString("car_name"));
                                model.setCar_category(jsonObject1.getString("car_category"));
                                model.setCompany_name(jsonObject1.getString("company_name"));
                                model.setPrice(jsonObject1.getString("price"));
                                model.setImg(jsonObject1.getString("image_path"));

                                models.add(model);
                            }

                            adapter.notifyDataSetChanged();

                            Log.e("selectedcars",selected_car);
                        }
                        else
                        {
                            Toast.makeText(getApplicationContext(),jsonObject.getString("Message"),Toast.LENGTH_SHORT).show();
                        }
                        if (models.size() == 0)
                        {
                            msg.setVisibility(View.VISIBLE);
                        }else
                        {
                            Log.d("statushere", String.valueOf(models.get(0).isSelected()));
                            msg.setVisibility(View.INVISIBLE);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });
            Mysingleton.getInstance(getApplicationContext()).addToRequestQueue(request);
        }
        else
        {
            Toast.makeText(getApplicationContext(),"Check Internet Connection",Toast.LENGTH_SHORT).show();
        }
    }*/

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}