package makos.com.carrent.Car_list;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.res.ColorStateList;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import makos.com.carrent.R;

/**
 * Created by PC1 on 15-12-2018.
 */

public class Previous_order_Adpater extends RecyclerView.Adapter<Previous_order_Adpater.Previous_order_viewholder>
{
    Activity activity;
    ArrayList<Previous_orders_model> model = new ArrayList<Previous_orders_model>();

    public Previous_order_Adpater(Activity activity, ArrayList<Previous_orders_model> model)
    {
        this.activity = activity;
        this.model = model;
    }

    @NonNull
    @Override
    public Previous_order_viewholder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i)
    {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.order_folder,viewGroup,false);
        Previous_order_viewholder holder = new Previous_order_viewholder(view);
        return holder;
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(@NonNull Previous_order_viewholder holder, int position)
    {
        TextView orderno = holder.orderno;
        TextView invoiceno = holder.invoiceno;
        TextView billamt = holder.billamt;
        TextView status = holder.status;

        orderno.setText(model.get(position).getOrder_no());
        invoiceno.setText(model.get(position).getInvoice_no());
        billamt.setText(String.format("%.2f",Double.parseDouble(model.get(position).getBill_amount())) + "₪");

        //status.setFocusable(false);

        if (model.get(position).getPaymnet_status().equals("Pending"))
        {
            status.setText("הצג הזמנה");
            status.setBackgroundResource(R.color.yellow);
        }else
        {
            status.setText(Html.fromHtml(model.get(position).getPaymnet_status()).toString());
            status.setBackgroundResource(R.color.lightgreen);
            status.setText("חשבונית");
        }
    }

    @Override
    public int getItemCount()
    {
        return model.size();
    }

    public static class Previous_order_viewholder extends RecyclerView.ViewHolder
    {
        TextView orderno,invoiceno,billamt,status;
        public Previous_order_viewholder(@NonNull View itemView)
        {
            super(itemView);

            orderno = itemView.findViewById(R.id.order_no);
            invoiceno = itemView.findViewById(R.id.invoice_no);
            billamt = itemView.findViewById(R.id.bill_amt);
            status = itemView.findViewById(R.id.payment_status);
        }
    }
}
