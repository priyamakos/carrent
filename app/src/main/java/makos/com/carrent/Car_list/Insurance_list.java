package makos.com.carrent.Car_list;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import makos.com.carrent.Mysingleton.Mysingleton;
import makos.com.carrent.NetworkInfo.Connectivity;
import makos.com.carrent.R;
import makos.com.carrent.back_to_order.Insurance_model;
import makos.com.carrent.statusurl.Staticurl;

public class Insurance_list extends AppCompatActivity
{
    TextView msg;
    RecyclerView recyclerView;
    RecyclerView.Adapter adapter;
    RecyclerView.LayoutManager layoutManager;
    ProgressBar progressBar;

    Button proceed;

    ArrayList<Insurance_model> insurance = new ArrayList<Insurance_model>();
    ArrayList<String> selected_insurance = new ArrayList<String>();
    ArrayList<String> selected_insuranceid = new ArrayList<String>();

    String userid = "";
    String company_name = "";
    String category_name = "";

    String selected_carname = "";
    String selected_carimgs = "";
    String selected_carcomp = "";
    String selected_carprices = "";

    String order_date = "";
    String order_time = "";
    String return_date = "";
    String insurance_status = "0";
    double car_price = 0;
    String selected_cars = "";
    String selected_insurances = "";

    String insurance_title = "";
    String insurance_desc = "";

    double insurance_sum = 0;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insurance_list);

        msg = findViewById(R.id.msg);

        progressBar=findViewById(R.id.progressbar);
        final Intent intent = getIntent();
        //userid = intent.getStringExtra("user_id");
        company_name = intent.getStringExtra("company_name");
        order_date = intent.getStringExtra("order_date");
        order_time = intent.getStringExtra("order_time");
        return_date = intent.getStringExtra("return_date");
        car_price = intent.getDoubleExtra("car_price",0);
        selected_cars = intent.getStringExtra("selected_car_id");
        category_name = intent.getStringExtra("category_name");

        selected_carname = intent.getStringExtra("selected_carnames");
        selected_carcomp = intent.getStringExtra("selected_carcomp");
        selected_carimgs = intent.getStringExtra("selected_carimgs");
        selected_carprices = intent.getStringExtra("selected_carprices");

        proceed = findViewById(R.id.submit);

        recyclerView = findViewById(R.id.insurancerecycler);
        adapter = new Insurance_Adapter(insurance,Insurance_list.this);
        layoutManager = new LinearLayoutManager(getApplicationContext());

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

        preferences = getSharedPreferences("makos.com.carrent",MODE_PRIVATE);
        editor = preferences.edit();

        userid = preferences.getString("USER_ID","0");
        insurancelist();

        recyclerView.addOnItemTouchListener(new Insurance_list.RecyclerTouchListener(getApplicationContext(), recyclerView,
                new Insurance_list.RecyclerTouchListener.ClickListener() {
                    @SuppressLint("ResourceAsColor")
                    @Override
                    public void onClick(View view, int position)
                    {
                        Log.e("herestart", String.valueOf(insurance.get(position).isSelected()));
                        if (insurance.get(position).isSelected() == false)
                        {
                            for (int i = 0;i < insurance.size(); i++)
                            {
                                if (insurance.get(i).isSelected())
                                {
                                    insurance.get(position).setSelected(false);

                                    //notifyDataSetChanged();
                                    insurance.get(i).setColor(R.color.transparent);
                                }
                            }
                            insurance.get(position).setSelected(true);
                            //notifyDataSetChanged();
                            insurance.get(position).setColor(R.color.greenlight);
                        }else
                        {
                            insurance.get(position).setSelected(false);
                            //notifyDataSetChanged();
                            insurance.get(position).setColor(R.color.transparent);
                        }

                        Log.e("hereend", String.valueOf(insurance.get(position).isSelected()));

                        adapter.notifyDataSetChanged();
                        Log.e("herestatus", String.valueOf(insurance.get(position).isSelected()) + " " + position);

                        /*if (insurance.get(position).isSelected())
                        {
                            selected_insurance.add(insurance.get(position).getInsurance_amt());
                            selected_insuranceid.add(insurance.get(position).getInsurance_id());
                        }else
                        {
                            selected_insurance.remove(insurance.get(position).getInsurance_amt());
                            selected_insuranceid.remove(insurance.get(position).getInsurance_id());
                        }*/

                        Log.e("heresize", String.valueOf(selected_insurance.size()) + " " + position);
                    }
                    @Override
                    public void onLongClick(View view, int position)
                    {

                    }
                }));

        proceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Log.e("heresize", String.valueOf(selected_insurance.size()));
                for (int i = 0; i < insurance.size(); i++)
                {
                    insurance_status = "1";
                    //  insurance_sum = insurance_sum + Double.parseDouble(selected_insurance.get(i));

                    if (insurance.get(i).isSelected())
                    {
                        insurance_sum = Double.parseDouble(insurance.get(i).getInsurance_amt());
                        selected_insurances = insurance.get(i).getInsurance_id();
                        insurance_title = insurance.get(i).getInsurance_title().toString();
                        insurance_desc = insurance.get(i).getInsurance_details().toString();
                    }
                }
                Log.e("insurance_sum", String.valueOf(insurance_sum));

                Intent intent1 = new Intent(Insurance_list.this,Insurance_Extra_list.class);
                intent1.setFlags(intent1.getFlags() | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                intent1.putExtra("user_id",userid);
                intent1.putExtra("company_name",company_name);//city name
                intent1.putExtra("category_name",category_name);

                intent1.putExtra("insurance_status",selected_insurances);
                intent1.putExtra("insurance_price",insurance_sum);
                intent1.putExtra("insurance_title",insurance_title);
                intent1.putExtra("insurance_descr",insurance_desc);

                intent1.putExtra("order_date",order_date);
                intent1.putExtra("order_time",order_time);
                intent1.putExtra("return_date",return_date);
                intent1.putExtra("selected_car_id",selected_cars);

                intent1.putExtra("selected_carnames",selected_carname);
                intent1.putExtra("selected_carcomp",selected_carcomp);
                intent1.putExtra("selected_carimgs",selected_carimgs);
                intent1.putExtra("selected_carprices",selected_carprices);

//                Log.e("selected_cars_show",selected_cars);
                intent1.putExtra("car_price",car_price);
                startActivity(intent1);
            }
        });

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    private void insurancelist()
    {
        insurance.clear();
        String insuranceurl = Staticurl.insurance;

        progressBar.setVisibility(View.VISIBLE);
        if (Connectivity.isConnected(getApplicationContext()))
        {
            StringRequest request = new StringRequest(Request.Method.GET, insuranceurl, new Response.Listener<String>() {
                @Override
                public void onResponse(String response)
                {
                    Log.d("insurance_responce",response);
                    try
                    {
                        JSONArray jsonArray = new JSONArray(response);
                        JSONObject jsonObject = jsonArray.getJSONObject(0);

                        if (jsonObject.getString("Status").equals("True"))
                        {
                            JSONArray jsonArray1 = jsonObject.getJSONArray("Insurance Data");

                            for (int i=0; i<jsonArray1.length(); i++)
                            {
                                JSONObject jsonObject1 = jsonArray1.getJSONObject(i);
                                Insurance_model model = new Insurance_model();

                                model.setInsurance_id(jsonObject1.getString("insurance_id"));
                                model.setInsurance_title(jsonObject1.getString("insurance_title"));
                                model.setInsurance_details(jsonObject1.getString("insurance_details"));
                                model.setInsurance_amt(jsonObject1.getString("insurance_amt"));

                                insurance.add(model);
                                //insurances.add(jsonObject1.getString("insurance_details"));
                            }
                            progressBar.setVisibility(View.INVISIBLE);

                            if (insurance.size() == 0)
                            {
                                msg.setVisibility(View.VISIBLE);
                            }else
                            {
                                msg.setVisibility(View.INVISIBLE);
                            }

                            adapter.notifyDataSetChanged();

                            /*insurancespin.setAdapter(new ArrayAdapter<String>(Insurance_Extra_list.this,R.layout.spiner,insurances));
                            insurancespin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
                                {
                                    insurance_status = "1";
                                    insuranceprice = insuranceprice + Double.parseDouble(insurance.get(insurancespin.getSelectedItemPosition()).getInsurance_amt());
                                    Log.e("insurancebill", String.valueOf(insuranceprice));
                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {

                                }
                            });*/
                         /*if (insurancespin.getSelectedItemPosition() > 0)
                         {
                             Log.d("ok","ok");
                             insurance_status = "1";
                             insuranceprice = insuranceprice + Double.parseDouble(insurance.get(insurancespin.getSelectedItemPosition()).getInsurance_amt());
                             Log.e("insurancebill", String.valueOf(insuranceprice));
                         }*/
                        }
                        else
                        {
                            msg.setVisibility(View.VISIBLE);
                            progressBar.setVisibility(View.INVISIBLE);
                            Toast.makeText(getApplicationContext(),"אין ביטוח זמין",Toast.LENGTH_SHORT).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressBar.setVisibility(View.INVISIBLE);
                }
            });
            Mysingleton.getInstance(getApplicationContext()).addToRequestQueue(request);
        }else
        {
            Toast.makeText(getApplicationContext(),"בדוק את החיבור לאינטרנט",Toast.LENGTH_SHORT).show();
        }
    }

    private static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private Insurance_list.RecyclerTouchListener.ClickListener clickListener;

        public RecyclerTouchListener(Context applicationContext, final RecyclerView recyclerView, final Insurance_list.RecyclerTouchListener.ClickListener clickListener)
        {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(applicationContext, new GestureDetector.SimpleOnGestureListener()
            {
                @Override
                public boolean onSingleTapUp(MotionEvent e)
                {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e)
                {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null)
                    {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }
        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e)
        {
            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e))
            {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }
        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e)
        {
        }
        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept)
        {
        }
        public interface ClickListener
        {
            void onClick(View view, int position);
            void onLongClick(View view, int position);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}