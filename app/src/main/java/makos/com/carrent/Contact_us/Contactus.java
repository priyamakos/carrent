package makos.com.carrent.Contact_us;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;

import makos.com.carrent.Mysingleton.Mysingleton;
import makos.com.carrent.NetworkInfo.Connectivity;
import makos.com.carrent.R;
import makos.com.carrent.statusurl.Staticurl;

public class Contactus extends AppCompatActivity
{
    RecyclerView recyclerView;
    RecyclerView.Adapter adapter;
    RecyclerView.LayoutManager layoutManager;
    ProgressBar progressBar;

    EditText first_name,phnum,message;
    Button send;

    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    String user_id = "";

    ArrayList<Contact_Model> models = new ArrayList<Contact_Model>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contactus);

        first_name=findViewById(R.id.firstname);
        phnum=findViewById(R.id.phnum);
        message=findViewById(R.id.msg);
        send=findViewById(R.id.send);
        progressBar = findViewById(R.id.progressbar);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                validation();
            }
        });

        preferences = getSharedPreferences("makos.com.carrent",MODE_PRIVATE);
        editor = preferences.edit();

        user_id = preferences.getString("USER_ID","0");

      /*  recyclerView = findViewById(R.id.recycler);
        progressBar=findViewById(R.id.progressbar);

        adapter = new Contact_Adapter(Contactus.this,models);
        layoutManager = new LinearLayoutManager(getApplicationContext());

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

        contactus();*/

    }

    private void validation() {

        if (first_name.getText().toString().equals("")) {
            Toast.makeText(getApplicationContext(), "הזן שם פרטי", Toast.LENGTH_SHORT).show();
        } else if (phnum.getText().toString().equals("")) {
            Toast.makeText(getApplicationContext(), "הזן מספר טלפון", Toast.LENGTH_SHORT).show();
        } else if (isValidMobile(phnum.getText().toString())) {
            if (message.getText().toString().equals("")) {
                Toast.makeText(getApplicationContext(), "הזן הודעה", Toast.LENGTH_SHORT).show();
            } else {
               // Toast.makeText(getApplicationContext(), "API call", Toast.LENGTH_SHORT).show();
               // clear();
                contact_us();
            }
        }
    }

    private void contact_us()
    {
        //?user_id=5&first_name=Akanksha&phone_no=9561639420&message=Test
        progressBar.setVisibility(View.VISIBLE);
        String contact_us_url = "";
        try {
            contact_us_url = Staticurl.contact_us
                    + "?user_id=" + user_id
                    + "&first_name=" + URLEncoder.encode(Html.toHtml(first_name.getText()), "UTF-8").toString()
                    + "&phone_no=" + phnum.getText().toString()
                    + "&message=" + URLEncoder.encode(Html.toHtml(message.getText()), "UTF-8").toString();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        Log.e("contact_us_url",contact_us_url);

        if (Connectivity.isConnected(Contactus.this))
        {
            StringRequest request = new StringRequest(Request.Method.GET, contact_us_url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response)
                {
                    Log.e("contact_us_responce",response);
                    try
                    {
                        JSONArray jsonArray = new JSONArray(response);
                        JSONObject jsonObject = jsonArray.getJSONObject(0);

                        if (jsonObject.getString("Status").equals("True"))
                        {
                            progressBar.setVisibility(View.INVISIBLE);
                            Toast.makeText(Contactus.this,"הודעה נשלחה בהצלחה",Toast.LENGTH_SHORT).show();
                            clear();
                        }else
                        {
                            progressBar.setVisibility(View.INVISIBLE);
                            Toast.makeText(Contactus.this,"שגיאה בשליחת הודעה",Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });
            Mysingleton.getInstance(getApplicationContext()).addToRequestQueue(request);
        }
        else
        {
            progressBar.setVisibility(View.INVISIBLE);
            Toast.makeText(getApplicationContext(),"בדוק את החיבור לאינטרנט",Toast.LENGTH_SHORT).show();
        }
    }

    private void clear() {
        first_name.setText("");
        phnum.setText("");
        message.setText("");
    }

    private boolean isValidMobile(String phone) {
        boolean check=false;
        //if(!Pattern.matches("[a-zA-Z]+", phone))
        //{
        if(phone.length() == 10)// || phone.length() > 0
        {
            //if(phone.length() != 10)
            // {
            check = true;

            Log.d("motest", "false");

            //txtPhone.setError("Not Valid Number");
            // }
        } else {
            Toast.makeText(getApplicationContext(), "מספר הטלפון הנייד אינו חוקי", Toast.LENGTH_SHORT).show();
            Log.d("motest", "true");
            check = false;
        }
        //} else
        // {
        //   check=false;
        //}
        return check;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

     /*private void contactus()
    {
        String contacturl = Staticurl.contactus;

        progressBar.setVisibility(View.VISIBLE);

        if (Connectivity.isConnected(getApplicationContext()))
        {
            StringRequest  request = new StringRequest(Request.Method.GET, contacturl, new Response.Listener<String>() {
                @Override
                public void onResponse(String response)
                {
                    try
                    {
                        JSONArray jsonArray = new JSONArray(response);
                        JSONObject jsonObject = jsonArray.getJSONObject(0);

                        if (jsonObject.getString("Status").equals("True"))
                        {
                            JSONArray jsonArray1 = jsonObject.getJSONArray("Contact Details");

                            for (int i = 0; i < jsonArray1.length() ; i ++)
                            {
                                JSONObject jsonObject1 = jsonArray1.getJSONObject(i);
                                Contact_Model model = new Contact_Model();

                                model.setComapnyname(jsonObject1.getString("company_name"));
                                model.setCompanyaddress(jsonObject1.getString("address"));
                                model.setCompannyemail(jsonObject1.getString("email_id"));
                                model.setCompanyphone(jsonObject1.getString("mobile"));

                                models.add(model);
                            }
                            progressBar.setVisibility(View.INVISIBLE);
                            adapter.notifyDataSetChanged();
                        }
                        else
                        {
                            progressBar.setVisibility(View.INVISIBLE);
                            Toast.makeText(getApplicationContext(),"אין אנשי קשר זמינים",Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressBar.setVisibility(View.INVISIBLE);
                }
            });
            Mysingleton.getInstance(getApplicationContext()).addToRequestQueue(request);
        }
        else
        {
            Toast.makeText(getApplicationContext(),"בדוק את חיבור האינטרנט",Toast.LENGTH_SHORT).show();
        }
    }*/
}
