package makos.com.carrent.Contact_us;

/**
 * Created by PC1 on 01-12-2018.
 */

public class Contact_Model
{
    private String comapnyname;
    private String compannyemail;
    private String companyaddress;
    private String companyphone;

    public String getComapnyname() {
        return comapnyname;
    }

    public void setComapnyname(String comapnyname) {
        this.comapnyname = comapnyname;
    }

    public String getCompannyemail() {
        return compannyemail;
    }

    public void setCompannyemail(String compannyemail) {
        this.compannyemail = compannyemail;
    }

    public String getCompanyaddress() {
        return companyaddress;
    }

    public void setCompanyaddress(String companyaddress) {
        this.companyaddress = companyaddress;
    }

    public String getCompanyphone() {
        return companyphone;
    }

    public void setCompanyphone(String companyphone) {
        this.companyphone = companyphone;
    }
}
