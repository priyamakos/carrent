package makos.com.carrent.Contact_us;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import makos.com.carrent.R;

/**
 * Created by PC1 on 01-12-2018.
 */

public class Contact_Adapter extends RecyclerView.Adapter<Contact_Adapter.Contact_Holder>
{
    Activity activity;
    ArrayList<Contact_Model> models = new ArrayList<Contact_Model>();

    public Contact_Adapter(Activity activity, ArrayList<Contact_Model> models)
    {
        this.activity = activity;
        this.models = models;
    }

    @NonNull
    @Override
    public Contact_Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i)
    {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.custom_contact_list,viewGroup,false);
        Contact_Holder holder = new Contact_Holder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull Contact_Holder holder, int position)
    {
        TextView name = holder.name;
        TextView address = holder.address;
        TextView email = holder.email;
        TextView phone = holder.phone;

        name.setText(Html.fromHtml(models.get(position).getComapnyname()).toString());
        address.setText(Html.fromHtml(models.get(position).getCompanyaddress()).toString());
        email.setText(Html.fromHtml(models.get(position).getCompannyemail()).toString());
        phone.setText(Html.fromHtml(models.get(position).getCompanyphone()).toString());
    }

    @Override
    public int getItemCount()
    {
        return models.size();
    }

    public static class Contact_Holder extends RecyclerView.ViewHolder
    {
        TextView name,address,email,phone;
        public Contact_Holder(@NonNull View itemView)
        {
            super(itemView);

            name = itemView.findViewById(R.id.companyname);
            address = itemView.findViewById(R.id.companyaddr);
            email = itemView.findViewById(R.id.companyemail);
            phone = itemView.findViewById(R.id.companyphone);
        }
    }
}
