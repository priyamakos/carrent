package makos.com.carrent;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.TextView;

public class yearmonthpicker extends AppCompatActivity {

    TextView month_name,year_name;
    Button ok,cancel;
    NumberPicker year_picker,month_picker;
    LinearLayout year_layout,month_layout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_yearmonthpicker);

        month_name=findViewById(R.id.month_name);
        year_name=findViewById(R.id.year_name);
        ok=findViewById(R.id.okbtn);
        cancel=findViewById(R.id.cancelbtn);
        year_picker=findViewById(R.id.year_picker);
        month_picker=findViewById(R.id.month_picker);
        year_layout=findViewById(R.id.linearlayout_year);
        month_layout=findViewById(R.id.linearlayout_month);

        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);

        month_picker.setMinValue(1);
        month_picker.setMaxValue(12);

        year_picker.setMinValue(2019);
        year_picker.setMaxValue(2055);

        month_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                month_picker.setVisibility(View.VISIBLE);
                month_layout.setVisibility(View.VISIBLE);
                year_picker.setVisibility(View.INVISIBLE);
                year_layout.setVisibility(View.GONE);
            }
        });

        year_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                month_picker.setVisibility(View.INVISIBLE);
                month_layout.setVisibility(View.GONE);
                year_layout.setVisibility(View.VISIBLE);
                year_picker.setVisibility(View.VISIBLE);


            }
        });


        year_picker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                year_name.setText(String.valueOf(year_picker.getValue()));

            }
        });

        month_picker.setOnScrollListener(new NumberPicker.OnScrollListener() {
            @Override
            public void onScrollStateChange(NumberPicker view, int scrollState) {
                month_name.setText(String.valueOf(month_picker.getValue()));
            }
        });

    }
}
