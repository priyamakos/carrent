package makos.com.carrent.statusurl;

public class Staticurl {

    public static String baseurl = "http://rentapp-env.av3qgyvtfy.ap-south-1.elasticbeanstalk.com/rentapp";

    public static String login = baseurl+"/login";

    public static String  register = baseurl+"/signup";

    public static String forgot_passsword = baseurl + "/forgot_pass?email_id=";

    public static String change_password = baseurl + "/change_password";//?id=1&old_password=123&new_password=1234

    public static String carlist = baseurl + "/company_wise_car_list";

    public static String qa = baseurl + "/questions_answers";

    public static String contactus = baseurl + "/contact_us";

    public static String invoice = baseurl + "/order_folder";//?cid=1

    public static String pointdetails = baseurl + "/getPoints";

    public static String userpoint = baseurl + "/get_user_points";

    public static String extralist = baseurl + "/extra_list";

    public static String insurance = baseurl + "/insurance_details";

    public static String companylist = baseurl + "/company_list";

    public static String getterms = baseurl + "/getTerms";

    public static String citylist=baseurl+"/city_list";

    public static String categorylist = baseurl + "/car_category_list";

    public static String comparecar = baseurl + "/branch_category_wise_car_list";//?company_id=1&category_id=1

    public static String make_an_order = baseurl + "/make_order";
    //?user_id=1&city=solapur&order_date=2018-10-11&order_time=10:30&return_date=2018-10-11&selected_car_ids=1,2,3,4&authority=1&extra_list_ids=1,2,3,4&insurance_status=0&bill_amt=1000&paid_amt=200&pending_amt=800&bill_status=0

    public static String previous_orders = baseurl + "/order_list";//?user_id=1

    public static String make_payment = baseurl + "/make_payment";
    //?order_id=1&bill_amt=1000&paid_amt=1000&pending_amt=0&bill_status=1

    public static String orderwise_invoice = baseurl + "/orderidwise_order_folder";
    //?order_id=1

  //  public static String uploadimg=baseurl+"/fileUploadServlet";

    public static String damage_list=baseurl+"/damage_list";

    public static String  order_damage_list=baseurl+"/damage_list_order_wise";

    public static String  remove_image=baseurl+"/remove_damage_image";

    public static String get_useprofile=baseurl+"/get_user_profile";

    public static String update_profile=baseurl+"/update_profile";

    public static String return_order = baseurl + "/return_order";

    public static String logout=baseurl+"/logout";

    public static String original_car_price = baseurl + "/car_list1";

    public static String used_points = baseurl + "/use_points";//?id=12&used_points=7

    public static String contact_us = baseurl + "/contact1";
    //?user_id=5&first_name=Akanksha&phone_no=9561639420&message=Test
}
