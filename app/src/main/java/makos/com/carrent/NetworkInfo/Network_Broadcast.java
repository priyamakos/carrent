package makos.com.carrent.NetworkInfo;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

public class Network_Broadcast extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if(!Connectivity.isConnected(context))
        {
            Toast.makeText(context, "No Internet Connection!", Toast.LENGTH_LONG).show();
        }
    }
}
