package makos.com.carrent.lobby;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import makos.com.carrent.Car_list.Car_list;
import makos.com.carrent.Company_list.Company_list;
import makos.com.carrent.Contact_us.Contactus;
import makos.com.carrent.QuestionAnswer.Question_Answer;
import makos.com.carrent.R;
import makos.com.carrent.back_to_order.Back_order;
import makos.com.carrent.points.Points;
import makos.com.carrent.profile.Profile;
import makos.com.carrent.shooting_damage.Cardemo;

public class  Lobby extends Fragment {

    CardView cart,person,shooting_dange,points,questions,contct_us;
    String userid="";

    SharedPreferences preferences;
    SharedPreferences.Editor editor;

    @Nullable
    @Override   
    public View onCreateView(@NonNull final LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_lobby, container, false);
        cart=view.findViewById(R.id.cardview);
        person=view.findViewById(R.id.cardview2);
        shooting_dange=view.findViewById(R.id.cardview3);
        points=view.findViewById(R.id.cardview4);
        questions=view.findViewById(R.id.cardview6);
        contct_us=view.findViewById(R.id.cardview5);

        Bundle bundle = getArguments();

        preferences = getActivity().getSharedPreferences("makos.com.carrent", Context.MODE_PRIVATE);

        userid = bundle.getString("userid");

        Log.e("useridhere",userid);


        contct_us.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1=new Intent(getActivity(),Contactus.class);
                intent1.setFlags(intent1.getFlags() | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent1);
            }
        });

        cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*Back_order order=new Back_order();
                FragmentManager fragmentManagers2= getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransactions2 = fragmentManagers2.beginTransaction();
                fragmentTransactions2.replace(R.id.flContent, order);
                fragmentTransactions2.addToBackStack(null);
                fragmentTransactions2.commit();*/

                Intent intent = new Intent(getActivity(),Back_order.class);
                intent.setFlags(intent.getFlags() | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                intent.putExtra("user_id",userid);
                startActivity(intent);
            }
        });

        person.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*Profile profile=new Profile();
                FragmentManager fragmentManagers11= getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransactions11 = fragmentManagers11.beginTransaction();
                fragmentTransactions11.replace(R.id.flContent, profile);
                fragmentTransactions11.addToBackStack(null);
                fragmentTransactions11.commit();*/

                Intent profile = new Intent(getActivity(),Profile.class);
                profile.setFlags(profile.getFlags() | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                profile.putExtra("user_id",userid);
                startActivity(profile);
            }
        });

        shooting_dange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              /* Cardemo damage=new Cardemo();
                FragmentManager fragmentManagers11= getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransactions11 = fragmentManagers11.beginTransaction();
                fragmentTransactions11.replace(R.id.flContent, damage);
                fragmentTransactions11.addToBackStack(null);
                fragmentTransactions11.commit();*/
              Intent in=new Intent(getActivity(),Cardemo.class);
              in.setFlags(in.getFlags() | Intent.FLAG_ACTIVITY_SINGLE_TOP);
              in.putExtra("userid",userid);
              startActivity(in);
            }
        });

        points.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*Points points=new Points();
                FragmentManager fragmentManagers11= getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransactions11 = fragmentManagers11.beginTransaction();
                fragmentTransactions11.replace(R.id.flContent, points);
                fragmentTransactions11.addToBackStack(null);
                fragmentTransactions11.commit();*/

                Intent points = new Intent(getActivity(),Points.class);
                points.setFlags(points.getFlags() | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(points);
            }
        });

        questions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*Question_Answer question=new Question_Answer();
                Bundle bundle1 =new Bundle();
                bundle1.putString("userid",userid);
                FragmentManager fragmentManagers12= getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransactions12 = fragmentManagers12.beginTransaction();
                question.setArguments(bundle1);
                fragmentTransactions12.replace(R.id.flContent, question);
                fragmentTransactions12.addToBackStack(null);
                fragmentTransactions12.commit();*/

                //lobby
                Intent question = new Intent(getActivity(),Question_Answer.class);
                question.setFlags(question.getFlags() | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(question);
            }
        });

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().setTitle("לובי");
    }


}
