package makos.com.carrent.QuestionAnswer;

/**
 * Created by PC1 on 01-12-2018.
 */

public class QA_Model
{
    private String question;
    private String answer;
    private boolean selected;

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }
}
