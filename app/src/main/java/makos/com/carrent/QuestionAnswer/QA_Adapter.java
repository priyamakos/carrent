package makos.com.carrent.QuestionAnswer;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import makos.com.carrent.R;

import static makos.com.carrent.R.*;

/**
 * Created by PC1 on 01-12-2018.
 */

public class QA_Adapter extends RecyclerView.Adapter<QA_Adapter.QA_Viewholder>
{
    Activity activity;
    ArrayList<QA_Model> qa = new ArrayList<QA_Model>();
    boolean status = false;

    public QA_Adapter(Activity activity, ArrayList<QA_Model> qa)
    {
        this.activity = activity;
        this.qa = qa;
    }

    @NonNull
    @Override
    public QA_Viewholder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i)
    {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.custom_qa_list,viewGroup,false);
        QA_Viewholder holder = new QA_Viewholder(view);
        return holder;
    }

    @SuppressLint({"ResourceAsColor", "NewApi"})
    @Override
    public void onBindViewHolder(@NonNull QA_Viewholder holder, final int position)
    {
        final TextView question = holder.question;
        final TextView img = holder.img;
        final TextView answer = holder.answer;
        final RelativeLayout ans = holder.ans;
        final RelativeLayout questionlayout = holder.questionlayout;
        questionlayout.setBackgroundResource(color.greenlights);

        question.setText(Html.fromHtml(qa.get(position).getQuestion()).toString());
        answer.setText(Html.fromHtml(qa.get(position).getAnswer()).toString());

        ans.setOnClickListener(new View.OnClickListener() {
            @SuppressLint({"NewApi", "ResourceAsColor"})
            @Override
            public void onClick(View v)
            {
                Log.e("statusclick", String.valueOf(status));
                if (status == false)
                {
                    qa.get(position).setSelected(true);
                    status = true;
                }

                if (qa.get(position).isSelected() == true)
                {
                    qa.get(position).setSelected(false);
                    img.setText("-");
                    answer.setVisibility(View.VISIBLE);
                    question.setTextColor(ContextCompat.getColor(activity, R.color.greenlights));
                    questionlayout.setBackgroundResource(color.colorPrimaryDark);
                }else
                {
                    qa.get(position).setSelected(true);
                    img.setText("+");
                    answer.setVisibility(View.GONE);
                    question.setTextColor(ContextCompat.getColor(activity, R.color.colorPrimaryDark));
                    questionlayout.setBackgroundResource(color.greenlights);
                }
            }
        });
    }

    @Override
    public int getItemCount()
    {
        return qa.size();
    }

    public static class QA_Viewholder extends RecyclerView.ViewHolder
    {
        TextView question,img,answer;
        RelativeLayout questionlayout;
        RelativeLayout ans;
        public QA_Viewholder(@NonNull View itemView)
        {
            super(itemView);

            question = itemView.findViewById(R.id.question);
            img = itemView.findViewById(R.id.img);
            ans = itemView.findViewById(R.id.viewans);
            answer = itemView.findViewById(R.id.ans);
            questionlayout = itemView.findViewById(R.id.layout2);
        }
    }
}
