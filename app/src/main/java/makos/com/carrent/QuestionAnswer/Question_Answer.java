package makos.com.carrent.QuestionAnswer;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import makos.com.carrent.MainActivity;
import makos.com.carrent.Mysingleton.Mysingleton;
import makos.com.carrent.NetworkInfo.Connectivity;
import makos.com.carrent.R;
import makos.com.carrent.lobby.Lobby;
import makos.com.carrent.shooting_damage.Shooting_Damages;
import makos.com.carrent.statusurl.Staticurl;

import static android.content.Context.MODE_PRIVATE;

public class Question_Answer extends AppCompatActivity {
    Button backtolobby;
    TextView alltheend;
    RelativeLayout layout,mobile_layout;
    ProgressBar progressBar;

    RecyclerView recyclerView;
    RecyclerView.Adapter adapter;
    RecyclerView.LayoutManager layoutManager;

    ArrayList<QA_Model> qa = new ArrayList<QA_Model>();
    String userid="";

    SharedPreferences preferences;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question__answer);

        backtolobby= findViewById(R.id.backtolobby);
       // alltheend=view.findViewById(R.id.alltheend);
        layout= findViewById(R.id.layout);
        progressBar= findViewById(R.id.progressbar);
       // mobile_layout=view.findViewById(R.id.mobile_layout);

       // Bundle bundle = getArguments();

        //userid = bundle.getString("userid");

        preferences = getSharedPreferences("makos.com.carrent",MODE_PRIVATE);
        editor = preferences.edit();

        userid = preferences.getString("USER_ID","0");

        recyclerView = findViewById(R.id.recycler);

        adapter = new QA_Adapter(Question_Answer.this,qa);
        layoutManager = new LinearLayoutManager(getApplicationContext());

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

        question_ans();

        backtolobby.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
            }
        });

       /* layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mobile_layout.setVisibility(View.VISIBLE);
            }
        });*/

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

    }

    private void question_ans()
    {
        qa.clear();
        String qaurl = Staticurl.qa;

        progressBar.setVisibility(View.VISIBLE);
        if (Connectivity.isConnected(getApplicationContext()))
        {
            final StringRequest request = new StringRequest(Request.Method.GET, qaurl, new Response.Listener<String>() {
                @Override
                public void onResponse(String response)
                {
                    try
                    {

                        JSONArray jsonArray = new JSONArray(response);
                        JSONObject jsonObject = jsonArray.getJSONObject(0);

                        if (jsonObject.getString("Status").equals("True"))
                        {
                            JSONArray jsonArray1 = jsonObject.getJSONArray("Data");

                            for (int i = 0; i < jsonArray1.length() ; i++)
                            {
                                JSONObject jsonObject1 = jsonArray1.getJSONObject(i);
                                QA_Model model = new QA_Model();

                                model.setQuestion(jsonObject1.getString("question"));
                                model.setAnswer(jsonObject1.getString("answer"));

                                qa.add(model);
                            }
                            progressBar.setVisibility(View.INVISIBLE);
                            adapter.notifyDataSetChanged();
                        }else
                        {
                            progressBar.setVisibility(View.INVISIBLE);
                            Toast.makeText(getApplicationContext(),"אין תשובה תשובות זמין",Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressBar.setVisibility(View.INVISIBLE);
                }
            });
            Mysingleton.getInstance(getApplicationContext()).addToRequestQueue(request);
        }
        else
        {
            Toast.makeText(getApplicationContext(),"בדוק את חיבור האינטרנט",Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
