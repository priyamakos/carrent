package makos.com.carrent.Company_list;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import makos.com.carrent.Car_list.Car_List_Adapter;
import makos.com.carrent.Car_list.Car_List_Model;
import makos.com.carrent.Car_list.Car_list;
import makos.com.carrent.Mysingleton.Mysingleton;
import makos.com.carrent.NetworkInfo.Connectivity;
import makos.com.carrent.R;
import makos.com.carrent.back_to_order.Back_order;
import makos.com.carrent.statusurl.Staticurl;

public class Company_list extends AppCompatActivity
{
    RelativeLayout layout;
    TextView msg,terms;
    ProgressBar progressBar;
    RecyclerView recyclerView;
    RecyclerView.Adapter adapter;
    RecyclerView.LayoutManager layoutManager;
    Button submit;

    boolean stat = false;
    String selected_company_id = "";
    String selected_company_name = "";

    public ArrayList<Company_list_model> models;

    public ArrayList<String> selected_companies = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_company_list);

        submit = findViewById(R.id.submit);
        progressBar = findViewById(R.id.progressbar);

        models = new ArrayList<Company_list_model>();
        recyclerView = findViewById(R.id.recycler);

        adapter = new Company_list_Adapter(Company_list.this,models);
        layoutManager = new LinearLayoutManager(getApplicationContext());

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

        company_list();

        recyclerView.addOnItemTouchListener(new Company_list.RecyclerTouchListener(getApplicationContext(), recyclerView,
                new Company_list.RecyclerTouchListener.ClickListener() {
                    @SuppressLint("ResourceAsColor")
                    @Override
                    public void onClick(final View view, final int position)
                    {
                        terms = view.findViewById(R.id.terms);

                        terms.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v)
                            {
                                getTerms();
                            }
                        });

                        view.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v)
                            {
                                if (!models.get(position).isSelected())
                                {
                                    for (int i = 0;i < models.size() ;i++)
                                    {
                                        if (models.get(i).isSelected())
                                        {
                                            models.get(i).setSelected(false);
                                            models.get(i).setColor(R.color.transparent);
                                        }
                                        Log.e("herestart", String.valueOf(models.get(i).isSelected()) + " " + i);
                                    }
                                    models.get(position).setSelected(true);
                                    models.get(position).setColor(R.color.greenlight);
                                    Log.e("herestart", String.valueOf(models.get(position).isSelected()) + " " + position);

                                }else
                                {
                                    models.get(position).setSelected(false);
                                    models.get(position).setColor(R.color.transparent);
                                }
                                adapter.notifyDataSetChanged();
                            }
                        });

                    }
                    @Override
                    public void onLongClick(View view, int position)
                    {

                    }
                }));

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                for (int i = 0;i < models.size() ;i++)
                {
                    if (models.get(i).isSelected())
                    {
                        selected_company_id = models.get(i).getCompany_id();
                        selected_company_name = models.get(i).getCompany_name();
                    }
                }

                Log.e("selected_company_id", selected_company_id);

                Intent intent = new Intent(Company_list.this, Back_order.class);
                intent.putExtra("selected_car_ids",selected_company_id);
                intent.putExtra("selected_company_name",selected_company_name);
                startActivity(intent);
            }
        });

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    private void getTerms()
    {
        String gettermsurl = Staticurl.getterms;
        progressBar.setVisibility(View.VISIBLE);

        if (Connectivity.isConnected(getApplicationContext()))
        {
            progressBar.setVisibility(View.VISIBLE);
            final StringRequest request = new StringRequest(Request.Method.GET, gettermsurl, new Response.Listener<String>() {
                @Override
                public void onResponse(String response)
                {
                    try
                    {
                        JSONArray jsonArray = new JSONArray(response);

                        JSONObject jsonObject = jsonArray.getJSONObject(0);

                        if (jsonObject.getString("Status").equals("True"))
                        {
                            progressBar.setVisibility(View.INVISIBLE);
                            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(jsonObject.getString("Terms")));//,Uri.parse(jsonObject.getString("Terms"))
                            //intent.setDataAndType(Uri.parse(jsonObject.getString("Terms")), "text/html");
                            startActivity(intent);
                        }
                        else {
                            progressBar.setVisibility(View.INVISIBLE);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressBar.setVisibility(View.INVISIBLE);
                }
            });
            Mysingleton.getInstance(getApplicationContext()).addToRequestQueue(request);
        }
        else
        {
            Toast.makeText(getApplicationContext(),"בדוק את החיבור לאינטרנט",Toast.LENGTH_SHORT).show();
        }
    }

    private void company_list()
    {
        String company_list = Staticurl.companylist;

        Log.d("Company_list",company_list);

        if (Connectivity.isConnected(getApplicationContext()))
        {
            progressBar.setVisibility(View.VISIBLE);
            StringRequest request = new StringRequest(Request.Method.GET, company_list, new Response.Listener<String>() {
                @Override
                public void onResponse(String response)
                {
                    try
                    {
                        JSONArray jsonArray = new JSONArray(response);
                        JSONObject jsonObject = jsonArray.getJSONObject(0);

                        if (jsonObject.getString("Status").equals("True"))
                        {
                            JSONArray jsonArray1 = jsonObject.getJSONArray("Company Data");

                            for (int i=0;i<jsonArray1.length();i++)
                            {
                                JSONObject jsonObject1 = jsonArray1.getJSONObject(i);
                                Company_list_model model = new Company_list_model();

                                model.setCompany_id(jsonObject1.getString("company_id"));
                                model.setCompany_name(jsonObject1.getString("company_name"));
                                model.setBranch(jsonObject1.getString("branch"));
                                model.setContact(jsonObject1.getString("contact"));
                                model.setTelephone(jsonObject1.getString("telephone"));
                                model.setEmail_id(jsonObject1.getString("email_id"));
                                model.setImage_path(jsonObject1.getString("image_path"));
                                model.setTerms_path(jsonObject1.getString("terms_path"));

                                models.add(model);
                            }
                            adapter.notifyDataSetChanged();
                            progressBar.setVisibility(View.INVISIBLE);
                        }
                        else
                        {
                            progressBar.setVisibility(View.INVISIBLE);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressBar.setVisibility(View.INVISIBLE);
                }
            });
            Mysingleton.getInstance(getApplicationContext()).addToRequestQueue(request);
        }
    }

    private static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private Company_list.RecyclerTouchListener.ClickListener clickListener;

        public RecyclerTouchListener(Context applicationContext, final RecyclerView recyclerView, final Company_list.RecyclerTouchListener.ClickListener clickListener)
        {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(applicationContext, new GestureDetector.SimpleOnGestureListener()
            {
                @Override
                public boolean onSingleTapUp(MotionEvent e)
                {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e)
                {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null)
                    {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }
        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e)
        {
            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e))
            {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }
        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e)
        {
        }
        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept)
        {
        }
        public interface ClickListener
        {
            void onClick(View view, int position);
            void onLongClick(View view, int position);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
