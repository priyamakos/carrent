package makos.com.carrent.Company_list;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import makos.com.carrent.R;

/**
 * Created by PC1 on 22-12-2018.
 */

public class Company_list_Adapter extends RecyclerView.Adapter<Company_list_Adapter.Company_list_Viewholder>
{
    Activity activity;
    ArrayList<Company_list_model> models = new ArrayList<Company_list_model>();

    boolean stat = false;
    int pos;


    public Company_list_Adapter(Activity activity, ArrayList<Company_list_model> models)
    {
        this.activity = activity;
        this.models = models;
    }

    @NonNull
    @Override
    public Company_list_Viewholder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i)
    {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.custom_company_list,viewGroup,false);
        Company_list_Viewholder holder = new Company_list_Viewholder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull final Company_list_Viewholder holder, final int position)
    {
        final RelativeLayout layout = holder.layout;
        TextView name = holder.name;
        ImageView imageView = holder.img;

        name.setText(Html.fromHtml(models.get(position).getCompany_name()).toString());
        Picasso.with(activity).load(models.get(position).getImage_path()).fit().placeholder(R.drawable.carlogo).error(R.drawable.carlogo).into(imageView);


        Log.e("positionhere", String.valueOf(position));

        layout.setBackgroundResource(models.get(position).getColor());

    }

    @Override
    public int getItemCount()
    {
        return models.size();
    }

    public static class Company_list_Viewholder extends RecyclerView.ViewHolder
    {
        RelativeLayout layout;
        TextView name;
        ImageView img;

        public Company_list_Viewholder(@NonNull View itemView)
        {
            super(itemView);

            layout = itemView.findViewById(R.id.layout);
            name = itemView.findViewById(R.id.companyname);
            img = itemView.findViewById(R.id.img);
        }
    }
}
