package makos.com.carrent.Account_Register;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.BidiFormatter;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import makos.com.carrent.MainActivity;
import makos.com.carrent.Mysingleton.Mysingleton;
import makos.com.carrent.NetworkInfo.Connectivity;
import makos.com.carrent.R;
import makos.com.carrent.login.Login;
import makos.com.carrent.statusurl.Staticurl;

/**
 * Created by pc2 on 01-12-2018.
 */

public class Register extends AppCompatActivity
{
    TextView terms;
    CheckBox termsconditions;
    EditText username,address,emailid,password;
    Button signin;
    ProgressBar progressBar;

    String accept_terms = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        username=findViewById(R.id.username);
        address=findViewById(R.id.address);
        emailid=findViewById(R.id.emailid);
        password=findViewById(R.id.password);
        signin=findViewById(R.id.signin);
        terms = findViewById(R.id.term);
        termsconditions = findViewById(R.id.terms);
        progressBar=findViewById(R.id.progressbar);

        terms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                getTerms();
            }
        });

        termsconditions.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if (isChecked)
                {
                    accept_terms = "1";
                }else
                {
                    accept_terms = "0";
                }
            }
        });

    /*    Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
         getSupportActionBar().setDisplayShowHomeEnabled(true);*/

    signin.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            validation();
        }
    });

    }

    private void validation() {
        if(username.getText().toString().equals("")){
            Toast.makeText(getApplicationContext(),"הכנס שם משתמש",Toast.LENGTH_SHORT).show();
        }else if(address.getText().toString().equals("")){
            Toast.makeText(getApplicationContext(),"הכנס כתובת",Toast.LENGTH_SHORT).show();
        }else if(emailid.getText().toString().equals("")){
            Toast.makeText(getApplicationContext(),"הזן מזהה דוא\"ל",Toast.LENGTH_SHORT).show();
        }else if(isValidMail(emailid.getText().toString()))
        {
            if (password.getText().toString().equals("")) {
            Toast.makeText(getApplicationContext(), "הזן את הסיסמה", Toast.LENGTH_SHORT).show();
            } else {
                register_api();
            }
        }

    }

    @SuppressLint("NewApi")
    private void register_api() {
        progressBar.setVisibility(View.VISIBLE);
        if(Connectivity.isConnected(getApplicationContext())){

            Log.e("userrname", String.valueOf(username)+ password);
            String reg_auth= null;

            try {
                reg_auth = Staticurl.register+
                        "?username="+ URLEncoder.encode(Html.toHtml(username.getText()),"UTF-8").toString().trim()+
                        "&address="+ URLEncoder.encode(Html.toHtml(address.getText()),"UTF-8").toString().trim()+
                        "&email_id="+URLEncoder.encode(emailid.getText().toString(),"UTF-8").toString().trim()+
                        "&password="+URLEncoder.encode(Html.toHtml(password.getText()),"UTF-8").toString().trim() +
                        "&login_through=" + "0" +
                        "&policy=" + URLEncoder.encode(accept_terms,"UTF-8").toString();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

            Log.e("login", reg_auth);


            StringRequest stringRequest=new StringRequest(Request.Method.GET,
                    reg_auth,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.e("registerresponce",response);
                            try {
                                JSONArray jsonArray=new JSONArray(response);
                                JSONObject jsonObject=jsonArray.getJSONObject(0);
                                if(jsonObject.getString("Status").equals("True"))
                                {
                                    progressBar.setVisibility(View.INVISIBLE);
                                    Toast.makeText(getApplicationContext(),"נרשם בהצלחה",Toast.LENGTH_SHORT).show();
                                    Intent intent=new Intent(Register.this,Login.class);
                                    startActivity(intent);
                                }
                                else
                                {
                                    progressBar.setVisibility(View.INVISIBLE);
                                    Toast.makeText(getApplicationContext(),"הרשמה נכשלה",Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e)
                            {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressBar.setVisibility(View.INVISIBLE);
                }
            });
            Mysingleton.getInstance(Register.this).addToRequestQueue(stringRequest);
        }
        else
        {
            Toast.makeText(getApplicationContext(), "בדוק את החיבור לאינטרנט", Toast.LENGTH_SHORT).show();
        }
    }

    private void getTerms()
    {
        String gettermsurl = Staticurl.getterms;
        progressBar.setVisibility(View.VISIBLE);

        if (Connectivity.isConnected(getApplicationContext()))
        {
            final StringRequest request = new StringRequest(Request.Method.GET, gettermsurl, new Response.Listener<String>() {
                @Override
                public void onResponse(String response)
                {
                    try
                    {
                        JSONArray jsonArray = new JSONArray(response);

                        JSONObject jsonObject = jsonArray.getJSONObject(0);

                        if (jsonObject.getString("Status").equals("True"))
                        {
                            progressBar.setVisibility(View.INVISIBLE);
                            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(jsonObject.getString("Terms")));//,Uri.parse(jsonObject.getString("Terms"))
                            //intent.setDataAndType(Uri.parse(jsonObject.getString("Terms")), "text/html");
                            startActivity(intent);
                        }
                        else {
                            progressBar.setVisibility(View.INVISIBLE);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressBar.setVisibility(View.INVISIBLE);
                }
            });
            Mysingleton.getInstance(getApplicationContext()).addToRequestQueue(request);
        }
        else
        {
            Toast.makeText(getApplicationContext(),"בדוק את החיבור לאינטרנט",Toast.LENGTH_SHORT).show();
        }
    }


    private boolean isValidMail(String email) {
        boolean check;
        Pattern p;
        Matcher m;

        String EMAIL_STRING = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        p = Pattern.compile(EMAIL_STRING);

        m = p.matcher(email);
        check = m.matches();

        if(!check) {
            Toast.makeText(getApplicationContext(),"הזן מזהה דוא\"ל חוקי",Toast.LENGTH_SHORT).show();
            // txtEmail.setError("Not Valid Email");
        }
        return check;
    }
}
